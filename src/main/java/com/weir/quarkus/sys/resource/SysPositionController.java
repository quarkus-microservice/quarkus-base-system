package com.weir.quarkus.sys.resource;

import lombok.extern.slf4j.Slf4j;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.base.system.utils.QueryHelper;
import com.weir.quarkus.sys.entity.SysPosition;
import com.weir.quarkus.sys.entity.SysUserPosition;
import com.weir.quarkus.sys.query.SysPositionQuery;
import com.weir.quarkus.sys.vo.PageDTO;
import com.weir.quarkus.sys.vo.Result;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

/**
 * @Description: 职务表
 * @Author: jeecg-boot
 * @Date: 2019-09-19
 * @Version: V1.0
 */
@Slf4j
@Tag(name = "职务表")
@Path("/sys/position")
public class SysPositionController {

	@Inject
	EntityManager em;
    /**
     * 分页列表查询
     *
     * @param sysPosition
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
//    @AutoLog(value = "职务表-分页列表查询")
    @Operation(summary = "职务表-分页列表查询")
    @GET
    @Path(value = "/list")
    public Response queryPageList(@BeanParam SysPositionQuery query) {
    	PageDTO<SysPosition> queryPage = QueryHelper.createQueryPage(em, SysPosition.class, query);
		return Response.ok().entity(Result.ok(queryPage)).build();
    }

    /**
     * 添加
     *
     * @param sysPosition
     * @return
     */
//    @AutoLog(value = "职务表-添加")
    @Operation(summary = "职务表-添加")
    @POST @Path(value = "/add")
    @Transactional
    public Response add(SysPosition sysPosition) {
        sysPosition.persist();
        return Response.ok().entity(Result.OK("添加成功",sysPosition)).build();
    }

    /**
     * 编辑
     *
     * @param sysPosition
     * @return
     */
//    @AutoLog(value = "职务表-编辑")
    @Operation(summary = "职务表-编辑")
//    @RequestMapping(value = "/edit", method ={RequestMethod.PUT, RequestMethod.POST})
    @POST @Path(value = "/edit")
    @Transactional
    public Response edit(SysPosition sysPosition) {
    	SysPosition old = SysPosition.findById(sysPosition.getId());
    	if (old == null) {
    		return Response.ok().entity(Result.error("未找到对应实体")).build();
		}
    	old.setName(sysPosition.getName());
    	return Response.ok().entity(Result.OK("添加成功",sysPosition)).build();
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
//    @AutoLog(value = "职务表-通过id删除")
    @Operation(summary = "职务表-通过id删除")
    @DELETE @Path(value = "/delete")
    @Transactional
    public Response delete(@QueryParam(value = "id") @NotNull String id) {
    	SysPosition.deleteById(id);
    	SysUserPosition.delete("positionId", id);
    	return Response.ok().entity(Result.OK("删除成功")).build();
    }

//    /**
//     * 批量删除
//     *
//     * @param ids
//     * @return
//     */
////    @AutoLog(value = "职务表-批量删除")
//    @Operation(summary = "职务表-批量删除")
//    @DELETE @Path(value = "/deleteBatch")
//    @Transactional
//    public Response deleteBatch(@QueryParam(value = "ids") @NotNull String ids) {
//        Result<SysPosition> result = new Result<SysPosition>();
//        if (ids == null || "".equals(ids.trim())) {
//            result.error500("参数不识别！");
//        } else {
//            this.sysPositionService.removeByIds(Arrays.asList(ids.split(",")));
//            result.success("删除成功!");
//        }
//        return result;
//    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
//    @AutoLog(value = "职务表-通过id查询")
    @Operation(summary = "职务表-通过id查询")
    @GET @Path(value = "/queryById")
    public Response queryById(@QueryParam(value = "id") @NotNull String id) {
    	SysPosition old = SysPosition.findById(id);
    	if (old == null) {
    		return Response.ok().entity(Result.error("未找到对应实体")).build();
		}
    	return Response.ok().entity(Result.OK(old)).build();
    }
//
//    /**
//     * 导出excel
//     *
//     * @param request
//     * @param response
//     */
//    @RequestMapping(value = "/exportXls")
//    public ModelAndView exportXls(HttpServletRequest request, HttpServletResponse response) {
//        // Step.1 组装查询条件
//        QueryWrapper<SysPosition> queryWrapper = null;
//        try {
//            String paramsStr = request.getParameter("paramsStr");
//            if (oConvertUtils.isNotEmpty(paramsStr)) {
//                String deString = URLDecoder.decode(paramsStr, "UTF-8");
//                SysPosition sysPosition = JSON.parseObject(deString, SysPosition.class);
//                //------------------------------------------------------------------------------------------------
//                //是否开启系统管理模块的多租户数据隔离【SAAS多租户模式】
//                if(MybatisPlusSaasConfig.OPEN_SYSTEM_TENANT_CONTROL){
//                    sysPosition.setTenantId(oConvertUtils.getInt(TenantContext.getTenant(),0));
//                }
//                //------------------------------------------------------------------------------------------------
//                queryWrapper = QueryGenerator.initQueryWrapper(sysPosition, request.getParameterMap());
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//
//        //Step.2 AutoPoi 导出Excel
//        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
//        List<SysPosition> pageList = sysPositionService.list(queryWrapper);
//        LoginUser user = SecureUtil.currentUser();
//        //导出文件名称
//        mv.addObject(NormalExcelConstants.FILE_NAME, "职务表列表");
//        mv.addObject(NormalExcelConstants.CLASS, SysPosition.class);
//        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("职务表列表数据", "导出人:"+user.getRealname(),"导出信息"));
//        mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
//        return mv;
//    }
//
//    /**
//     * 通过excel导入数据
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
//    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response)throws IOException {
//        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
//        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
//        // 错误信息
//        List<String> errorMessage = new ArrayList<>();
//        int successLines = 0, errorLines = 0;
//        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
//            // 获取上传文件对象
//            MultipartFile file = entity.getValue();
//            ImportParams params = new ImportParams();
//            params.setTitleRows(2);
//            params.setHeadRows(1);
//            params.setNeedSave(true);
//            try {
//                List<Object>  listSysPositions = ExcelImportUtil.importExcel(file.getInputStream(), SysPosition.class, params);
//                List<String> list = ImportExcelUtil.importDateSave(listSysPositions, ISysPositionService.class, errorMessage,CommonConstant.SQL_INDEX_UNIQ_CODE);
//                errorLines+=list.size();
//                successLines+=(listSysPositions.size()-errorLines);
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//                return Result.error("文件导入失败:" + e.getMessage());
//            } finally {
//                try {
//                    file.getInputStream().close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return ImportExcelUtil.imporReturnRes(errorLines,successLines,errorMessage);
//    }
//
//    /**
//     * 通过code查询
//     *
//     * @param code
//     * @return
//     */
//    @AutoLog(value = "职务表-通过code查询")
//    @Operation(summary = "职务表-通过code查询")
//    @GetMapping(value = "/queryByCode")
//    public Result<SysPosition> queryByCode(@RequestParam(name = "code", required = true) String code) {
//        Result<SysPosition> result = new Result<SysPosition>();
//        QueryWrapper<SysPosition> queryWrapper = new QueryWrapper<SysPosition>();
//        queryWrapper.eq("code",code);
//        SysPosition sysPosition = sysPositionService.getOne(queryWrapper);
//        if (sysPosition == null) {
//            result.error500("未找到对应实体");
//        } else {
//            result.setResult(sysPosition);
//            result.setSuccess(true);
//        }
//        return result;
//    }
//
//
//    /**
//     * 通过多个ID查询
//     *
//     * @param ids
//     * @return
//     */
//    @AutoLog(value = "职务表-通过多个查询")
//    @Operation(summary = "职务表-通过多个id查询")
//    @GetMapping(value = "/queryByIds")
//    public Result<List<SysPosition>> queryByIds(@RequestParam(name = "ids") String ids) {
//        Result<List<SysPosition>> result = new Result<>();
//        QueryWrapper<SysPosition> queryWrapper = new QueryWrapper<>();
//        queryWrapper.in(true,"id",ids.split(","));
//        List<SysPosition> list = sysPositionService.list(queryWrapper);
//        if (list == null) {
//            result.error500("未找到对应实体");
//        } else {
//            result.setResult(list);
//            result.setSuccess(true);
//        }
//        return result;
//    }
//
//
//
//    /**
//     * 获取职位用户列表
//     *
//     * @param pageNo
//     * @param pageSize
//     * @param positionId
//     * @return
//     */
//    @GetMapping("/getPositionUserList")
//    public Result<IPage<SysUser>> getPositionUserList(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
//                                                      @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
//                                                      @RequestParam(name = "positionId") String positionId) {
//
//        Page<SysUser> page = new Page<>(pageNo, pageSize);
//        IPage<SysUser> pageList = userPositionService.getPositionUserList(page, positionId);
//        List<String> userIds = pageList.getRecords().stream().map(SysUser::getId).collect(Collectors.toList());
//        if (null != userIds && userIds.size() > 0) {
//            Map<String, String> useDepNames = userService.getDepNamesByUserIds(userIds);
//            pageList.getRecords().forEach(item -> {
//                item.setOrgCodeTxt(useDepNames.get(item.getId()));
//            });
//        }
//        return Result.ok(pageList);
//    }
//
//    /**
//     * 添加成员到用户职位关系表
//     *
//     * @param userIds
//     * @param positionId
//     * @return
//     */
//    @PostMapping("/savePositionUser")
//    public Result<String> saveUserPosition(@RequestParam(name = "userIds") String userIds,
//                                           @RequestParam(name = "positionId") String positionId) {
//        userPositionService.saveUserPosition(userIds, positionId);
//        return Result.ok("添加成功");
//    }
//
//    /**
//     * 职位列表移除成员
//     *
//     * @param userIds
//     * @param positionId
//     * @return
//     */
//    @DeleteMapping("/removePositionUser")
//    public Result<String> removeUserPosition(@RequestParam(name = "userIds") String userIds,
//                                             @RequestParam(name = "positionId") String positionId) {
//        userPositionService.removePositionUser(userIds, positionId);
//        return Result.OK("移除成员成功");
//    }
}
