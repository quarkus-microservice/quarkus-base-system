package com.weir.quarkus.sys.resource;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;

import com.weir.quarkus.base.system.utils.QueryHelper;
import com.weir.quarkus.sys.entity.SysDict;
import com.weir.quarkus.sys.entity.SysPermission;
import com.weir.quarkus.sys.entity.SysRole;
import com.weir.quarkus.sys.query.SysRoleQuery;
import com.weir.quarkus.sys.vo.PageDTO;
import com.weir.quarkus.sys.vo.Result;
import com.weir.quarkus.sys.vo.TreeModel;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.panache.common.Sort;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @Author scott
 * @since 2018-12-19
 */
@Path("/sys/role")
@Slf4j
public class SysRoleController {
	
	@Inject
	EntityManager em;
	/**
	  * 分页列表查询 【系统角色，不做租户隔离】
	 * @param role
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
//	@PreAuthorize("@jps.requiresPermissions('system:role:list')")
//	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@GET
	@Path("/list")
	public Response queryPageList(@BeanParam SysRoleQuery query) {
		PageDTO<SysRole> queryPage = QueryHelper.createQueryPage(em, SysRole.class, query);
		return Response.ok().entity(Result.ok(queryPage)).build();
	}
	
	/**
	 * 分页列表查询【租户角色，做租户隔离】
	 * @param role
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
//	@RequestMapping(value = "/listByTenant", method = RequestMethod.GET)
//	public Result<IPage<SysRole>> listByTenant(SysRole role,
//												@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
//												@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
//												HttpServletRequest req) {
//		Result<IPage<SysRole>> result = new Result<IPage<SysRole>>();
//		//------------------------------------------------------------------------------------------------
//		//此接口必须通过租户来隔离查询
//		role.setTenantId(oConvertUtils.getInt(!"0".equals(TenantContext.getTenant()) ? TenantContext.getTenant() : "", -1));
//		
//		QueryWrapper<SysRole> queryWrapper = QueryGenerator.initQueryWrapper(role, req.getParameterMap());
//		Page<SysRole> page = new Page<SysRole>(pageNo, pageSize);
//		IPage<SysRole> pageList = sysRoleService.page(page, queryWrapper);
//		result.setSuccess(true);
//		result.setResult(pageList);
//		return result;
//	}
//	
	/**
	  *   添加
	 * @param role
	 * @return
	 */
//	@RequestMapping(value = "/add", method = RequestMethod.POST)
//	@PreAuthorize("@jps.requiresPermissions('system:role:add')")
	@POST
	@Path("/add")
	@Transactional
	public Response add(SysRole role) {
		role.persist();
		return Response.ok().entity(Result.OK("添加成功！",role)).build();
	}
	
	/**
	  *  编辑
	 * @param role
	 * @return
	 */
//	@PreAuthorize("@jps.requiresPermissions('system:role:edit')")
//	@RequestMapping(value = "/edit",method = {RequestMethod.PUT,RequestMethod.POST})
	@POST
	@Path("/edit")
	@Transactional
	public Response edit(SysRole role) {
		SysRole old = SysRole.findById(role.getId());
		if (old == null) {
			return Response.ok().entity(Result.error("未找到对应角色！")).build();
		}
		old.setDescription(role.getDescription());
		old.setRoleCode(role.getRoleCode());
		old.setRoleName(role.getRoleName());
		return Response.ok().entity(Result.OK("修改成功！",old)).build();
	}
	
	/**
	  *   通过id删除
	 * @param id
	 * @return
	 */
//	@PreAuthorize("@jps.requiresPermissions('system:role:delete')")
//	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@DELETE
	@Path("/delete")
	public Response delete(@QueryParam("id") @NotNull String id) {
		SysRole.deleteById(id);
		return Response.ok().entity(Result.OK("删除角色成功！")).build();
	}
//	
//	/**
//	  *  批量删除
//	 * @param ids
//	 * @return
//	 */
//	@PreAuthorize("@jps.requiresPermissions('system:role:deleteBatch')")
//	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
//	public Result<SysRole> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
//		baseCommonService.addLog("删除角色操作，角色ids：" + ids, CommonConstant.LOG_TYPE_2, CommonConstant.OPERATE_TYPE_4);
//		Result<SysRole> result = new Result<SysRole>();
//		if(oConvertUtils.isEmpty(ids)) {
//			result.error500("未选中角色！");
//		}else {
//			//如果是saas隔离的情况下，判断当前租户id是否是当前租户下的
//			if(MybatisPlusSaasConfig.OPEN_SYSTEM_TENANT_CONTROL){
//				int tenantId = oConvertUtils.getInt(TenantContext.getTenant(), 0);
//				String[] roleIds = ids.split(SymbolConstant.COMMA);
//				LoginUser sysUser = SecureUtil.currentUser();
//				String username = "admin";
//				for (String id:roleIds) {
//					Long getRoleCount = sysRoleService.getRoleCountByTenantId(id, tenantId);
//					//如果存在角色id为0，即不存在，则删除角色
//					if(getRoleCount == 0 && !username.equals(sysUser.getUsername()) ){
//						baseCommonService.addLog("未经授权，删除非本租户下的角色ID：" + id + "，操作人：" + sysUser.getUsername(), CommonConstant.LOG_TYPE_2, CommonConstant.OPERATE_TYPE_4);
//						return Result.error("批量删除角色失败,存在角色不在此租户中，禁止批量删除");
//					}
//				}
//			}
//			sysRoleService.deleteBatchRole(ids.split(","));
//			result.success("删除角色成功!");
//		}
//		return result;
//	}
//	
//	/**
//	  * 通过id查询
//	 * @param id
//	 * @return
//	 */
//	@RequestMapping(value = "/queryById", method = RequestMethod.GET)
//	public Result<SysRole> queryById(@RequestParam(name="id",required=true) String id) {
//		Result<SysRole> result = new Result<SysRole>();
//		SysRole sysrole = sysRoleService.getById(id);
//		if(sysrole==null) {
//			result.error500("未找到对应实体");
//		}else {
//			result.setResult(sysrole);
//			result.setSuccess(true);
//		}
//		return result;
//	}
//
//	/**
//	 * 查询全部角色（参与租户隔离）
//	 * 
//	 * @return
//	 */
//	@RequestMapping(value = "/queryall", method = RequestMethod.GET)
//	public Result<List<SysRole>> queryall() {
//		Result<List<SysRole>> result = new Result<>();
//		LambdaQueryWrapper<SysRole> query = new LambdaQueryWrapper<SysRole>();
//		//------------------------------------------------------------------------------------------------
//		//是否开启系统管理模块的多租户数据隔离【SAAS多租户模式】
//		if(MybatisPlusSaasConfig.OPEN_SYSTEM_TENANT_CONTROL){
//			query.eq(SysRole::getTenantId, oConvertUtils.getInt(TenantContext.getTenant(), 0));
//		}
//		//------------------------------------------------------------------------------------------------
//		List<SysRole> list = sysRoleService.list(query);
//		if(list==null||list.size()<=0) {
//			result.error500("未找到角色信息");
//		}else {
//			result.setResult(list);
//			result.setSuccess(true);
//		}
//		return result;
//	}
//
//	/**
//	 * 查询全部系统角色（不做租户隔离）
//	 *
//	 * @return
//	 */
//	@PreAuthorize("@jps.requiresPermissions('system:role:queryallNoByTenant')")
//	@RequestMapping(value = "/queryallNoByTenant", method = RequestMethod.GET)
//	public Result<List<SysRole>> queryallNoByTenant() {
//		Result<List<SysRole>> result = new Result<>();
//		LambdaQueryWrapper<SysRole> query = new LambdaQueryWrapper<SysRole>();
//		List<SysRole> list = sysRoleService.list(query);
//		if(list==null||list.size()<=0) {
//			result.error500("未找到角色信息");
//		}else {
//			result.setResult(list);
//			result.setSuccess(true);
//		}
//		return result;
//	}
//	
	/**
	  * 校验角色编码唯一
	 */
//	@RequestMapping(value = "/checkRoleCode", method = RequestMethod.GET)
	@GET
	@Path("/checkRoleCode")
	public Response checkUsername(@BeanParam SysRoleQuery query) {
		if (StringUtils.isBlank(query.getRoleCode())) {
			return Response.ok().entity(Result.error("角色编码不能为空！")).build();
		}
		if (StringUtils.isNotBlank(query.getId())) {
			SysRole old = SysRole.findById(query.getId());
			if (old == null) {
				return Response.ok().entity(Result.error("未找到对应角色！")).build();
			}
			long count = SysRole.count("roleCode = ?1 and id <> ?2", query.getRoleCode(),query.getId());
			if (count > 0) {
				return Response.ok().entity(Result.error("角色编码已存在")).build();
			}
		}else {
			long count = SysRole.count("roleCode", query.getRoleCode());
			if (count > 0) {
				return Response.ok().entity(Result.error("角色编码已存在")).build();
			}
		}
		return Response.ok().entity(Result.ok(true)).build();
	}
//
//	/**
//	 * 导出excel
//	 * @param request
//	 */
//	@RequestMapping(value = "/exportXls")
//	public ModelAndView exportXls(SysRole sysRole,HttpServletRequest request) {
//		//------------------------------------------------------------------------------------------------
//		//是否开启系统管理模块的多租户数据隔离【SAAS多租户模式】
//		if(MybatisPlusSaasConfig.OPEN_SYSTEM_TENANT_CONTROL){
//			sysRole.setTenantId(oConvertUtils.getInt(TenantContext.getTenant(), 0));
//		}
//		//------------------------------------------------------------------------------------------------
//		
//		// Step.1 组装查询条件
//		QueryWrapper<SysRole> queryWrapper = QueryGenerator.initQueryWrapper(sysRole, request.getParameterMap());
//		//Step.2 AutoPoi 导出Excel
//		ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
//		List<SysRole> pageList = sysRoleService.list(queryWrapper);
//		//导出文件名称
//		mv.addObject(NormalExcelConstants.FILE_NAME,"角色列表");
//		mv.addObject(NormalExcelConstants.CLASS,SysRole.class);
//		LoginUser user = SecureUtil.currentUser();
//		mv.addObject(NormalExcelConstants.PARAMS,new ExportParams("角色列表数据","导出人:"+user.getRealname(),"导出信息"));
//		mv.addObject(NormalExcelConstants.DATA_LIST,pageList);
//		return mv;
//	}
//
//	/**
//	 * 通过excel导入数据
//	 * @param request
//	 * @param response
//	 * @return
//	 */
//	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
//	public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
//		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
//		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
//		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
//            // 获取上传文件对象
//			MultipartFile file = entity.getValue();
//			ImportParams params = new ImportParams();
//			params.setTitleRows(2);
//			params.setHeadRows(1);
//			params.setNeedSave(true);
//			try {
//				return sysRoleService.importExcelCheckRoleCode(file, params);
//			} catch (Exception e) {
//				log.error(e.getMessage(), e);
//				return Result.error("文件导入失败:" + e.getMessage());
//			} finally {
//				try {
//					file.getInputStream().close();
//				} catch (IOException e) {
//					log.error(e.getMessage(), e);
//				}
//			}
//		}
//		return Result.error("文件导入失败！");
//	}
//	
//	/**
//	 * 查询数据规则数据
//	 */
//	@GetMapping(value = "/datarule/{permissionId}/{roleId}")
//	public Result<?> loadDatarule(@PathVariable("permissionId") String permissionId,@PathVariable("roleId") String roleId) {
//		List<SysPermissionDataRule> list = sysPermissionDataRuleService.getPermRuleListByPermId(permissionId);
//		if(list==null || list.size()==0) {
//			return Result.error("未找到权限配置信息");
//		}else {
//			Map<String,Object> map = new HashMap(5);
//			map.put("datarule", list);
//			LambdaQueryWrapper<SysRolePermission> query = new LambdaQueryWrapper<SysRolePermission>()
//					.eq(SysRolePermission::getPermissionId, permissionId)
//					.isNotNull(SysRolePermission::getDataRuleIds)
//					.eq(SysRolePermission::getRoleId,roleId);
//			SysRolePermission sysRolePermission = sysRolePermissionService.getOne(query);
//			if(sysRolePermission==null) {
//				//return Result.error("未找到角色菜单配置信息");
//			}else {
//				String drChecked = sysRolePermission.getDataRuleIds();
//				if(oConvertUtils.isNotEmpty(drChecked)) {
//					map.put("drChecked", drChecked.endsWith(",")?drChecked.substring(0, drChecked.length()-1):drChecked);
//				}
//			}
//			return Result.ok(map);
//			//TODO 以后按钮权限的查询也走这个请求 无非在map中多加两个key
//		}
//	}
//	
//	/**
//	 * 保存数据规则至角色菜单关联表
//	 */
//	@PostMapping(value = "/datarule")
//	public Result<?> saveDatarule(@RequestBody JSONObject jsonObject) {
//		try {
//			String permissionId = jsonObject.getString("permissionId");
//			String roleId = jsonObject.getString("roleId");
//			String dataRuleIds = jsonObject.getString("dataRuleIds");
//			log.info("保存数据规则>>"+"菜单ID:"+permissionId+"角色ID:"+ roleId+"数据权限ID:"+dataRuleIds);
//			LambdaQueryWrapper<SysRolePermission> query = new LambdaQueryWrapper<SysRolePermission>()
//					.eq(SysRolePermission::getPermissionId, permissionId)
//					.eq(SysRolePermission::getRoleId,roleId);
//			SysRolePermission sysRolePermission = sysRolePermissionService.getOne(query);
//			if(sysRolePermission==null) {
//				return Result.error("请先保存角色菜单权限!");
//			}else {
//				sysRolePermission.setDataRuleIds(dataRuleIds);
//				this.sysRolePermissionService.updateById(sysRolePermission);
//			}
//		} catch (Exception e) {
//			log.error("SysRoleController.saveDatarule()发生异常：" + e.getMessage(),e);
//			return Result.error("保存失败");
//		}
//		return Result.ok("保存成功!");
//	}
	
	record TreeListDTO(List<TreeModel> treeList,List<String> ids) {}
	/**
	 * 用户角色授权功能，查询菜单权限树
	 * @param request
	 * @return
	 */
//	@RequestMapping(value = "/queryTreeList", method = RequestMethod.GET)
	@GET
	@Path("/queryTreeList")
	public Response queryTreeList() {
		List<SysPermission> list = SysPermission.listAll(Sort.by("sortNo"));
		//全部权限ids
		List<String> ids = list.stream().map(SysPermission::getId).collect(Collectors.toList());
		List<TreeModel> treeList = new ArrayList<>();
		getTreeModelList(treeList, list, null);
		return Response.ok().entity(Result.ok(new TreeListDTO(treeList, ids))).build();
	}
	
	private void getTreeModelList(List<TreeModel> treeList,List<SysPermission> metaList,TreeModel temp) {
		for (SysPermission permission : metaList) {
			String tempPid = permission.getParentId();
			TreeModel tree = new TreeModel(permission.getId(), tempPid, permission.getName(),permission.getRuleFlag(), permission.getIsLeaf());
			if(temp==null && StringUtils.isBlank(tempPid)) {
				treeList.add(tree);
				if(!tree.getIsLeaf()) {
					getTreeModelList(treeList, metaList, tree);
				}
			}else if(temp!=null && tempPid!=null && tempPid.equals(temp.getKey())){
				temp.getChildren().add(tree);
				if(!tree.getIsLeaf()) {
					getTreeModelList(treeList, metaList, tree);
				}
			}
		}
	}
//
//    /**
//     * TODO 权限未完成（敲敲云接口，租户应用）
//     * 分页获取全部角色列表（包含每个角色的数量）
//     * @return
//     */
//    @RequestMapping(value = "/queryPageRoleCount", method = RequestMethod.GET)
//    public Result<IPage<SysUserRoleCountVo>> queryPageRoleCount(@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
//                                                                @RequestParam(name="pageSize", defaultValue="10") Integer pageSize) {
//        Result<IPage<SysUserRoleCountVo>> result = new Result<>();
//		LambdaQueryWrapper<SysRole> query = new LambdaQueryWrapper<SysRole>();
//		//------------------------------------------------------------------------------------------------
//		//是否开启系统管理模块的多租户数据隔离【SAAS多租户模式】
//		if(MybatisPlusSaasConfig.OPEN_SYSTEM_TENANT_CONTROL){
//			query.eq(SysRole::getTenantId, oConvertUtils.getInt(TenantContext.getTenant(), 0));
//		}
//		//------------------------------------------------------------------------------------------------
//        Page<SysRole> page = new Page<>(pageNo, pageSize);
//        IPage<SysRole> pageList = sysRoleService.page(page, query);
//        List<SysRole> records = pageList.getRecords();
//        IPage<SysUserRoleCountVo> sysRoleCountPage = new PageDTO<>();
//        List<SysUserRoleCountVo> sysCountVoList = new ArrayList<>();
//        //循环角色数据获取每个角色下面对应的角色数量
//        for (SysRole role:records) {
//            LambdaQueryWrapper<SysUserRole> countQuery = new LambdaQueryWrapper<>();
//			countQuery.eq(SysUserRole::getRoleId,role.getId());
//            long count = sysUserRoleService.count(countQuery);
//            SysUserRoleCountVo countVo = new SysUserRoleCountVo();
//            BeanUtils.copyProperties(role,countVo);
//            countVo.setCount(count);
//            sysCountVoList.add(countVo);
//        }
//        sysRoleCountPage.setRecords(sysCountVoList);
//        sysRoleCountPage.setTotal(pageList.getTotal());
//        sysRoleCountPage.setSize(pageList.getSize());
//        result.setSuccess(true);
//        result.setResult(sysRoleCountPage);
//        return result;
//    }
}
