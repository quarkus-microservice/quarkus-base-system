package com.weir.quarkus.sys.resource;


import java.util.Arrays;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.weir.quarkus.base.system.utils.QueryHelper;
import com.weir.quarkus.sys.entity.QSysDictItem;
import com.weir.quarkus.sys.entity.SysDict;
import com.weir.quarkus.sys.entity.SysDictItem;
import com.weir.quarkus.sys.query.DictItemQuery;
import com.weir.quarkus.sys.utils.MergeEntityCopyUtil;
import com.weir.quarkus.sys.vo.PageDTO;
import com.weir.quarkus.sys.vo.Result;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @Author zhangweijian
 * @since 2018-12-28
 */
@Tag(name = "数据字典")
@Path("/sys/dictItem")
@Slf4j
public class SysDictItemController {
    private static final QSysDictItem qSysDictItem = QSysDictItem.sysDictItem;
	
	@Inject JPAQueryFactory jpaQueryFactory;
	@Inject EntityManager em;
	
	/**
	 * @功能：查询字典数据
	 * @param sysDictItem
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
//	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@GET
	@Path("/list")
	public Response queryPageList(@BeanParam DictItemQuery query) {
//		Result<IPage<DictItemQuery>> result = new Result<IPage<DictItemQuery>>();
//		QueryWrapper<DictItemQuery> queryWrapper = QueryGenerator.initQueryWrapper(sysDictItem, req.getParameterMap());
//		queryWrapper.orderByAsc("sort_order");
//		Page<DictItemQuery> page = new Page<DictItemQuery>(pageNo, pageSize);
//		IPage<DictItemQuery> pageList = sysDictItemService.page(page, queryWrapper);
//		result.setSuccess(true);
//		result.setResult(pageList);
//		return result;
		query.setColumn("sortOrder");
		PageDTO<SysDictItem> queryPage = QueryHelper.createQueryPage(em, SysDictItem.class, query);
		return Response.ok().entity(Result.ok(queryPage)).build();
	}
	
	/**
	 * @功能：新增
	 * @return
	 */
//	@PreAuthorize("@jps.requiresPermissions('system:dict:item:add')")
//	@RequestMapping(value = "/add", method = RequestMethod.POST)
//	@CacheEvict(value= {CacheConstant.SYS_DICT_CACHE, CacheConstant.SYS_ENABLE_DICT_CACHE}, allEntries=true)
	@POST
	@Path("/add")
	@Transactional
	public Response add(SysDictItem sysDictItem) {
		sysDictItem.persist();
		return Response.ok().entity(Result.OK("添加成功",sysDictItem)).build();
	}
	
	/**
	 * @功能：编辑
	 * @param sysDictItem
	 * @return
	 */
//	@PreAuthorize("@jps.requiresPermissions('system:dict:item:edit')")
//	@RequestMapping(value = "/edit",  method = { RequestMethod.PUT,RequestMethod.POST })
//	@CacheEvict(value={CacheConstant.SYS_DICT_CACHE, CacheConstant.SYS_ENABLE_DICT_CACHE}, allEntries=true)
	@POST
	@Path("/edit")
	@Transactional
	public Response edit(SysDictItem sysDictItem) {
		SysDictItem old = SysDictItem.findById(sysDictItem.getId());
		if (old == null) {
			return Response.ok().entity(Result.error("未找到对应实体")).build();
		}
		MergeEntityCopyUtil.updateEntity(sysDictItem, old);
		em.merge(old);
		return Response.ok().entity(Result.OK("编辑成功!")).build();
	}
	
	/**
	 * @功能：删除字典数据
	 * @param id
	 * @return
	 */
//	@PreAuthorize("@jps.requiresPermissions('system:dict:item:delete')")
//	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
//	@CacheEvict(value={CacheConstant.SYS_DICT_CACHE, CacheConstant.SYS_ENABLE_DICT_CACHE}, allEntries=true)
	@DELETE
	@Path("/delete")
	@Transactional
	public Response delete(@QueryParam("id") @NotNull String id) {
		SysDictItem old = SysDictItem.findById(id);
		if (old == null) {
			return Response.ok().entity(Result.error("未找到对应实体")).build();
		}
		old.delete();
		return Response.ok().entity(Result.OK("删除成功!")).build();
	}
	
	/**
	 * @功能：批量删除字典数据
	 * @param ids
	 * @return
	 */
//	@PreAuthorize("@jps.requiresPermissions('system:dict:item:deleteBatch')")
//	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
//	@CacheEvict(value={CacheConstant.SYS_DICT_CACHE, CacheConstant.SYS_ENABLE_DICT_CACHE}, allEntries=true)
	@DELETE
	@Path("/deleteBatch")
	@Transactional
	public Response deleteBatch(@QueryParam("ids") @NotNull String ids) {
		SysDictItem.delete("id in (?1)", Arrays.asList(ids.split(",")));
		return Response.ok().entity(Result.OK("删除成功!")).build();
	}

	/**
	 * 字典值重复校验
	 * @param sysDictItem
	 * @param request
	 * @return
	 */
//	@RequestMapping(value = "/dictItemCheck", method = RequestMethod.GET)
	@Operation(summary = "字典重复校验接口")
	@GET
	@Path("/dictItemCheck")
	public Response doDictItemCheck(@BeanParam DictItemQuery query) {
		BooleanBuilder booleanBuilder = new BooleanBuilder();
		booleanBuilder.and(qSysDictItem.dictId.eq(query.getDictId()));
		booleanBuilder.and(qSysDictItem.itemValue.eq(query.getItemValue()));
		if (StringUtils.isNotBlank(query.getId())) {
			booleanBuilder.and(qSysDictItem.id.ne(query.getId()));
		}
		int size = jpaQueryFactory.selectFrom(qSysDictItem).where(booleanBuilder).fetch().size();
		if (size == 0) {
			return Response.ok().entity(Result.ok("该值可用！")).build();
		}
		return Response.ok().entity(Result.error("该值不可用，系统中已存在！")).build();
	}
	
}
