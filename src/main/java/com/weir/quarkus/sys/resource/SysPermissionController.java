package com.weir.quarkus.sys.resource;

import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.weir.quarkus.base.system.handler.BaseEntityListener;
import com.weir.quarkus.sys.entity.QSysPermission;
import com.weir.quarkus.sys.entity.SysPermission;
import com.weir.quarkus.sys.entity.SysRolePermission;
import com.weir.quarkus.sys.entity.SysUser;
import com.weir.quarkus.sys.service.SysPermissionService;
import com.weir.quarkus.sys.utils.PermissionDataUtil;
import com.weir.quarkus.sys.utils.SymbolConstant;
import com.weir.quarkus.sys.vo.CommonConstant;
import com.weir.quarkus.sys.vo.MenuDTO;
import com.weir.quarkus.sys.vo.MetaDTO;
import com.weir.quarkus.sys.vo.Result;
import com.weir.quarkus.sys.vo.SysPermissionDTO;
import com.weir.quarkus.sys.vo.SysPermissionTree;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * <p>
 * 菜单权限表 前端控制器
 * </p>
 *
 * @Author scott
 * @since 2018-12-21
 */
@Tag(name = "菜单权限")
@Slf4j
@Path("/sys/permission")
public class SysPermissionController {

	private static final QSysPermission qSysPermission = QSysPermission.sysPermission;
	@Inject
	JPAQueryFactory jpaQueryFactory;

	@Inject
	JsonWebToken jwt;
	@Inject
	SysPermissionService sysPermissionService;
	
	record SaveRolePermissionVo(String roleId,String permissionIds,String lastPermissionIds) {}
	/**
	 * 保存角色授权
	 *
	 * @return
	 */
//	@RequestMapping(value = "/saveRolePermission", method = RequestMethod.POST)
//	@PreAuthorize("@jps.requiresPermissions('system:permission:saveRole')")
	@POST
	@Path("/saveRolePermission")
	public Response saveRolePermission(SaveRolePermissionVo vo) {
		sysPermissionService.saveRolePermission(vo.roleId(), vo.permissionIds(), vo.lastPermissionIds());
		return Response.ok().entity(Result.ok("保存成功！")).build();
	}
	
	/**
	 * 查询角色授权
	 *
	 * @return
	 */
//	@RequestMapping(value = "/queryRolePermission", method = RequestMethod.GET)
	@GET
	@Path("/queryRolePermission")
	public Response queryRolePermission(@QueryParam("roleId") @NotNull String roleId) {
		List<SysRolePermission> list = SysRolePermission.list("roleId", roleId);
		List<String> pids = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(list)) {
			pids = list.stream().map(SysRolePermission::getPermissionId).distinct().collect(Collectors.toList());
		}
		return Response.ok().entity(Result.ok(pids)).build();
	}

	/**
	 * 添加菜单
	 * 
	 * @param permission
	 * @return
	 */
//	@PreAuthorize("@jps.requiresPermissions('system:permission:add')")
//	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@POST
	@Path("/add")
	public Response add(SysPermissionDTO permission) {
		permission = PermissionDataUtil.intelligentProcessData(permission);
		sysPermissionService.addPermission(permission);
		return Response.ok().entity(Result.ok(permission)).build();
	}

	/**
	 * 编辑菜单
	 * 
	 * @param dto
	 * @return
	 */
//	@PreAuthorize("@jps.requiresPermissions('system:permission:edit')")
//	@RequestMapping(value = "/edit", method = { RequestMethod.PUT, RequestMethod.POST })
	@POST
	@Path("/edit")
	public Response edit(SysPermissionDTO dto) {
		SysPermission p = SysPermission.findById(dto.getId());
		if (p == null) {
			return Response.ok().entity(Result.error("未找到菜单信息")).build();
		}
		dto = PermissionDataUtil.intelligentProcessData(dto);
		SysPermission editPermission = sysPermissionService.editPermission(dto);
		return Response.ok().entity(Result.ok(editPermission)).build();
	}

	/**
	 * 加载数据节点
	 *
	 * @return
	 */
	// @RequiresPermissions("system:permission:list")
	@GET
	@Path("/list")
	public Response list(@QueryParam("name") String name) {
		BooleanBuilder booleanBuilder = new BooleanBuilder();
		booleanBuilder.and(qSysPermission.delFlag.eq(false));
		if (StringUtils.isNotBlank(name)) {
			booleanBuilder.and(qSysPermission.name.like(name));
		}
		List<SysPermission> list = jpaQueryFactory.selectFrom(qSysPermission).where(booleanBuilder)
				.orderBy(qSysPermission.sortNo.asc()).fetch();
		List<SysPermissionTree> treeList = new ArrayList<>();
		// 如果有菜单名查询条件，则平铺数据 不做上下级
		if (StringUtils.isNotBlank(name)) {
			if (list != null && list.size() > 0) {
				treeList = list.stream().map(e -> {
					e.setIsLeaf(true);
					return new SysPermissionTree(e);
				}).collect(Collectors.toList());
			}
		} else {
			getTreeList(treeList, list, null);
		}
		return Response.ok().entity(Result.ok(treeList)).build();
	}

	private void getTreeList(List<SysPermissionTree> treeList, List<SysPermission> metaList, SysPermissionTree temp) {
		for (SysPermission permission : metaList) {
			String tempPid = permission.getParentId();
			SysPermissionTree tree = new SysPermissionTree(permission);
			if (temp == null && StringUtils.isBlank(tempPid)) {
				treeList.add(tree);
				if (!tree.getIsLeaf()) {
					getTreeList(treeList, metaList, tree);
				}
			} else if (temp != null && tempPid != null && tempPid.equals(temp.getId())) {
				temp.getChildren().add(tree);
				if (!tree.getIsLeaf()) {
					getTreeList(treeList, metaList, tree);
				}
			}

		}
	}

	/**
	 * 检测菜单路径是否存在
	 * 
	 * @param id
	 * @param url
	 * @return
	 */
	@GET
	@Path("/checkPermDuplication")
	public Response checkPermDuplication(@QueryParam("id") String id, @QueryParam("url") @NotNull String url,
			@QueryParam("alwaysShow") @NotNull Boolean alwaysShow) {
		BooleanBuilder booleanBuilder = new BooleanBuilder();
		if (StringUtils.isNotBlank(id)) {
			booleanBuilder.and(qSysPermission.id.ne(id));
		}
		booleanBuilder.and(qSysPermission.url.eq(url));
		booleanBuilder.and(qSysPermission.alwaysShow.eq(alwaysShow));
		int size = jpaQueryFactory.from(qSysPermission).where(booleanBuilder).fetch().size();
		if (size == 0) {
			return Response.ok().entity(Result.ok("该值可用！")).build();
		}
		return Response.ok().entity(Result.error("访问路径不允许重复，请重定义！")).build();
	}

	record AuthDTO(String action, String type, String describe) {
	}

	record AuthAllDTO(String action, String status, String type, String describe) {
	}

	record PermCodeDTO(List<String> codeList, List<AuthDTO> auth, List<AuthAllDTO> allAuth, Boolean sysSafeMode) {
	}

	/**
	 * 【vue3专用】获取 1、查询用户拥有的按钮/表单访问权限 2、所有权限 (菜单权限配置) 3、系统安全模式 (开启则online报表的数据源必填)
	 */
	@GET
	@Path("/getPermCode")
	public Response getPermCode() {
		Object claim = jwt.getClaim(Claims.preferred_username.name());
		if (claim == null) {
			return Response.ok().entity(Result.error("请登录系统！")).build();
		}
		SysUser user = BaseEntityListener.getUser(claim.toString());

		List<SysPermission> metaList = sysPermissionService.queryByUser(user.getUsername());
		// 按钮权限（用户拥有的权限集合）
		List<String> codeList = metaList.stream()
				.filter((permission) -> CommonConstant.MENU_TYPE_2.equals(permission.getMenuType())
						&& CommonConstant.STATUS_1.equals(permission.getStatus()))
				.collect(ArrayList::new, (list, permission) -> list.add(permission.getPerms()), ArrayList::addAll);
		List<AuthDTO> authDTOs = getAuthDTOs(metaList);
		// 查询所有的权限
		List<AuthAllDTO> authAllDTOs = getAuthAllDTOs();
		PermCodeDTO permCodeDTO = new PermCodeDTO(codeList, authDTOs, authAllDTOs, null);
		return Response.ok().entity(Result.ok(permCodeDTO)).build();
	}

	private List<AuthAllDTO> getAuthAllDTOs() {
		List<SysPermission> allAuthList = SysPermission.list("delFlag=false and menuType=2");
		List<AuthAllDTO> authAllDTOs = new ArrayList<>();
		for (SysPermission permission : allAuthList) {
			AuthAllDTO json = new AuthAllDTO(permission.getPerms(), permission.getStatus(), permission.getPermsType(),
					permission.getName());
			authAllDTOs.add(json);
		}
		return authAllDTOs;
	}

	private List<AuthDTO> getAuthDTOs(List<SysPermission> metaList) {
		List<AuthDTO> authDTOs = new ArrayList<>();
		for (SysPermission permission : metaList) {
			if (permission.getMenuType() == null) {
				continue;
			}
			AuthDTO json = null;
			if (permission.getMenuType().equals(CommonConstant.MENU_TYPE_2)
					&& CommonConstant.STATUS_1.equals(permission.getStatus())) {
				json = new AuthDTO(permission.getPerms(), permission.getPermsType(), permission.getName());
				authDTOs.add(json);
			}
		}
		return authDTOs;
	}

	record PermByTokenDTO(List<MenuDTO> menu, List<AuthDTO> auth, List<AuthAllDTO> allAuth, Boolean sysSafeMode) {
	}

	/**
	 * 查询用户拥有的菜单权限和按钮权限
	 *
	 * @return
	 */
	@GET
	@Path("/getUserPermissionByToken")
	public Response getUserPermissionByToken() {
		Object claim = jwt.getClaim(Claims.preferred_username.name());
		if (claim == null) {
			return Response.ok().entity(Result.error("请登录系统！")).build();
		}
		SysUser user = BaseEntityListener.getUser(claim.toString());

		List<SysPermission> metaList = sysPermissionService.queryByUser(user.getUsername());
		List<MenuDTO> menuDTOs = new ArrayList<>();
		getPermission(metaList, menuDTOs, null);
		handleFirstLevelMenuHidden(menuDTOs);
		List<AuthDTO> authDTOs = getAuthDTOs(metaList);
		List<AuthAllDTO> authAllDTOs = getAuthAllDTOs();
		PermByTokenDTO permByTokenDTO = new PermByTokenDTO(menuDTOs, authDTOs, authAllDTOs, null);
		return Response.ok().entity(Result.ok(permByTokenDTO)).build();
	}

	/**
	 * 一级菜单的子菜单全部是隐藏路由，则一级菜单不显示
	 * 
	 * @param jsonArray
	 */
	private void handleFirstLevelMenuHidden(List<MenuDTO> menuDTOs) {
		menuDTOs.forEach(m -> {
			if (CollectionUtils.isNotEmpty(m.getChildren())) {
				List<MenuDTO> childrens = m.getChildren().stream().filter(c -> c.getHidden() == null || !c.getHidden())
						.collect(Collectors.toList());
				if (CollectionUtils.isEmpty(childrens)) {
					m.setHidden(true);
					// vue3版本兼容代码
					MetaDTO metaDTO = new MetaDTO();
					metaDTO.setHideMenu(true);
					m.setMeta(metaDTO);
				}
			}
		});
	}

	/**
	 * @param metaList
	 */
	private void getPermission(List<SysPermission> metaList, List<MenuDTO> menuDTOs, MenuDTO menuParentDTO) {
		for (SysPermission permission : metaList) {
			if (permission.getMenuType() == null) {
				continue;
			}
			MenuDTO menuDTO = getMenu(permission);
			if (menuDTO == null) {
				continue;
			}
			if (menuParentDTO == null && StringUtils.isBlank(permission.getParentId())) {
				menuDTOs.add(menuDTO);
				if (!permission.getIsLeaf()) {
					getPermission(metaList, menuDTOs, menuDTO);
				}
			} else if (menuParentDTO != null && StringUtils.isNotBlank(permission.getParentId())
					&& permission.getParentId().equals(menuParentDTO.getId())) {
				// 类型( 0：一级菜单 1：子菜单 2：按钮 )
				if (permission.getMenuType().equals(CommonConstant.MENU_TYPE_2)) {
					MetaDTO metaDTO = menuParentDTO.getMeta();
					if (CollectionUtils.isNotEmpty(metaDTO.getPermissionList())) {
						metaDTO.getPermissionList().add(menuDTO);
					} else {
						List<MenuDTO> permissionList = new ArrayList<>();
						permissionList.add(menuDTO);
						metaDTO.setPermissionList(permissionList);
					}
					// 类型( 0：一级菜单 1：子菜单 2：按钮 )
				} else if (permission.getMenuType().equals(CommonConstant.MENU_TYPE_1)
						|| permission.getMenuType().equals(CommonConstant.MENU_TYPE_0)) {
					if (CollectionUtils.isNotEmpty(menuParentDTO.getChildren())) {
						menuParentDTO.getChildren().add(menuDTO);
					} else {
						List<MenuDTO> children = new ArrayList<>();
						children.add(menuDTO);
						menuParentDTO.setChildren(children);
					}
					if (!permission.getIsLeaf()) {
						getPermission(metaList, menuDTOs, menuDTO);
					}
				}
			}
		}
	}

	/**
	 * @param permission
	 */
	private MenuDTO getMenu(SysPermission permission) {
//		JSONObject json = new JSONObject();
		MenuDTO menuDTO = new MenuDTO();
		// 类型(0：一级菜单 1：子菜单 2：按钮)
		if (permission.getMenuType().equals(CommonConstant.MENU_TYPE_2)) {
			return null;
		} else if (permission.getMenuType().equals(CommonConstant.MENU_TYPE_0)
				|| permission.getMenuType().equals(CommonConstant.MENU_TYPE_1)) {
//			json.put("id", permission.getId());
			menuDTO.setId(permission.getId());
			if (permission.getIsRoute() != null && permission.getIsRoute()) {
				// 表示生成路由
//				json.put("route", "1");
				menuDTO.setRoute("1");
			} else {
				// 表示不生成路由
//				json.put("route", "0");
				menuDTO.setRoute("0");
			}

//			if (isWwwHttpUrl(permission.getUrl())) {
//				json.put("path", Md5Util.md5Encode(permission.getUrl(), "utf-8"));
//			} else {
//				json.put("path", permission.getUrl());
//			}
			menuDTO.setPath(permission.getUrl());

			// 重要规则：路由name (通过URL生成路由name,路由name供前端开发，页面跳转使用)
			if (StringUtils.isNotBlank(permission.getComponentName())) {
//				json.put("name", permission.getComponentName());
				menuDTO.setName(permission.getComponentName());
			} else {
//				json.put("name", urlToRouteName(permission.getUrl()));
				menuDTO.setName(urlToRouteName(permission.getUrl()));
			}

//			JSONObject meta = new JSONObject();
			MetaDTO metaDTO = new MetaDTO();
			// 是否隐藏路由，默认都是显示的
			if (permission.getHidden() != null && permission.getHidden()) {
//				json.put("hidden", true);
				menuDTO.setHidden(true);
				// vue3版本兼容代码
//                meta.put("hideMenu",true);
				metaDTO.setHideMenu(true);
			}
			// 聚合路由
			if (permission.getAlwaysShow() != null && permission.getAlwaysShow()) {
//				json.put("alwaysShow", true);
				menuDTO.setAlwaysShow(true);
			}
//			json.put("component", permission.getComponent());
			menuDTO.setComponent(permission.getComponent());
			// 由用户设置是否缓存页面 用布尔值
			if (permission.getKeepAlive() != null && permission.getKeepAlive()) {
//				meta.put("keepAlive", true);
				metaDTO.setKeepAlive(true);
			} else {
//				meta.put("keepAlive", false);
				metaDTO.setKeepAlive(false);
			}

			/* update_begin author:wuxianquan date:20190908 for:往菜单信息里添加外链菜单打开方式 */
			// 外链菜单打开方式
			if (permission.getInternalOrExternal() != null && permission.getInternalOrExternal()) {
//				meta.put("internalOrExternal", true);
				metaDTO.setInternalOrExternal(true);
			} else {
//				meta.put("internalOrExternal", false);
				metaDTO.setInternalOrExternal(false);
			}
			/* update_end author:wuxianquan date:20190908 for: 往菜单信息里添加外链菜单打开方式 */

//			meta.put("title", permission.getName());
			metaDTO.setTitle(permission.getName());

			// update-begin--Author:scott Date:20201015 for：路由缓存问题，关闭了tab页时再打开就不刷新 #842
			String component = permission.getComponent();
			if (StringUtils.isNotBlank(permission.getComponentName()) || StringUtils.isNotBlank(component)) {
//				meta.put("componentName", oConvertUtils.getString(permission.getComponentName(),component.substring(component.lastIndexOf("/")+1)));
				metaDTO.setComponentName(StringUtils.isBlank(permission.getComponentName())
						? component.substring(component.lastIndexOf("/") + 1)
						: permission.getComponentName());
			}
			// update-end--Author:scott Date:20201015 for：路由缓存问题，关闭了tab页时再打开就不刷新 #842

			if (StringUtils.isBlank(permission.getParentId())) {
				// 一级菜单跳转地址
//				json.put("redirect", permission.getRedirect());
				menuDTO.setRedirect(permission.getRedirect());
				if (StringUtils.isNotBlank(permission.getIcon())) {
//					meta.put("icon", permission.getIcon());
					metaDTO.setIcon(permission.getIcon());
				}
			} else {
				if (StringUtils.isNotBlank(permission.getIcon())) {
//					meta.put("icon", permission.getIcon());
					metaDTO.setIcon(permission.getIcon());
				}
			}
//			if (isWwwHttpUrl(permission.getUrl())) {
//				meta.put("url", permission.getUrl());
//			}
			// update-begin--Author:sunjianlei Date:20210918 for：新增适配vue3项目的隐藏tab功能
			if (permission.getHideTab() != null && permission.getHideTab()) {
//				meta.put("hideTab", true);
				metaDTO.setHideTab(true);
			}
			// update-end--Author:sunjianlei Date:20210918 for：新增适配vue3项目的隐藏tab功能
//			json.put("meta", meta);
			menuDTO.setMeta(metaDTO);
		}
		return menuDTO;
	}

	/**
	 * 通过URL生成路由name（去掉URL前缀斜杠，替换内容中的斜杠‘/’为-） 举例： URL = /isystem/role RouteName =
	 * isystem-role
	 *
	 * @return
	 */
	private String urlToRouteName(String url) {
		if (StringUtils.isNotBlank(url)) {
			if (url.startsWith(SymbolConstant.SINGLE_SLASH)) {
				url = url.substring(1);
			}
			url = url.replace("/", "-");

			// 特殊标记
			url = url.replace(":", "@");
			return url;
		} else {
			return null;
		}
	}
}
