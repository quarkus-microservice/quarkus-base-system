package com.weir.quarkus.sys.resource;

import lombok.extern.slf4j.Slf4j;

import jakarta.annotation.Resource;
import jakarta.annotation.security.PermitAll;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.util.*;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

//import com.alibaba.fastjson.JSONObject;
import com.weir.quarkus.base.system.handler.BaseEntityListener;
import com.weir.quarkus.base.system.jwt.TokenService;
import com.weir.quarkus.base.system.redis.ValueCommandsService;
import com.weir.quarkus.base.system.vo.ResultDataVo;
import com.weir.quarkus.base.system.vo.ResultVo;
import com.weir.quarkus.sys.config.SysConfig;
import com.weir.quarkus.sys.entity.SysUser;
import com.weir.quarkus.sys.rule.FillRuleService;
import com.weir.quarkus.sys.rule.IFillRuleHandler;
import com.weir.quarkus.sys.rule.OrderNumberRule;
import com.weir.quarkus.sys.service.SysPermissionService;
import com.weir.quarkus.sys.utils.Md5Util;
import com.weir.quarkus.sys.utils.PasswordUtil;
import com.weir.quarkus.sys.utils.RandImageUtil;
import com.weir.quarkus.sys.vo.CommonConstant;
import com.weir.quarkus.sys.vo.Result;
import com.weir.quarkus.sys.vo.SysLoginModel;
import com.weir.quarkus.sys.vo.UserInfo;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.jackson.ObjectMapperCustomizer;
import io.vertx.core.json.JsonObject;

/**
 * @Author scott
 * @since 2018-12-17
 */
@Path("/sys")
@Tag(name="用户登录")
@Slf4j
public class LoginController {
	
	@Inject ValueCommandsService valueCommandsService;
	@Inject SysConfig sysConfig;
	@Inject TokenService tokenService;
	@Inject JsonWebToken jwt;
	@Inject SysPermissionService sysPermissionService;

	private final String BASE_CHECK_CODES = "qwertyuiplkjhgfdsazxcvbnmQWERTYUPLKJHGFDSAZXCVBNM1234567890";
	@Context HttpServletRequest httpServletRequest;
	
	@Inject
    Instance<IFillRuleHandler> instance;
	@Inject
	FillRuleService fillRuleService;
	
	/**
	 * 使用spring authorization server提供的各类登录接口
	 * @param sysLoginModel
	 * @return
	 */
	@Operation(summary = "登录接口")
	@POST
	@Path(value = "/login")
	public Response login(SysLoginModel sysLoginModel){
		String username = sysLoginModel.getUsername();
		String password = sysLoginModel.getPassword();
//		if(isLoginFailOvertimes(username)){
//			return Response.ok().entity(Result.error("该用户登录失败次数过多，请于10分钟后再次登录！")).build();
//		}
//        String captcha = sysLoginModel.getCaptcha();
//        if(captcha==null){
//            return Response.ok()
//					.entity(Result.error("验证码无效")).build();
//        }
//        String lowerCaseCaptcha = captcha.toLowerCase();
//		// 加入密钥作为混淆，避免简单的拼接，被外部利用，用户自定义该密钥即可
//        String origin = lowerCaseCaptcha+sysLoginModel.getCheckKey()+sysConfig.signatureSecret();
//		String realKey = Md5Util.md5Encode(origin, "utf-8");
//		Object checkCode = valueCommandsService.get(realKey);
//		if(checkCode==null || !checkCode.toString().equals(lowerCaseCaptcha)) {
//            log.warn("验证码错误，key= {} , Ui checkCode= {}, Redis checkCode = {}", sysLoginModel.getCheckKey(), lowerCaseCaptcha, checkCode);
//			return Response.ok()
//					.entity(Result.error("验证码错误")).build();
//		}
		
		//1. 校验用户是否有效
		Optional<SysUser> sysUserOptional = SysUser.find("username", username).firstResultOptional();
		if (sysUserOptional.isEmpty()) {
			return Response.ok().entity(Result.error("该用户不存在，请注册")).build();
		}
		SysUser sysUser = sysUserOptional.get();
		//情况2：根据用户信息查询，该用户已注销
		if (CommonConstant.DEL_FLAG_1.equals(sysUser.getDelFlag())) {
			return Response.ok().entity(Result.error("该用户已注销")).build();
		}
		//情况3：根据用户信息查询，该用户已冻结
		if (CommonConstant.USER_FREEZE.equals(sysUser.getStatus())) {
			return Response.ok().entity(Result.error("该用户已冻结")).build();
		}

		//2. 校验用户名或密码是否正确
		String userpassword = PasswordUtil.encrypt(username, password, sysUser.getSalt());
		String syspassword = sysUser.getPassword();
		if (!syspassword.equals(userpassword)) {
			addLoginFailOvertimes(username);
			return Response.ok().entity(Result.error("用户名或密码错误")).build();
		}
				
		//用户登录信息
		UserInfo userInfo = userInfo(sysUser);
//		valueCommandsService.del(realKey);
		valueCommandsService.del(CommonConstant.LOGIN_FAIL + username);
		return Response.ok().entity(Result.ok(userInfo)).build();
	}
	
	/**
	 * 退出登录
	 * @param request
	 * @param response
	 * @return
	 */
	@GET
	@Path(value = "/logout")
	public Response logout() {
		//用户退出逻辑
	    String token = httpServletRequest.getHeader(CommonConstant.X_ACCESS_TOKEN);
	    if(StringUtils.isEmpty(token)) {
//	    	return Result.error("退出登录失败！");
	    	return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new Result<>(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "退出登录失败！")).build();
	    }
//	    String username = JwtUtil.getUsername(token);
//		LoginUser sysUser = sysBaseApi.getUserByName(username);
		Object claim = jwt.getClaim(Claims.preferred_username.name());
		if (claim == null) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new Result<>(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Token无效!")).build();
		}
		SysUser sysUser = new JsonObject(new JsonObject(claim.toString()).getValue("user").toString()).mapTo(SysUser.class);
	    if(sysUser!=null) {
			//update-begin--Author:wangshuai  Date:20200714  for：登出日志没有记录人员
//			baseCommonService.addLog("用户名: "+sysUser.getRealname()+",退出成功！", CommonConstant.LOG_TYPE_1, null,sysUser);
			//update-end--Author:wangshuai  Date:20200714  for：登出日志没有记录人员
//	    	log.info(" 用户名:  "+sysUser.getRealname()+",退出成功！ ");
	    	//清空用户登录Token缓存
//	    	redisUtil.del(CommonConstant.PREFIX_USER_TOKEN + token);
	    	valueCommandsService.del(CommonConstant.PREFIX_USER_TOKEN + token);
	    	//清空用户登录Shiro权限缓存
//			redisUtil.del(CommonConstant.PREFIX_USER_SHIRO_CACHE + sysUser.getId());
			//清空用户的缓存信息（包括部门信息），例如sys:cache:user::<username>
//			redisUtil.del(String.format("%s::%s", CacheConstant.SYS_USERS_CACHE, sysUser.getUsername()));
			//调用shiro的logout
//			SecurityUtils.getSubject().logout();

//			OAuth2Authorization authorization = authorizationService.findByToken(token, OAuth2TokenType.ACCESS_TOKEN);

			// 清空用户信息
//			cacheManager.getCache("user_details").evict(authorization.getPrincipalName());
			// 清空access token
//			authorizationService.remove(authorization);

//	    	return Result.ok("退出登录成功！");
	    	return Response.ok().entity(new ResultDataVo<>(200, "退出登录成功！")).build();
	    }else {
	    	return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new Result<>(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Token无效!")).build();
	    }
	}
	
	


	/**
	 * 用户信息
	 *
	 * @param sysUser
	 * @param result
	 * @return
	 */
	private UserInfo userInfo(SysUser sysUser) {
//		String username = sysUser.getUsername();
//		String syspassword = sysUser.getPassword();
		// 获取用户部门信息
//		JSONObject obj = new JSONObject(new LinkedHashMap<>());
		UserInfo obj = new UserInfo();
		//1.生成token
//		String token = JwtUtil.sign(username, syspassword);
//		// 设置token缓存有效时间
//		redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
//		redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME * 2 / 1000);
//		obj.put("token", token);
		
		// 权限集合
		List<String> codes = sysPermissionService.getPerms(sysUser.getUsername());
		String ujson = new JsonObject().put("user", sysUser).toString();
		String token = tokenService.generateToken(sysUser.getPassword(), ujson, codes);
		obj.setToken(token);
		//2.设置登录租户
//		Result<JSONObject> loginTenantError = sysUserService.setLoginTenant(sysUser, obj, username,result);
//		if (loginTenantError != null) {
//			return loginTenantError;
//		}

		//3.设置登录用户信息
//		obj.put("userInfo", sysUser);
		obj.setUserInfo(sysUser);
		
		return obj;
	}

	/**
	 * 后台生成图形验证码 ：有效
	 * @param response
	 * @param key
	 */
	@Operation(summary ="获取验证码")
	@GET
	@Path(value = "/randomImage/{key}")
	public Response randomImage(@PathParam("key") String key){
//		Result<String> res = new Result<String>();
		try {
			//生成验证码
//			String code = RandomUtil.randomString(BASE_CHECK_CODES,4);
			String code = RandomStringUtils.randomAlphanumeric(4);
			//存到redis中
			String lowerCaseCode = code.toLowerCase();
			
			// 加入密钥作为混淆，避免简单的拼接，被外部利用，用户自定义该密钥即可
//			String origin = lowerCaseCode+key+jeecgBaseConfig.getSignatureSecret();
			String origin = lowerCaseCode+key+sysConfig.signatureSecret();
			String realKey = Md5Util.md5Encode(origin, "utf-8");
            
//			redisUtil.set(realKey, lowerCaseCode, 60);
			valueCommandsService.set(realKey, lowerCaseCode, 60);
			log.info("获取验证码，Redis key = {}，checkCode = {}", realKey, code);
			//返回前端
			String base64 = RandImageUtil.generate(code);
//			res.setSuccess(true);
//			res.setResult(base64);
			return Response.ok().entity(Result.okT(base64)).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
//			res.error500("获取验证码失败,请检查redis配置!");
//			return res;
		}
//		return res;
		return Response.ok().entity(Result.error("获取验证码失败,请检查redis配置!")).build();
	}

	/**
	 * 登录失败超出次数5 返回true
	 * @param username
	 * @return
	 */
	private boolean isLoginFailOvertimes(String username){
		String key = CommonConstant.LOGIN_FAIL + username;
//		Object failTime = redisUtil.get(key);
		Object failTime = valueCommandsService.get(key);
		if(failTime!=null){
			Integer val = Integer.parseInt(failTime.toString());
			if(val>5){
				return true;
			}
		}
		return false;
	}

	/**
	 * 记录登录失败次数
	 * @param username
	 */
	private void addLoginFailOvertimes(String username){
		String key = CommonConstant.LOGIN_FAIL + username;
//		Object failTime = redisUtil.get(key);
		Object failTime = valueCommandsService.get(key);
		Integer val = 0;
		if(failTime!=null){
			val = Integer.parseInt(failTime.toString());
		}
		// 10分钟
//		redisUtil.set(key, ++val, 10);
		valueCommandsService.set(key, ++val, 600);
	}

}