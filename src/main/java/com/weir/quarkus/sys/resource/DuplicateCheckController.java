package com.weir.quarkus.sys.resource;

import lombok.extern.slf4j.Slf4j;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.weir.quarkus.sys.service.SysDictService;
import com.weir.quarkus.sys.service.TableNameService;
import com.weir.quarkus.sys.vo.DuplicateCheckVo;
import com.weir.quarkus.sys.vo.Result;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

/**
 * @Title: DuplicateCheckAction
 * @Description: 重复校验工具
 * @Author 张代浩
 * @Date 2019-03-25
 * @Version V1.0
 */
@Slf4j
@Path("/sys/duplicate")
@Tag(name="重复校验")
public class DuplicateCheckController {
	@Inject EntityManager em;
	@Inject SysDictService sysDictService;
	@Inject TableNameService tableNameService;
	/**
	 * 校验数据是否在系统中是否存在
	 * 
	 * @return
	 */
//	@RequestMapping(value = "/check", method = RequestMethod.GET)
	@Operation(summary = "重复校验接口")
	@GET
	@Path("/check")
	public Response doDuplicateCheck(@BeanParam DuplicateCheckVo duplicateCheckVo) {
//		List<String> allColumnNames = tableNameService.getAllColumnNames("sys_dict");
//		System.out.println("----------------allColumnNames--------------------"+allColumnNames);
//		return Response.ok().entity(Result.ok("")).build();
		boolean duplicateCheckData = sysDictService.duplicateCheckData(duplicateCheckVo);
		return Response.ok().entity(Result.ok(duplicateCheckData)).build();
	}
    

}
