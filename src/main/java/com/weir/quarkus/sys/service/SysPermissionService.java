/** 
 * @Title: SysPermissionService.java 
 * @Package com.weir.quarkus.sys.service 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月8日 16:23:58 
 * @version V1.0 
 */
package com.weir.quarkus.sys.service;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.weir.quarkus.base.system.utils.IpUtil;
import com.weir.quarkus.sys.entity.QSysPermission;
import com.weir.quarkus.sys.entity.QSysRole;
import com.weir.quarkus.sys.entity.QSysRolePermission;
import com.weir.quarkus.sys.entity.QSysUser;
import com.weir.quarkus.sys.entity.QSysUserRole;
import com.weir.quarkus.sys.entity.SysPermission;
import com.weir.quarkus.sys.entity.SysRolePermission;
import com.weir.quarkus.sys.mapper.SysPermissionMapper;
import com.weir.quarkus.sys.utils.MergeEntityCopyUtil;
import com.weir.quarkus.sys.vo.CommonConstant;
import com.weir.quarkus.sys.vo.SysPermissionDTO;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Context;

/**
 * @Title: SysPermissionService.java
 * @Package com.weir.quarkus.sys.service
 * @Description: TODO(用一句话描述该文件做什么)
 * @author weir
 * @date 2024年3月8日 16:23:58
 * @version V1.0
 */
@ApplicationScoped
public class SysPermissionService {

	private static final QSysPermission qSysPermission = QSysPermission.sysPermission;
	private static final QSysRolePermission qSysRolePermission = QSysRolePermission.sysRolePermission;
	private static final QSysUserRole qSysUserRole = QSysUserRole.sysUserRole;
	private static final QSysRole qSysRole = QSysRole.sysRole;
	private static final QSysUser qSysUser = QSysUser.sysUser;

	@Inject
	JPAQueryFactory jpaQueryFactory;
	@Inject
	JPAQuery jPAQuery;
	@Inject
	EntityManager entityManager;
	@Inject
	SysPermissionMapper sysPermissionMapper;
	@Context
	HttpServletRequest request;

	/**
	 * 
	 * @param roleId
	 * @param permissionIds
	 * @param lastPermissionIds old权限
	 */
	@Transactional
	public void saveRolePermission(String roleId, String permissionIds, String lastPermissionIds) {
		String ip = "";
		try {
			ip = IpUtil.getSingleIp(request);
		} catch (Exception e) {
			ip = "127.0.0.1";
		}
		if (StringUtils.isNotBlank(permissionIds)) {			
			List<String> newIds = Arrays.asList(permissionIds.split(","));
			if (StringUtils.isNotBlank(lastPermissionIds)) {				
				List<String> oldIds = Arrays.asList(lastPermissionIds.split(","));
				newIds.removeAll(oldIds);
			}
			if (CollectionUtils.isNotEmpty(newIds)) {
				List<SysRolePermission> rolePermissions = new ArrayList<>();
				for (String id : newIds) {
					SysRolePermission rolepms = new SysRolePermission(roleId, id, LocalDateTime.now(), ip);
					rolePermissions.add(rolepms);
				}
				SysRolePermission.persist(rolePermissions);
			}
		}
		if (StringUtils.isNotBlank(lastPermissionIds)) {
			List<String> oldIds2 = Arrays.asList(lastPermissionIds.split(","));
			if (StringUtils.isNotBlank(permissionIds)) {				
				List<String> newIds2 = Arrays.asList(permissionIds.split(","));
				oldIds2.removeAll(newIds2);
			}
			if (CollectionUtils.isNotEmpty(oldIds2)) {
				SysRolePermission.delete("id in (?1)", oldIds2);
			}
		}
	}

	@Transactional
	public SysPermission editPermission(SysPermissionDTO dto) {
		SysPermission p = SysPermission.findById(dto.getId());
		// Step1.判断是否是一级菜单，是的话清空父菜单ID
		if (CommonConstant.MENU_TYPE_0.equals(dto.getMenuType())) {
			dto.setParentId("");
		}
		// Step2.判断菜单下级是否有菜单，无则设置为叶子节点
		long count = SysPermission.count("parentId", dto.getId());
		if (count == 0) {
			dto.setIsLeaf(true);
		}

		MergeEntityCopyUtil.updateEntity(dto, p);
		entityManager.merge(p);

		// 如果当前菜单的父菜单变了，则需要修改新父菜单和老父菜单的，叶子节点状态
		String pid = dto.getParentId();
		boolean flag = (StringUtils.isNotBlank(pid) && !pid.equals(p.getParentId()))
				|| StringUtils.isBlank(pid) && StringUtils.isNotBlank(p.getParentId());
		if (flag) {
			// a.设置新的父菜单不为叶子节点
			SysPermission.update("isLeaf = false where id=?1", pid);
			// b.判断老的菜单下是否还有其他子菜单，没有的话则设置为叶子节点
			long cc = SysPermission.count("parentId", p.getParentId());
			if (cc == 0) {
				if (StringUtils.isNotBlank(p.getParentId())) {
					SysPermission.update("isLeaf = true where id=?1", p.getParentId());
				}
			}
		}
		return SysPermission.findById(p.getId());
	}

	public static void main(String[] args) {
		SysPermission p = new SysPermission();
		p.setId("22");
		p.setName("ww");
		SysPermissionDTO dto = new SysPermissionDTO();
		dto.setId("22");
		dto.setName("44ee");
		dto.setComponent("qq222");
		dto.setMenuType(234);
		Object objP = MergeEntityCopyUtil.updateEntity(dto, p);
		System.out.println(objP);
	}

	@Transactional
	public void addPermission(SysPermissionDTO sysPermission) {
		// 判断是否是一级菜单，是的话清空父菜单
		if (CommonConstant.MENU_TYPE_0.equals(sysPermission.getMenuType())) {
			sysPermission.setParentId(null);
		}
		String pid = sysPermission.getParentId();
		if (StringUtils.isNotBlank(pid)) {
			// 设置父节点不为叶子节点
			SysPermission.update("isLeaf = false where id=?1", pid);
		}
//		sysPermission.setCreateTime(LocalDateTime.now());
		sysPermission.setDelFlag(false);
		sysPermission.setIsLeaf(true);
		SysPermission permission = sysPermissionMapper.toEntity(sysPermission);
		permission.persist();
	}

//	SELECT p.*
//	   FROM  sys_permission p
//	   WHERE (exists(
//				select a.id from sys_role_permission a
//				join sys_role b on a.role_id = b.id
//				join sys_user_role c on c.role_id = b.id
//				join sys_user d on d.id = c.user_id
//				where p.id = a.permission_id AND d.username = 'admin'
//			)
//			or (p.url like '%:code' and p.url like '/online%' and p.hidden = 1)
//			or (p.url like '%:id' and p.url like '/online%' and p.hidden = 1)
//			or p.url = '/online')
//	   and p.del_flag = 0
	public List<SysPermission> queryByUser(String username) {
		if (StringUtils.isEmpty(username)) {
			return null;
		}
		List<SysPermission> metaList = jpaQueryFactory.selectFrom(qSysPermission)
				.where(qSysPermission.delFlag.eq(false).and(JPAExpressions.select(qSysRolePermission.id)
						.from(qSysRolePermission).join(qSysRole).on(qSysRolePermission.roleId.eq(qSysRole.id))
						.join(qSysUserRole).on(qSysRole.id.eq(qSysUserRole.roleId)).join(qSysUser)
						.on(qSysUserRole.userId.eq(qSysUser.id)).where(qSysPermission.id
								.eq(qSysRolePermission.permissionId).and(qSysUser.username.eq(username)))
						.exists()))
				.fetch();
		return metaList;
	}
	
	public List<String> getPerms(String username) {
		List<SysPermission> permissions = jpaQueryFactory.selectFrom(qSysPermission)
		.join(qSysRolePermission).on(qSysRolePermission.permissionId.eq(qSysPermission.id))
		.join(qSysRole).on(qSysRolePermission.roleId.eq(qSysRole.id))
		.join(qSysUserRole).on(qSysRole.id.eq(qSysUserRole.roleId))
		.join(qSysUser).on(qSysUserRole.userId.eq(qSysUser.id))
		.where(qSysUser.username.eq(username)).fetch();
		if (CollectionUtils.isNotEmpty(permissions)) {
			return permissions.stream().filter(p->StringUtils.isNotBlank(p.getPerms())).map(SysPermission::getPerms).distinct().collect(Collectors.toList());
		}
		return null;
	}
}
