/** 
 * @Title: SysDictService.java 
 * @Package com.weir.quarkus.sys.service 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月12日 11:05:07 
 * @version V1.0 
 */
package com.weir.quarkus.sys.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.weir.quarkus.sys.entity.QSysDict;
import com.weir.quarkus.sys.entity.QSysDictItem;
import com.weir.quarkus.sys.entity.QSysUser;
import com.weir.quarkus.sys.entity.SysDict;
import com.weir.quarkus.sys.entity.SysDictItem;
import com.weir.quarkus.sys.utils.SymbolConstant;
import com.weir.quarkus.sys.vo.DictModel;
import com.weir.quarkus.sys.vo.DuplicateCheckVo;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.panache.common.Sort;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

/**
 * @Title: SysDictService.java
 * @Package com.weir.quarkus.sys.service
 * @Description: TODO(用一句话描述该文件做什么)
 * @author weir
 * @date 2024年3月12日 11:05:07
 * @version V1.0
 */
@ApplicationScoped
public class SysDictService {
	
	private static final QSysDict qSysDict = QSysDict.sysDict;
	private static final QSysDictItem qSysDictItem = QSysDictItem.sysDictItem;
	
	@Inject
	JPAQueryFactory jpaQueryFactory;
	@Inject EntityManager em;
	@Inject TableNameService tableNameService;
	
	public Map<String, List<DictModel>> queryAllDictItems() {
		Map<String, List<DictModel>> res = new HashMap<>(5);
		List<SysDict> ls = SysDict.listAll();
		List<SysDictItem> sysDictItemList = SysDictItem.list("status", Sort.by("sortOrder"), 1);
		for (SysDict d : ls) {
			List<DictModel> dictModelList = sysDictItemList.stream().filter(s -> d.getId().equals(s.getDictId())).map(item -> {
				DictModel dictModel = new DictModel();
				dictModel.setText(item.getItemText());
				dictModel.setValue(item.getItemValue());
				return dictModel;
			}).collect(Collectors.toList());
			res.put(d.getDictCode(), dictModelList);
		}
		return res;
	}
	
	@Transactional
	public int updateDel(String id) {
		return em.createNativeQuery("update sys_dict set del_flag = 0 where id = ?1").setParameter(1,id).executeUpdate();
	}
	@Transactional
	public void delPhysically(String id) {
		em.createNativeQuery("delete from sys_dict where id = ?1").setParameter(1,id).executeUpdate();
		SysDictItem.delete("dictId", id);
	}
	public List<SysDict> delList() {
		return em.createNativeQuery("select * from sys_dict where del_flag = 1", SysDict.class).getResultList();
	}
	
	public List<DictModel> queryDictItemsByCode(String code) {
//		select s.item_value as "value",s.item_text as "text",s.item_color as color from sys_dict_item s
//		   where dict_id = (select id from sys_dict where dict_code = #{code})
//		   order by s.sort_order asc, s.create_time DESC;
		List<DictModel> dictModels = jpaQueryFactory.select(
				Projections.fields(DictModel.class,
						qSysDictItem.itemValue.as("value"),qSysDictItem.itemText.as("text"),qSysDictItem.itemColor.as("color"))
						)
		.from(qSysDictItem).join(qSysDict)
		.on(qSysDict.id.eq(qSysDictItem.dictId))
		.where(qSysDict.dictCode.eq(code))
		.orderBy(qSysDictItem.sortOrder.asc(),qSysDictItem.createTime.desc()).fetch();
		return dictModels;
	}

	public boolean duplicateCheckData(DuplicateCheckVo duplicateCheckVo) {
		if (!checkTableNameExist(duplicateCheckVo.getTableName())) {
			return false;
		}
		if (!checkColumnNameExist(duplicateCheckVo.getTableName(), duplicateCheckVo.getFieldName())) {
			return false;
		}
		Object singleResult;
		// 4.执行SQL 查询是否存在值
		if (StringUtils.isNotBlank(duplicateCheckVo.getDataId())) {
			String sql = "SELECT COUNT(1) FROM " + duplicateCheckVo.getTableName() + " WHERE "
					+ duplicateCheckVo.getFieldName() + " = ?1 AND id <> ?2";
			singleResult = em.createNativeQuery(sql, Long.class).setParameter(1, duplicateCheckVo.getFieldVal())
					.setParameter(2, duplicateCheckVo.getDataId()).getSingleResult();
		} else {
			String sql = "SELECT COUNT(1) FROM " + duplicateCheckVo.getTableName() + " WHERE "
					+ duplicateCheckVo.getFieldName() + " = ?1";
			singleResult = em.createNativeQuery(sql, Long.class).setParameter(1, duplicateCheckVo.getFieldVal())
					.getSingleResult();
		}
		if (singleResult == null || singleResult.equals(0)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 验证表是否存在
	 * 
	 * @param tableName 表名
	 * @return （false：不存在，true：存在）
	 */
	public boolean checkTableNameExist(String tableName) {
		List<String> allTableNames = tableNameService.getAllTableNames();
		return allTableNames.contains(tableName) ? true : false;
	}

	public boolean checkColumnNameExist(String tableName, String columnName) {
		List<String> allColumnNames = tableNameService.getAllColumnNames(tableName);
		return allColumnNames.contains(columnName) ? true : false;
	}
}
