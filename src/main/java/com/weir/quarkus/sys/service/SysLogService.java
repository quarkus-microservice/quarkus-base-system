/** 
 * @Title: SysPermissionService.java 
 * @Package com.weir.quarkus.sys.service 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月8日 16:23:58 
 * @version V1.0 
 */
package com.weir.quarkus.sys.service;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.weir.quarkus.base.system.utils.IpUtil;
import com.weir.quarkus.sys.entity.QSysDepart;
import com.weir.quarkus.sys.entity.QSysPermission;
import com.weir.quarkus.sys.entity.QSysRole;
import com.weir.quarkus.sys.entity.QSysRolePermission;
import com.weir.quarkus.sys.entity.QSysUser;
import com.weir.quarkus.sys.entity.QSysUserRole;
import com.weir.quarkus.sys.entity.SysDepart;
import com.weir.quarkus.sys.entity.SysLog;
import com.weir.quarkus.sys.entity.SysPermission;
import com.weir.quarkus.sys.entity.SysRolePermission;
import com.weir.quarkus.sys.entity.SysUser;
import com.weir.quarkus.sys.mapper.SysPermissionMapper;
import com.weir.quarkus.sys.utils.MergeEntityCopyUtil;
import com.weir.quarkus.sys.utils.SymbolConstant;
import com.weir.quarkus.sys.vo.CommonConstant;
import com.weir.quarkus.sys.vo.SysDepartTreeModel;
import com.weir.quarkus.sys.vo.SysPermissionDTO;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Context;

/**
 * @Title: SysPermissionService.java
 * @Package com.weir.quarkus.sys.service
 * @Description: TODO(用一句话描述该文件做什么)
 * @author weir
 * @date 2024年3月8日 16:23:58
 * @version V1.0
 */
@ApplicationScoped
public class SysLogService {

	@Transactional
	public void saveLog(SysLog log) {
		log.persist();
	}
}
