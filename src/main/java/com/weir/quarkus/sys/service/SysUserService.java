/** 
 * @Title: SysUserService.java 
 * @Package com.weir.quarkus.sys.service 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月14日 16:06:03 
 * @version V1.0 
 */
package com.weir.quarkus.sys.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.weir.quarkus.sys.entity.QSysDepart;
import com.weir.quarkus.sys.entity.QSysUser;
import com.weir.quarkus.sys.entity.QSysUserDepart;
import com.weir.quarkus.sys.entity.QSysUserRole;
import com.weir.quarkus.sys.entity.SysDepart;
import com.weir.quarkus.sys.entity.SysUser;
import com.weir.quarkus.sys.entity.SysUserDepart;
import com.weir.quarkus.sys.entity.SysUserPosition;
import com.weir.quarkus.sys.entity.SysUserRole;
import com.weir.quarkus.sys.query.SysUserQuery;
import com.weir.quarkus.sys.utils.MergeEntityCopyUtil;
import com.weir.quarkus.sys.utils.PageUtil;
import com.weir.quarkus.sys.utils.PasswordUtil;
import com.weir.quarkus.sys.utils.SymbolConstant;
import com.weir.quarkus.sys.vo.CommonConstant;
import com.weir.quarkus.sys.vo.PageDTO;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

/**
 * @Title: SysUserService.java
 * @Package com.weir.quarkus.sys.service
 * @Description: TODO(用一句话描述该文件做什么)
 * @author weir
 * @date 2024年3月14日 16:06:03
 * @version V1.0
 */
@ApplicationScoped
public class SysUserService {

	private static final QSysUserDepart qSysUserDepart = QSysUserDepart.sysUserDepart;
	private static final QSysDepart qSysDepart = QSysDepart.sysDepart;
	private static final QSysUser qSysUser = QSysUser.sysUser;
	private static final QSysUserRole qSysUserRole = QSysUserRole.sysUserRole;

	@Inject
	EntityManager em;
	@Inject
	JPAQueryFactory jpaQueryFactory;

	public PageDTO getUserByRoleId(SysUserQuery userQuery) {
		BooleanBuilder booleanBuilder = new BooleanBuilder();
		booleanBuilder.and(qSysUser.delFlag.eq(false));
		booleanBuilder.and(qSysUserRole.roleId.eq(userQuery.getRoleId()));
		if (StringUtils.isNotBlank(userQuery.getUsername())) {
			booleanBuilder.and(qSysUser.username.like(userQuery.getUsername()));
		}
		JPAQuery<SysUser> where = jpaQueryFactory.selectFrom(qSysUser).join(qSysUserRole)
				.on(qSysUser.id.eq(qSysUserRole.userId)).where(booleanBuilder);
		int count = where.fetch().size();
		List<SysUser> list = where.offset((long) (userQuery.getPage() - 1) * userQuery.getPageSize())
				.limit(userQuery.getPageSize()).fetch();
		PageDTO pageDTO = PageUtil.toPage(userQuery.getPage(), userQuery.getPageSize(), count, list);
		return pageDTO;
	}

	public record DepartIdModel(String key, String value, String title) {
	}

	public List<DepartIdModel> queryDeparts(String userId) {
		List<SysDepart> list = jpaQueryFactory.selectDistinct(qSysDepart).from(qSysDepart).join(qSysUserDepart)
				.on(qSysUserDepart.depId.eq(qSysDepart.id)).where(qSysUserDepart.userId.eq(userId)).fetch();
		if (!CollectionUtils.isEmpty(list)) {
//			list.forEach(d->{
//				new DepartIdModel(d.getId(), d.getId(), d.getDepartName());
//			});
			return list.stream().map(d -> {
				return new DepartIdModel(d.getId(), d.getId(), d.getDepartName());
			}).collect(Collectors.toList());
		}
		return null;
	}

	@Transactional
	public void saveUser(SysUser user) {
		if (user.getId() == null) {
			String salt = RandomStringUtils.randomAlphanumeric(8);
			user.setSalt(salt);
			String passwordEncode = PasswordUtil.encrypt(user.getUsername(), user.getPassword(), salt);
			user.setPassword(passwordEncode);
			user.setStatus(1);
			user.setDelFlag(false);
			// 用户表字段org_code不能在这里设置他的值
			user.setOrgCode(null);
			user.persist();
		} else {
			SysUser old = SysUser.findById(user.getId());
			MergeEntityCopyUtil.updateEntity(user, old);
			em.merge(old);
		}

		if (user.getId() != null) {
			SysUserRole.delete("userId", user.getId());
			SysUserDepart.delete("userId", user.getId());
			SysUserPosition.delete("userId", user.getId());
		}
		// step.2 保存角色
		if (StringUtils.isNotBlank(user.getSelectedroles())) {
			String[] arr = user.getSelectedroles().split(",");
			for (String roleId : arr) {
				SysUserRole userRole = new SysUserRole(user.getId(), roleId);
				userRole.persist();
			}
		}

		// step.3 保存所属部门
		if (StringUtils.isNotBlank(user.getSelecteddeparts())) {
			String[] arr = user.getSelecteddeparts().split(",");
			for (String deaprtId : arr) {
				SysUserDepart userDeaprt = new SysUserDepart(user.getId(), deaprtId);
				userDeaprt.persist();
			}
		}

		saveUserPosition(user.getId(), user.getPost());
	}

	/**
	 * 保存用户职位
	 *
	 * @param userId
	 * @param postIds
	 */
	private void saveUserPosition(String userId, String positionIds) {
		if (StringUtils.isNotBlank(positionIds)) {
			String[] positionIdArray = positionIds.split(SymbolConstant.COMMA);
			for (String postId : positionIdArray) {
				SysUserPosition userPosition = new SysUserPosition();
				userPosition.setUserId(userId);
				userPosition.setPositionId(postId);
				userPosition.persist();
			}
		}
	}
}
