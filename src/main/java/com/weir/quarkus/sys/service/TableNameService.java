/** 
 * @Title: SysDictService.java 
 * @Package com.weir.quarkus.sys.service 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月12日 11:05:07 
 * @version V1.0 
 */
package com.weir.quarkus.sys.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.weir.quarkus.sys.vo.DuplicateCheckVo;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;

/**
 * @Title: SysDictService.java
 * @Package com.weir.quarkus.sys.service
 * @Description: TODO(用一句话描述该文件做什么)
 * @author weir
 * @date 2024年3月12日 11:05:07
 * @version V1.0
 */
@ApplicationScoped
public class TableNameService {
	@Inject
	EntityManager em;
	
    /**
     * 获取某个表的所有字段名
     *
     * @return
     * @throws SQLException
     */
    public List<String> getAllColumnNames(String tableName) {
        List<String> list = new ArrayList<String>();
        SessionFactory sessionFactory = em.getEntityManagerFactory().unwrap(SessionFactory.class);
        Session session = sessionFactory.openSession();
        if (session != null) {
            list = session.doReturningWork(
                    connection -> {
                        List<String> resultList = new ArrayList<>();
                        ResultSet rs;
                        String username=connection.getMetaData().getUserName();
                        try {
                            rs = connection.getMetaData().getColumns(connection.getCatalog(), username, tableName, null);
                            while (rs.next()) {
                                String column= rs.getObject(4).toString();
                                resultList.add(column);
                            }
                        } catch (SQLException e) {
                            //不存在
                            return resultList;
                        }
                        rs.close();
                        return resultList;
                    }
            );
            session.close();
        }
        return list;
    }

	/**
	 * 获取所有表格
	 *
	 * @return
	 * @throws SQLException
	 */
	public List<String> getAllTableNames() {
		List<String> list = new ArrayList<String>();
		SessionFactory sessionFactory = em.getEntityManagerFactory().unwrap(SessionFactory.class);
		Session session = sessionFactory.openSession();
		if (session != null) {
			list = session.doReturningWork(connection -> {
				List<String> resultList = new ArrayList<>();
				ResultSet rs;
				String username = connection.getMetaData().getUserName();
				try {
					rs = connection.getMetaData().getTables(connection.getCatalog(), username, null, null);
					while (rs.next()) {
						String table = rs.getObject(3).toString();
						resultList.add(table);
					}
				} catch (SQLException e) {
					// 不存在
					return resultList;
				}
				rs.close();
				return resultList;
			});
			session.close();
		}
		return list;
	}
}
