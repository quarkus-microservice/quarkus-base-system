package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Date;

@Data
@Entity
@Table(name = "sys_tenant_pack_perms")
public class SysTenantPackPerms {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "pack_id")
    private Integer packId;
    @Basic
    @Column(name = "permission_id")
    private Integer permissionId;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getPackId() {
//        return packId;
//    }
//
//    public void setPackId(String packId) {
//        this.packId = packId;
//    }
//
//    public String getPermissionId() {
//        return permissionId;
//    }
//
//    public void setPermissionId(String permissionId) {
//        this.permissionId = permissionId;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Date getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Date createTime) {
//        this.createTime = createTime;
//    }
//
//    public String getUpdateBy() {
//        return updateBy;
//    }
//
//    public void setUpdateBy(String updateBy) {
//        this.updateBy = updateBy;
//    }
//
//    public Date getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Date updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysTenantPackPerms that = (SysTenantPackPerms) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (packId != null ? !packId.equals(that.packId) : that.packId != null) return false;
//        if (permissionId != null ? !permissionId.equals(that.permissionId) : that.permissionId != null) return false;
//        if (createBy != null ? !createBy.equals(that.createBy) : that.createBy != null) return false;
//        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
//        if (updateBy != null ? !updateBy.equals(that.updateBy) : that.updateBy != null) return false;
//        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (packId != null ? packId.hashCode() : 0);
//        result = 31 * result + (permissionId != null ? permissionId.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        return result;
//    }
}
