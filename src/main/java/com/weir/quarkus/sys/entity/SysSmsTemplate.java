package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_sms_template")
public class SysSmsTemplate {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "template_name")
    private String templateName;
    @Basic
    @Column(name = "template_code")
    private String templateCode;
    @Basic
    @Column(name = "template_type")
    private String templateType;
    @Basic
    @Column(name = "template_content")
    private String templateContent;
    @Basic
    @Column(name = "template_test_json")
    private String templateTestJson;

    @Basic
    @Column(name = "use_status")
    private Integer useStatus;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getTemplateName() {
//        return templateName;
//    }
//
//    public void setTemplateName(String templateName) {
//        this.templateName = templateName;
//    }
//
//    public String getTemplateCode() {
//        return templateCode;
//    }
//
//    public void setTemplateCode(String templateCode) {
//        this.templateCode = templateCode;
//    }
//
//    public String getTemplateType() {
//        return templateType;
//    }
//
//    public void setTemplateType(String templateType) {
//        this.templateType = templateType;
//    }
//
//    public String getTemplateContent() {
//        return templateContent;
//    }
//
//    public void setTemplateContent(String templateContent) {
//        this.templateContent = templateContent;
//    }
//
//    public String getTemplateTestJson() {
//        return templateTestJson;
//    }
//
//    public void setTemplateTestJson(String templateTestJson) {
//        this.templateTestJson = templateTestJson;
//    }
//
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.createTime = createTime;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Timestamp getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Timestamp updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    public String getUpdateBy() {
//        return updateBy;
//    }
//
//    public void setUpdateBy(String updateBy) {
//        this.updateBy = updateBy;
//    }
//
//    public String getUseStatus() {
//        return useStatus;
//    }
//
//    public void setUseStatus(String useStatus) {
//        this.useStatus = useStatus;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysSmsTemplate that = (SysSmsTemplate) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (templateName != null ? !templateName.equals(that.templateName) : that.templateName != null) return false;
//        if (templateCode != null ? !templateCode.equals(that.templateCode) : that.templateCode != null) return false;
//        if (templateType != null ? !templateType.equals(that.templateType) : that.templateType != null) return false;
//        if (templateContent != null ? !templateContent.equals(that.templateContent) : that.templateContent != null)
//            return false;
//        if (templateTestJson != null ? !templateTestJson.equals(that.templateTestJson) : that.templateTestJson != null)
//            return false;
//        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
//        if (createBy != null ? !createBy.equals(that.createBy) : that.createBy != null) return false;
//        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
//        if (updateBy != null ? !updateBy.equals(that.updateBy) : that.updateBy != null) return false;
//        if (useStatus != null ? !useStatus.equals(that.useStatus) : that.useStatus != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (templateName != null ? templateName.hashCode() : 0);
//        result = 31 * result + (templateCode != null ? templateCode.hashCode() : 0);
//        result = 31 * result + (templateType != null ? templateType.hashCode() : 0);
//        result = 31 * result + (templateContent != null ? templateContent.hashCode() : 0);
//        result = 31 * result + (templateTestJson != null ? templateTestJson.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
//        result = 31 * result + (useStatus != null ? useStatus.hashCode() : 0);
//        return result;
//    }
}
