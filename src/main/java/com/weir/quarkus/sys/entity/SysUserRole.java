package com.weir.quarkus.sys.entity;


import org.hibernate.annotations.GenericGenerator;

import com.weir.quarkus.base.system.entity.MyIdGenerator;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "sys_user_role")
public class SysUserRole extends PanacheEntityBase {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "user_id")
    private String userId;
    @Basic
    @Column(name = "role_id")
    private String roleId;
    @Basic
    @Column(name = "tenant_id")
    private Integer tenantId;
	/**
	 * @param userId
	 * @param roleId
	 */
	public SysUserRole(String userId, String roleId) {
		this.userId = userId;
		this.roleId = roleId;
	}
	/**
	 * 
	 */
	public SysUserRole() {
	}

}
