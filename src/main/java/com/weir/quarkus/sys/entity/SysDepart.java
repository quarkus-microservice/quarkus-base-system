package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.weir.quarkus.base.system.entity.BaseEntity;
import com.weir.quarkus.base.system.entity.MyIdGenerator;

@Getter
@Setter
@Entity
@Table(name = "sys_depart")
public class SysDepart extends BaseEntity {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "parent_id")
    private String parentId;
    @Basic
    @Column(name = "depart_name")
    private String departName;
    @Basic
    @Column(name = "depart_name_en")
    private String departNameEn;
    @Basic
    @Column(name = "depart_name_abbr")
    private String departNameAbbr;
    @Basic
    @Column(name = "depart_order")
    private Integer departOrder;
    @Basic
    @Column(name = "description")
    private String description;
    @Basic
    @Column(name = "org_category")
    private String orgCategory;
    @Basic
    @Column(name = "org_type")
    private String orgType;
    @Basic
    @Column(name = "org_code")
    private String orgCode;
    @Basic
    @Column(name = "mobile")
    private String mobile;
    @Basic
    @Column(name = "fax")
    private String fax;
    @Basic
    @Column(name = "address")
    private String address;
    @Basic
    @Column(name = "memo")
    private String memo;
    @Basic
    @Column(name = "status")
    private String status;
    @Basic
    @Column(name = "qywx_identifier")
    private String qywxIdentifier;

    @Basic
    @Column(name = "tenant_id")
    private Integer tenantId;
    @Basic
    @Column(name = "iz_leaf")
    private Boolean izLeaf;
    
    @Transient
	private String directorUserIds;
    @Transient
    private String oldDirectorUserIds;

}
