package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_form_file")
public class SysFormFile {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "table_name")
    private String tableName;
    @Basic
    @Column(name = "table_data_id")
    private String tableDataId;
    @Basic
    @Column(name = "file_id")
    private String fileId;
    @Basic
    @Column(name = "file_type")
    private String fileType;


//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getTableName() {
//        return tableName;
//    }
//
//    public void setTableName(String tableName) {
//        this.tableName = tableName;
//    }
//
//    public String getTableDataId() {
//        return tableDataId;
//    }
//
//    public void setTableDataId(String tableDataId) {
//        this.tableDataId = tableDataId;
//    }
//
//    public String getFileId() {
//        return fileId;
//    }
//
//    public void setFileId(String fileId) {
//        this.fileId = fileId;
//    }
//
//    public String getFileType() {
//        return fileType;
//    }
//
//    public void setFileType(String fileType) {
//        this.fileType = fileType;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.createTime = createTime;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysFormFile that = (SysFormFile) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (tableName != null ? !tableName.equals(that.tableName) : that.tableName != null) return false;
//        if (tableDataId != null ? !tableDataId.equals(that.tableDataId) : that.tableDataId != null) return false;
//        if (fileId != null ? !fileId.equals(that.fileId) : that.fileId != null) return false;
//        if (fileType != null ? !fileType.equals(that.fileType) : that.fileType != null) return false;
//        if (createBy != null ? !createBy.equals(that.createBy) : that.createBy != null) return false;
//        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (tableName != null ? tableName.hashCode() : 0);
//        result = 31 * result + (tableDataId != null ? tableDataId.hashCode() : 0);
//        result = 31 * result + (fileId != null ? fileId.hashCode() : 0);
//        result = 31 * result + (fileType != null ? fileType.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        return result;
//    }
}
