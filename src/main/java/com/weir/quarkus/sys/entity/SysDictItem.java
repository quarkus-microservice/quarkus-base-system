package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import org.hibernate.annotations.GenericGenerator;
import org.jeecgframework.poi.excel.annotation.Excel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.weir.quarkus.base.aspect.DictData;
import com.weir.quarkus.base.system.entity.BaseEntity;
import com.weir.quarkus.base.system.entity.MyIdGenerator;

@Getter
@Setter
//@Builder
@Entity
@Table(name = "sys_dict_item")
public class SysDictItem extends BaseSysEntity {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "dict_id")
    private String dictId;
    
    @Excel(name = "字典项文本", width = 20)
    @Basic
    @Column(name = "item_text")
    private String itemText;
    
    @Excel(name = "字典项值", width = 30)
    @Basic
    @Column(name = "item_value")
    private String itemValue;
    @Basic
    @Column(name = "item_color")
    private String itemColor;
    
    @Excel(name = "描述", width = 40)
    @Basic
    @Column(name = "description")
    private String description;
    
    @Excel(name = "排序", width = 15,type=4)
    @Basic
    @Column(name = "sort_order")
    private Integer sortOrder;
    
    @DictData(code = "dict_item_status")
    @Basic
    @Column(name = "status")
    private Integer status;


}
