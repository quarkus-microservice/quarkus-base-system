package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_user_agent")
public class SysUserAgent {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "user_name")
    private String userName;
    @Basic
    @Column(name = "agent_user_name")
    private String agentUserName;
    @Basic
    @Column(name = "start_time")
    private Timestamp startTime;
    @Basic
    @Column(name = "end_time")
    private Timestamp endTime;
    @Basic
    @Column(name = "status")
    private String status;
    @Basic
    @Column(name = "sys_org_code")
    private String sysOrgCode;
    @Basic
    @Column(name = "sys_company_code")
    private String sysCompanyCode;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getUserName() {
//        return userName;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public String getAgentUserName() {
//        return agentUserName;
//    }
//
//    public void setAgentUserName(String agentUserName) {
//        this.agentUserName = agentUserName;
//    }
//
//    public Timestamp getStartTime() {
//        return startTime;
//    }
//
//    public void setStartTime(Timestamp startTime) {
//        this.startTime = startTime;
//    }
//
//    public Timestamp getEndTime() {
//        return endTime;
//    }
//
//    public void setEndTime(Timestamp endTime) {
//        this.endTime = endTime;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public String getCreateName() {
//        return createName;
//    }
//
//    public void setCreateName(String createName) {
//        this.createName = createName;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.createTime = createTime;
//    }
//
//    public String getUpdateName() {
//        return updateName;
//    }
//
//    public void setUpdateName(String updateName) {
//        this.updateName = updateName;
//    }
//
//    public String getUpdateBy() {
//        return updateBy;
//    }
//
//    public void setUpdateBy(String updateBy) {
//        this.updateBy = updateBy;
//    }
//
//    public Timestamp getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Timestamp updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    public String getSysOrgCode() {
//        return sysOrgCode;
//    }
//
//    public void setSysOrgCode(String sysOrgCode) {
//        this.sysOrgCode = sysOrgCode;
//    }
//
//    public String getSysCompanyCode() {
//        return sysCompanyCode;
//    }
//
//    public void setSysCompanyCode(String sysCompanyCode) {
//        this.sysCompanyCode = sysCompanyCode;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysUserAgent that = (SysUserAgent) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
//        if (agentUserName != null ? !agentUserName.equals(that.agentUserName) : that.agentUserName != null)
//            return false;
//        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
//        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;
//        if (status != null ? !status.equals(that.status) : that.status != null) return false;
//        if (createName != null ? !createName.equals(that.createName) : that.createName != null) return false;
//        if (createBy != null ? !createBy.equals(that.createBy) : that.createBy != null) return false;
//        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
//        if (updateName != null ? !updateName.equals(that.updateName) : that.updateName != null) return false;
//        if (updateBy != null ? !updateBy.equals(that.updateBy) : that.updateBy != null) return false;
//        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
//        if (sysOrgCode != null ? !sysOrgCode.equals(that.sysOrgCode) : that.sysOrgCode != null) return false;
//        if (sysCompanyCode != null ? !sysCompanyCode.equals(that.sysCompanyCode) : that.sysCompanyCode != null)
//            return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (userName != null ? userName.hashCode() : 0);
//        result = 31 * result + (agentUserName != null ? agentUserName.hashCode() : 0);
//        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
//        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
//        result = 31 * result + (status != null ? status.hashCode() : 0);
//        result = 31 * result + (createName != null ? createName.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (updateName != null ? updateName.hashCode() : 0);
//        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        result = 31 * result + (sysOrgCode != null ? sysOrgCode.hashCode() : 0);
//        result = 31 * result + (sysCompanyCode != null ? sysCompanyCode.hashCode() : 0);
//        return result;
//    }
}
