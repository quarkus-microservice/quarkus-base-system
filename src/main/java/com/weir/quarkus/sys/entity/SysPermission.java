package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.weir.quarkus.base.system.entity.BaseEntity;
import com.weir.quarkus.base.system.entity.MyIdGenerator;

@Getter
@Setter
//@Builder
@Entity
@Table(name = "sys_permission")
public class SysPermission extends BaseEntity {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "parent_id")
    private String parentId;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "url")
    private String url;
    @Basic
    @Column(name = "component")
    private String component;
    @Basic
    @Column(name = "is_route")
    private Boolean isRoute;
    @Basic
    @Column(name = "component_name")
    private String componentName;
    @Basic
    @Column(name = "redirect")
    private String redirect;
    @Basic
    @Column(name = "menu_type")
    private Integer menuType;
    @Basic
    @Column(name = "perms")
    private String perms;
    @Basic
    @Column(name = "perms_type")
    private String permsType;
    @Basic
    @Column(name = "sort_no")
    private Integer sortNo;
    @Basic
    @Column(name = "always_show")
    private Boolean alwaysShow;
    @Basic
    @Column(name = "icon")
    private String icon;
    @Basic
    @Column(name = "is_leaf")
    private Boolean isLeaf;
    @Basic
    @Column(name = "keep_alive")
    private Boolean keepAlive;
    @Basic
    @Column(name = "hidden")
    private Boolean hidden;
    @Basic
    @Column(name = "hide_tab")
    private Boolean hideTab;
    @Basic
    @Column(name = "description")
    private String description;

    @Basic
    @Column(name = "del_flag")
    private Boolean delFlag;
    @Basic
    @Column(name = "rule_flag")
    private Integer ruleFlag;
    @Basic
    @Column(name = "status")
    private String status;
    @Basic
    @Column(name = "internal_or_external")
    private Boolean internalOrExternal;

}
