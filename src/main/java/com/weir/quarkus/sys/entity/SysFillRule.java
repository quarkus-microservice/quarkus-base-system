package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


import org.hibernate.annotations.GenericGenerator;

import com.weir.quarkus.base.system.entity.MyIdGenerator;

@Getter
@Setter
@Entity
@Table(name = "sys_fill_rule")
public class SysFillRule extends BaseSysEntity {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "rule_name")
    private String ruleName;
    @Basic
    @Column(name = "rule_code")
    private String ruleCode;
    @Basic
    @Column(name = "rule_class")
    private String ruleClass;
    @Basic
    @Column(name = "rule_params")
    private String ruleParams;

}
