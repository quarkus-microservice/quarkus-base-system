package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Date;

@Data
@Entity
@Table(name = "sys_tenant_pack")
public class SysTenantPack {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "tenant_id")
    private Integer tenantId;
    @Basic
    @Column(name = "pack_name")
    private String packName;
    @Basic
    @Column(name = "status")
    private String status;
    @Basic
    @Column(name = "remarks")
    private String remarks;

    @Basic
    @Column(name = "pack_code")
    private String packCode;
    @Basic
    @Column(name = "pack_type")
    private String packType;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public Integer getTenantId() {
//        return tenantId;
//    }
//
//    public void setTenantId(Integer tenantId) {
//        this.tenantId = tenantId;
//    }
//
//    public String getPackName() {
//        return packName;
//    }
//
//    public void setPackName(String packName) {
//        this.packName = packName;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public String getRemarks() {
//        return remarks;
//    }
//
//    public void setRemarks(String remarks) {
//        this.remarks = remarks;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Date getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Date createTime) {
//        this.createTime = createTime;
//    }
//
//    public String getUpdateBy() {
//        return updateBy;
//    }
//
//    public void setUpdateBy(String updateBy) {
//        this.updateBy = updateBy;
//    }
//
//    public Date getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Date updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    public String getPackCode() {
//        return packCode;
//    }
//
//    public void setPackCode(String packCode) {
//        this.packCode = packCode;
//    }
//
//    public String getPackType() {
//        return packType;
//    }
//
//    public void setPackType(String packType) {
//        this.packType = packType;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysTenantPack that = (SysTenantPack) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (tenantId != null ? !tenantId.equals(that.tenantId) : that.tenantId != null) return false;
//        if (packName != null ? !packName.equals(that.packName) : that.packName != null) return false;
//        if (status != null ? !status.equals(that.status) : that.status != null) return false;
//        if (remarks != null ? !remarks.equals(that.remarks) : that.remarks != null) return false;
//        if (createBy != null ? !createBy.equals(that.createBy) : that.createBy != null) return false;
//        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
//        if (updateBy != null ? !updateBy.equals(that.updateBy) : that.updateBy != null) return false;
//        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
//        if (packCode != null ? !packCode.equals(that.packCode) : that.packCode != null) return false;
//        if (packType != null ? !packType.equals(that.packType) : that.packType != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (tenantId != null ? tenantId.hashCode() : 0);
//        result = 31 * result + (packName != null ? packName.hashCode() : 0);
//        result = 31 * result + (status != null ? status.hashCode() : 0);
//        result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        result = 31 * result + (packCode != null ? packCode.hashCode() : 0);
//        result = 31 * result + (packType != null ? packType.hashCode() : 0);
//        return result;
//    }
}
