package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.hibernate.annotations.GenericGenerator;
import org.jboss.resteasy.reactive.DateFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.weir.quarkus.base.system.entity.BaseEntity;
import com.weir.quarkus.base.system.entity.MyIdGenerator;

@Getter
@Setter
//@Builder
//@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "sys_user")
public class SysUser extends BaseEntity {
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//	@GenericGenerator(name = "sequence_id", strategy = "com.weir.quarkus.base.system.entity.MyIdGenerator" )
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "username")
    private String username;
    @Basic
    @Column(name = "realname")
    private String realname;
    @Basic
    @Column(name = "password")
    private String password;
    @Basic
    @Column(name = "salt")
    private String salt;
    @Basic
    @Column(name = "avatar")
    private String avatar;
    
//    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
//    @DateFormat(pattern = "")
    @Basic
    @Column(name = "birthday")
    private LocalDate birthday;
    @Basic
    @Column(name = "sex")
    private Integer sex;
    @Basic
    @Column(name = "email")
    private String email;
    @Basic
    @Column(name = "phone")
    private String phone;
    @Basic
    @Column(name = "org_code")
    private String orgCode;
    @Basic
    @Column(name = "status")
    private Integer status;
    @Basic
    @Column(name = "third_id")
    private String thirdId;
    @Basic
    @Column(name = "third_type")
    private String thirdType;
    @Basic
    @Column(name = "activiti_sync")
    private Integer activitiSync;
    @Basic
    @Column(name = "work_no")
    private String workNo;
    @Basic
    @Column(name = "telephone")
    private String telephone;

    @Basic
    @Column(name = "user_identity")
    private Integer userIdentity;
    @Basic
    @Column(name = "depart_ids")
    private String departIds;
    @Basic
    @Column(name = "client_id")
    private String clientId;
    @Basic
    @Column(name = "login_tenant_id")
    private Integer loginTenantId;
    @Basic
    @Column(name = "bpm_status")
    private String bpmStatus;
    
    @Transient
    private String selectedroles;
    @Transient
    private String selecteddeparts;
    @Transient
    private String relTenantIds;
    
    @Transient
    private String post;

}
