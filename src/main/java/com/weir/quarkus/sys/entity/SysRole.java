package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


import org.hibernate.annotations.GenericGenerator;

import com.weir.quarkus.base.system.entity.MyIdGenerator;

@Getter
@Setter
//@Builder
@Entity
@Table(name = "sys_role")
public class SysRole extends BaseSysEntity {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "role_name")
    private String roleName;
    @Basic
    @Column(name = "role_code")
    private String roleCode;
    @Basic
    @Column(name = "description")
    private String description;

    @Basic
    @Column(name = "tenant_id")
    private Integer tenantId;

}
