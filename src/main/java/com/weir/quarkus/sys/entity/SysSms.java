package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_sms")
public class SysSms {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "es_title")
    private String esTitle;
    @Basic
    @Column(name = "es_type")
    private String esType;
    @Basic
    @Column(name = "es_receiver")
    private String esReceiver;
    @Basic
    @Column(name = "es_param")
    private String esParam;
    @Basic
    @Column(name = "es_content")
    private String esContent;
    @Basic
    @Column(name = "es_send_time")
    private Timestamp esSendTime;
    @Basic
    @Column(name = "es_send_status")
    private String esSendStatus;
    @Basic
    @Column(name = "es_send_num")
    private Integer esSendNum;
    @Basic
    @Column(name = "es_result")
    private String esResult;



//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getEsTitle() {
//        return esTitle;
//    }
//
//    public void setEsTitle(String esTitle) {
//        this.esTitle = esTitle;
//    }
//
//    public String getEsType() {
//        return esType;
//    }
//
//    public void setEsType(String esType) {
//        this.esType = esType;
//    }
//
//    public String getEsReceiver() {
//        return esReceiver;
//    }
//
//    public void setEsReceiver(String esReceiver) {
//        this.esReceiver = esReceiver;
//    }
//
//    public String getEsParam() {
//        return esParam;
//    }
//
//    public void setEsParam(String esParam) {
//        this.esParam = esParam;
//    }
//
//    public String getEsContent() {
//        return esContent;
//    }
//
//    public void setEsContent(String esContent) {
//        this.esContent = esContent;
//    }
//
//    public Timestamp getEsSendTime() {
//        return esSendTime;
//    }
//
//    public void setEsSendTime(Timestamp esSendTime) {
//        this.esSendTime = esSendTime;
//    }
//
//    public String getEsSendStatus() {
//        return esSendStatus;
//    }
//
//    public void setEsSendStatus(String esSendStatus) {
//        this.esSendStatus = esSendStatus;
//    }
//
//    public Integer getEsSendNum() {
//        return esSendNum;
//    }
//
//    public void setEsSendNum(Integer esSendNum) {
//        this.esSendNum = esSendNum;
//    }
//
//    public String getEsResult() {
//        return esResult;
//    }
//
//    public void setEsResult(String esResult) {
//        this.esResult = esResult;
//    }
//
//    public String getRemark() {
//        return remark;
//    }
//
//    public void setRemark(String remark) {
//        this.remark = remark;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.createTime = createTime;
//    }
//
//    public String getUpdateBy() {
//        return updateBy;
//    }
//
//    public void setUpdateBy(String updateBy) {
//        this.updateBy = updateBy;
//    }
//
//    public Timestamp getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Timestamp updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysSms sysSms = (SysSms) o;
//
//        if (id != null ? !id.equals(sysSms.id) : sysSms.id != null) return false;
//        if (esTitle != null ? !esTitle.equals(sysSms.esTitle) : sysSms.esTitle != null) return false;
//        if (esType != null ? !esType.equals(sysSms.esType) : sysSms.esType != null) return false;
//        if (esReceiver != null ? !esReceiver.equals(sysSms.esReceiver) : sysSms.esReceiver != null) return false;
//        if (esParam != null ? !esParam.equals(sysSms.esParam) : sysSms.esParam != null) return false;
//        if (esContent != null ? !esContent.equals(sysSms.esContent) : sysSms.esContent != null) return false;
//        if (esSendTime != null ? !esSendTime.equals(sysSms.esSendTime) : sysSms.esSendTime != null) return false;
//        if (esSendStatus != null ? !esSendStatus.equals(sysSms.esSendStatus) : sysSms.esSendStatus != null)
//            return false;
//        if (esSendNum != null ? !esSendNum.equals(sysSms.esSendNum) : sysSms.esSendNum != null) return false;
//        if (esResult != null ? !esResult.equals(sysSms.esResult) : sysSms.esResult != null) return false;
//        if (remark != null ? !remark.equals(sysSms.remark) : sysSms.remark != null) return false;
//        if (createBy != null ? !createBy.equals(sysSms.createBy) : sysSms.createBy != null) return false;
//        if (createTime != null ? !createTime.equals(sysSms.createTime) : sysSms.createTime != null) return false;
//        if (updateBy != null ? !updateBy.equals(sysSms.updateBy) : sysSms.updateBy != null) return false;
//        if (updateTime != null ? !updateTime.equals(sysSms.updateTime) : sysSms.updateTime != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (esTitle != null ? esTitle.hashCode() : 0);
//        result = 31 * result + (esType != null ? esType.hashCode() : 0);
//        result = 31 * result + (esReceiver != null ? esReceiver.hashCode() : 0);
//        result = 31 * result + (esParam != null ? esParam.hashCode() : 0);
//        result = 31 * result + (esContent != null ? esContent.hashCode() : 0);
//        result = 31 * result + (esSendTime != null ? esSendTime.hashCode() : 0);
//        result = 31 * result + (esSendStatus != null ? esSendStatus.hashCode() : 0);
//        result = 31 * result + (esSendNum != null ? esSendNum.hashCode() : 0);
//        result = 31 * result + (esResult != null ? esResult.hashCode() : 0);
//        result = 31 * result + (remark != null ? remark.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        return result;
//    }
}
