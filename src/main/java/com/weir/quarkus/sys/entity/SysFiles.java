package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_files")
public class SysFiles {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "file_name")
    private String fileName;
    @Basic
    @Column(name = "url")
    private String url;
    @Basic
    @Column(name = "file_type")
    private String fileType;
    @Basic
    @Column(name = "store_type")
    private String storeType;
    @Basic
    @Column(name = "parent_id")
    private String parentId;
    @Basic
    @Column(name = "tenant_id")
    private String tenantId;
    @Basic
    @Column(name = "file_size")
    private Double fileSize;
    @Basic
    @Column(name = "iz_folder")
    private String izFolder;
    @Basic
    @Column(name = "iz_root_folder")
    private String izRootFolder;
    @Basic
    @Column(name = "iz_star")
    private String izStar;
    @Basic
    @Column(name = "down_count")
    private Integer downCount;
    @Basic
    @Column(name = "read_count")
    private Integer readCount;
    @Basic
    @Column(name = "share_url")
    private String shareUrl;
    @Basic
    @Column(name = "share_perms")
    private String sharePerms;
    @Basic
    @Column(name = "enable_down")
    private String enableDown;
    @Basic
    @Column(name = "enable_updat")
    private String enableUpdat;
    @Basic
    @Column(name = "del_flag")
    private String delFlag;


//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getFileName() {
//        return fileName;
//    }
//
//    public void setFileName(String fileName) {
//        this.fileName = fileName;
//    }
//
//    public String getUrl() {
//        return url;
//    }
//
//    public void setUrl(String url) {
//        this.url = url;
//    }
//
//    public String getFileType() {
//        return fileType;
//    }
//
//    public void setFileType(String fileType) {
//        this.fileType = fileType;
//    }
//
//    public String getStoreType() {
//        return storeType;
//    }
//
//    public void setStoreType(String storeType) {
//        this.storeType = storeType;
//    }
//
//    public String getParentId() {
//        return parentId;
//    }
//
//    public void setParentId(String parentId) {
//        this.parentId = parentId;
//    }
//
//    public String getTenantId() {
//        return tenantId;
//    }
//
//    public void setTenantId(String tenantId) {
//        this.tenantId = tenantId;
//    }
//
//    public Double getFileSize() {
//        return fileSize;
//    }
//
//    public void setFileSize(Double fileSize) {
//        this.fileSize = fileSize;
//    }
//
//    public String getIzFolder() {
//        return izFolder;
//    }
//
//    public void setIzFolder(String izFolder) {
//        this.izFolder = izFolder;
//    }
//
//    public String getIzRootFolder() {
//        return izRootFolder;
//    }
//
//    public void setIzRootFolder(String izRootFolder) {
//        this.izRootFolder = izRootFolder;
//    }
//
//    public String getIzStar() {
//        return izStar;
//    }
//
//    public void setIzStar(String izStar) {
//        this.izStar = izStar;
//    }
//
//    public Integer getDownCount() {
//        return downCount;
//    }
//
//    public void setDownCount(Integer downCount) {
//        this.downCount = downCount;
//    }
//
//    public Integer getReadCount() {
//        return readCount;
//    }
//
//    public void setReadCount(Integer readCount) {
//        this.readCount = readCount;
//    }
//
//    public String getShareUrl() {
//        return shareUrl;
//    }
//
//    public void setShareUrl(String shareUrl) {
//        this.shareUrl = shareUrl;
//    }
//
//    public String getSharePerms() {
//        return sharePerms;
//    }
//
//    public void setSharePerms(String sharePerms) {
//        this.sharePerms = sharePerms;
//    }
//
//    public String getEnableDown() {
//        return enableDown;
//    }
//
//    public void setEnableDown(String enableDown) {
//        this.enableDown = enableDown;
//    }
//
//    public String getEnableUpdat() {
//        return enableUpdat;
//    }
//
//    public void setEnableUpdat(String enableUpdat) {
//        this.enableUpdat = enableUpdat;
//    }
//
//    public String getDelFlag() {
//        return delFlag;
//    }
//
//    public void setDelFlag(String delFlag) {
//        this.delFlag = delFlag;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.createTime = createTime;
//    }
//
//    public String getUpdateBy() {
//        return updateBy;
//    }
//
//    public void setUpdateBy(String updateBy) {
//        this.updateBy = updateBy;
//    }
//
//    public Timestamp getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Timestamp updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysFiles sysFiles = (SysFiles) o;
//
//        if (id != null ? !id.equals(sysFiles.id) : sysFiles.id != null) return false;
//        if (fileName != null ? !fileName.equals(sysFiles.fileName) : sysFiles.fileName != null) return false;
//        if (url != null ? !url.equals(sysFiles.url) : sysFiles.url != null) return false;
//        if (fileType != null ? !fileType.equals(sysFiles.fileType) : sysFiles.fileType != null) return false;
//        if (storeType != null ? !storeType.equals(sysFiles.storeType) : sysFiles.storeType != null) return false;
//        if (parentId != null ? !parentId.equals(sysFiles.parentId) : sysFiles.parentId != null) return false;
//        if (tenantId != null ? !tenantId.equals(sysFiles.tenantId) : sysFiles.tenantId != null) return false;
//        if (fileSize != null ? !fileSize.equals(sysFiles.fileSize) : sysFiles.fileSize != null) return false;
//        if (izFolder != null ? !izFolder.equals(sysFiles.izFolder) : sysFiles.izFolder != null) return false;
//        if (izRootFolder != null ? !izRootFolder.equals(sysFiles.izRootFolder) : sysFiles.izRootFolder != null)
//            return false;
//        if (izStar != null ? !izStar.equals(sysFiles.izStar) : sysFiles.izStar != null) return false;
//        if (downCount != null ? !downCount.equals(sysFiles.downCount) : sysFiles.downCount != null) return false;
//        if (readCount != null ? !readCount.equals(sysFiles.readCount) : sysFiles.readCount != null) return false;
//        if (shareUrl != null ? !shareUrl.equals(sysFiles.shareUrl) : sysFiles.shareUrl != null) return false;
//        if (sharePerms != null ? !sharePerms.equals(sysFiles.sharePerms) : sysFiles.sharePerms != null) return false;
//        if (enableDown != null ? !enableDown.equals(sysFiles.enableDown) : sysFiles.enableDown != null) return false;
//        if (enableUpdat != null ? !enableUpdat.equals(sysFiles.enableUpdat) : sysFiles.enableUpdat != null)
//            return false;
//        if (delFlag != null ? !delFlag.equals(sysFiles.delFlag) : sysFiles.delFlag != null) return false;
//        if (createBy != null ? !createBy.equals(sysFiles.createBy) : sysFiles.createBy != null) return false;
//        if (createTime != null ? !createTime.equals(sysFiles.createTime) : sysFiles.createTime != null) return false;
//        if (updateBy != null ? !updateBy.equals(sysFiles.updateBy) : sysFiles.updateBy != null) return false;
//        if (updateTime != null ? !updateTime.equals(sysFiles.updateTime) : sysFiles.updateTime != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
//        result = 31 * result + (url != null ? url.hashCode() : 0);
//        result = 31 * result + (fileType != null ? fileType.hashCode() : 0);
//        result = 31 * result + (storeType != null ? storeType.hashCode() : 0);
//        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
//        result = 31 * result + (tenantId != null ? tenantId.hashCode() : 0);
//        result = 31 * result + (fileSize != null ? fileSize.hashCode() : 0);
//        result = 31 * result + (izFolder != null ? izFolder.hashCode() : 0);
//        result = 31 * result + (izRootFolder != null ? izRootFolder.hashCode() : 0);
//        result = 31 * result + (izStar != null ? izStar.hashCode() : 0);
//        result = 31 * result + (downCount != null ? downCount.hashCode() : 0);
//        result = 31 * result + (readCount != null ? readCount.hashCode() : 0);
//        result = 31 * result + (shareUrl != null ? shareUrl.hashCode() : 0);
//        result = 31 * result + (sharePerms != null ? sharePerms.hashCode() : 0);
//        result = 31 * result + (enableDown != null ? enableDown.hashCode() : 0);
//        result = 31 * result + (enableUpdat != null ? enableUpdat.hashCode() : 0);
//        result = 31 * result + (delFlag != null ? delFlag.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        return result;
//    }
}
