package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_depart_role_permission")
public class SysDepartRolePermission {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "depart_id")
    private Integer departId;
    @Basic
    @Column(name = "role_id")
    private Integer roleId;
    @Basic
    @Column(name = "permission_id")
    private Integer permissionId;
    @Basic
    @Column(name = "data_rule_ids")
    private String dataRuleIds;
    @Basic
    @Column(name = "operate_date")
    private Timestamp operateDate;
    @Basic
    @Column(name = "operate_ip")
    private String operateIp;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getDepartId() {
//        return departId;
//    }
//
//    public void setDepartId(String departId) {
//        this.departId = departId;
//    }
//
//    public String getRoleId() {
//        return roleId;
//    }
//
//    public void setRoleId(String roleId) {
//        this.roleId = roleId;
//    }
//
//    public String getPermissionId() {
//        return permissionId;
//    }
//
//    public void setPermissionId(String permissionId) {
//        this.permissionId = permissionId;
//    }
//
//    public String getDataRuleIds() {
//        return dataRuleIds;
//    }
//
//    public void setDataRuleIds(String dataRuleIds) {
//        this.dataRuleIds = dataRuleIds;
//    }
//
//    public Timestamp getOperateDate() {
//        return operateDate;
//    }
//
//    public void setOperateDate(Timestamp operateDate) {
//        this.operateDate = operateDate;
//    }
//
//    public String getOperateIp() {
//        return operateIp;
//    }
//
//    public void setOperateIp(String operateIp) {
//        this.operateIp = operateIp;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysDepartRolePermission that = (SysDepartRolePermission) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (departId != null ? !departId.equals(that.departId) : that.departId != null) return false;
//        if (roleId != null ? !roleId.equals(that.roleId) : that.roleId != null) return false;
//        if (permissionId != null ? !permissionId.equals(that.permissionId) : that.permissionId != null) return false;
//        if (dataRuleIds != null ? !dataRuleIds.equals(that.dataRuleIds) : that.dataRuleIds != null) return false;
//        if (operateDate != null ? !operateDate.equals(that.operateDate) : that.operateDate != null) return false;
//        if (operateIp != null ? !operateIp.equals(that.operateIp) : that.operateIp != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (departId != null ? departId.hashCode() : 0);
//        result = 31 * result + (roleId != null ? roleId.hashCode() : 0);
//        result = 31 * result + (permissionId != null ? permissionId.hashCode() : 0);
//        result = 31 * result + (dataRuleIds != null ? dataRuleIds.hashCode() : 0);
//        result = 31 * result + (operateDate != null ? operateDate.hashCode() : 0);
//        result = 31 * result + (operateIp != null ? operateIp.hashCode() : 0);
//        return result;
//    }
}
