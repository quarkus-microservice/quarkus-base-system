package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_third_app_config")
public class SysThirdAppConfig {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "tenant_id")
    private Integer tenantId;
    @Basic
    @Column(name = "agent_id")
    private Integer agentId;
    @Basic
    @Column(name = "client_id")
    private Integer clientId;
    @Basic
    @Column(name = "client_secret")
    private String clientSecret;
    @Basic
    @Column(name = "third_type")
    private String thirdType;
    @Basic
    @Column(name = "agent_app_secret")
    private String agentAppSecret;
    @Basic
    @Column(name = "status")
    private Integer status;


//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public Integer getTenantId() {
//        return tenantId;
//    }
//
//    public void setTenantId(Integer tenantId) {
//        this.tenantId = tenantId;
//    }
//
//    public String getAgentId() {
//        return agentId;
//    }
//
//    public void setAgentId(String agentId) {
//        this.agentId = agentId;
//    }
//
//    public String getClientId() {
//        return clientId;
//    }
//
//    public void setClientId(String clientId) {
//        this.clientId = clientId;
//    }
//
//    public String getClientSecret() {
//        return clientSecret;
//    }
//
//    public void setClientSecret(String clientSecret) {
//        this.clientSecret = clientSecret;
//    }
//
//    public String getThirdType() {
//        return thirdType;
//    }
//
//    public void setThirdType(String thirdType) {
//        this.thirdType = thirdType;
//    }
//
//    public String getAgentAppSecret() {
//        return agentAppSecret;
//    }
//
//    public void setAgentAppSecret(String agentAppSecret) {
//        this.agentAppSecret = agentAppSecret;
//    }
//
//    public Integer getStatus() {
//        return status;
//    }
//
//    public void setStatus(Integer status) {
//        this.status = status;
//    }
//
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.createTime = createTime;
//    }
//
//    public Timestamp getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Timestamp updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysThirdAppConfig that = (SysThirdAppConfig) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (tenantId != null ? !tenantId.equals(that.tenantId) : that.tenantId != null) return false;
//        if (agentId != null ? !agentId.equals(that.agentId) : that.agentId != null) return false;
//        if (clientId != null ? !clientId.equals(that.clientId) : that.clientId != null) return false;
//        if (clientSecret != null ? !clientSecret.equals(that.clientSecret) : that.clientSecret != null) return false;
//        if (thirdType != null ? !thirdType.equals(that.thirdType) : that.thirdType != null) return false;
//        if (agentAppSecret != null ? !agentAppSecret.equals(that.agentAppSecret) : that.agentAppSecret != null)
//            return false;
//        if (status != null ? !status.equals(that.status) : that.status != null) return false;
//        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
//        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (tenantId != null ? tenantId.hashCode() : 0);
//        result = 31 * result + (agentId != null ? agentId.hashCode() : 0);
//        result = 31 * result + (clientId != null ? clientId.hashCode() : 0);
//        result = 31 * result + (clientSecret != null ? clientSecret.hashCode() : 0);
//        result = 31 * result + (thirdType != null ? thirdType.hashCode() : 0);
//        result = 31 * result + (agentAppSecret != null ? agentAppSecret.hashCode() : 0);
//        result = 31 * result + (status != null ? status.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        return result;
//    }
}
