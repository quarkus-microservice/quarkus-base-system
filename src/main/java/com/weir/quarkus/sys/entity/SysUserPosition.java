package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.weir.quarkus.base.system.entity.MyIdGenerator;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Getter
@Setter
@Entity
@Table(name = "sys_user_position")
public class SysUserPosition extends PanacheEntityBase {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "user_id")
    private String userId;
    @Basic
    @Column(name = "position_id")
    private String positionId;

}
