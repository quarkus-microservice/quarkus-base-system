package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_role_index")
public class SysRoleIndex {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "role_code")
    private String roleCode;
    @Basic
    @Column(name = "url")
    private String url;
    @Basic
    @Column(name = "component")
    private String component;
    @Basic
    @Column(name = "is_route")
    private Byte isRoute;
    @Basic
    @Column(name = "priority")
    private Integer priority;
    @Basic
    @Column(name = "status")
    private String status;

    @Basic
    @Column(name = "sys_org_code")
    private String sysOrgCode;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getRoleCode() {
//        return roleCode;
//    }
//
//    public void setRoleCode(String roleCode) {
//        this.roleCode = roleCode;
//    }
//
//    public String getUrl() {
//        return url;
//    }
//
//    public void setUrl(String url) {
//        this.url = url;
//    }
//
//    public String getComponent() {
//        return component;
//    }
//
//    public void setComponent(String component) {
//        this.component = component;
//    }
//
//    public Byte getIsRoute() {
//        return isRoute;
//    }
//
//    public void setIsRoute(Byte isRoute) {
//        this.isRoute = isRoute;
//    }
//
//    public Integer getPriority() {
//        return priority;
//    }
//
//    public void setPriority(Integer priority) {
//        this.priority = priority;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.createTime = createTime;
//    }
//
//    public String getUpdateBy() {
//        return updateBy;
//    }
//
//    public void setUpdateBy(String updateBy) {
//        this.updateBy = updateBy;
//    }
//
//    public Timestamp getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Timestamp updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    public String getSysOrgCode() {
//        return sysOrgCode;
//    }
//
//    public void setSysOrgCode(String sysOrgCode) {
//        this.sysOrgCode = sysOrgCode;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysRoleIndex that = (SysRoleIndex) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (roleCode != null ? !roleCode.equals(that.roleCode) : that.roleCode != null) return false;
//        if (url != null ? !url.equals(that.url) : that.url != null) return false;
//        if (component != null ? !component.equals(that.component) : that.component != null) return false;
//        if (isRoute != null ? !isRoute.equals(that.isRoute) : that.isRoute != null) return false;
//        if (priority != null ? !priority.equals(that.priority) : that.priority != null) return false;
//        if (status != null ? !status.equals(that.status) : that.status != null) return false;
//        if (createBy != null ? !createBy.equals(that.createBy) : that.createBy != null) return false;
//        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
//        if (updateBy != null ? !updateBy.equals(that.updateBy) : that.updateBy != null) return false;
//        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
//        if (sysOrgCode != null ? !sysOrgCode.equals(that.sysOrgCode) : that.sysOrgCode != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (roleCode != null ? roleCode.hashCode() : 0);
//        result = 31 * result + (url != null ? url.hashCode() : 0);
//        result = 31 * result + (component != null ? component.hashCode() : 0);
//        result = 31 * result + (isRoute != null ? isRoute.hashCode() : 0);
//        result = 31 * result + (priority != null ? priority.hashCode() : 0);
//        result = 31 * result + (status != null ? status.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        result = 31 * result + (sysOrgCode != null ? sysOrgCode.hashCode() : 0);
//        return result;
//    }
}
