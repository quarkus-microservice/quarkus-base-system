package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.SoftDelete;
import org.hibernate.annotations.Where;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.weir.quarkus.base.system.entity.BaseEntity;
import com.weir.quarkus.base.system.entity.MyIdGenerator;

@Getter
@Setter
//@Builder
@Entity
@Table(name = "sys_dict")
@SQLDelete(sql = "update sys_dict set del_flag = true where id = ?")
//@Where(clause = "delFlag = false")
@SQLRestriction("del_flag = false")
public class SysDict extends BaseEntity {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
	
	@Excel(name = "字典名称", width = 20)
    @Basic
    @Column(name = "dict_name")
    private String dictName;
	
	@Excel(name = "字典编码", width = 30)
    @Basic
    @Column(name = "dict_code")
    private String dictCode;
	
	@Excel(name = "描述", width = 30)
    @Basic
    @Column(name = "description")
    private String description;

    @Basic
    @Column(name = "type")
    private Integer type;
    @Basic
    @Column(name = "tenant_id")
    private Integer tenantId;
    @Basic
    @Column(name = "low_app_id")
    private String lowAppId;
    
    @Transient
    @ExcelCollection(name = "字典列表")
    private List<SysDictItem> sysDictItemList;

}
