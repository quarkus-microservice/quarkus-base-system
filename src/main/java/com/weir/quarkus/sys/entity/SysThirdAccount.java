package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_third_account")
public class SysThirdAccount {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "sys_user_id")
    private Integer sysUserId;
    @Basic
    @Column(name = "avatar")
    private String avatar;
    @Basic
    @Column(name = "status")
    private Byte status;
    @Basic
    @Column(name = "del_flag")
    private Byte delFlag;
    @Basic
    @Column(name = "realname")
    private String realname;
    @Basic
    @Column(name = "tenant_id")
    private Integer tenantId;
    @Basic
    @Column(name = "third_user_uuid")
    private Integer thirdUserUuid;
    @Basic
    @Column(name = "third_user_id")
    private Integer thirdUserId;

    @Basic
    @Column(name = "third_type")
    private String thirdType;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getSysUserId() {
//        return sysUserId;
//    }
//
//    public void setSysUserId(String sysUserId) {
//        this.sysUserId = sysUserId;
//    }
//
//    public String getAvatar() {
//        return avatar;
//    }
//
//    public void setAvatar(String avatar) {
//        this.avatar = avatar;
//    }
//
//    public Byte getStatus() {
//        return status;
//    }
//
//    public void setStatus(Byte status) {
//        this.status = status;
//    }
//
//    public Byte getDelFlag() {
//        return delFlag;
//    }
//
//    public void setDelFlag(Byte delFlag) {
//        this.delFlag = delFlag;
//    }
//
//    public String getRealname() {
//        return realname;
//    }
//
//    public void setRealname(String realname) {
//        this.realname = realname;
//    }
//
//    public Integer getTenantId() {
//        return tenantId;
//    }
//
//    public void setTenantId(Integer tenantId) {
//        this.tenantId = tenantId;
//    }
//
//    public String getThirdUserUuid() {
//        return thirdUserUuid;
//    }
//
//    public void setThirdUserUuid(String thirdUserUuid) {
//        this.thirdUserUuid = thirdUserUuid;
//    }
//
//    public String getThirdUserId() {
//        return thirdUserId;
//    }
//
//    public void setThirdUserId(String thirdUserId) {
//        this.thirdUserId = thirdUserId;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.createTime = createTime;
//    }
//
//    public String getUpdateBy() {
//        return updateBy;
//    }
//
//    public void setUpdateBy(String updateBy) {
//        this.updateBy = updateBy;
//    }
//
//    public Timestamp getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Timestamp updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    public String getThirdType() {
//        return thirdType;
//    }
//
//    public void setThirdType(String thirdType) {
//        this.thirdType = thirdType;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysThirdAccount that = (SysThirdAccount) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (sysUserId != null ? !sysUserId.equals(that.sysUserId) : that.sysUserId != null) return false;
//        if (avatar != null ? !avatar.equals(that.avatar) : that.avatar != null) return false;
//        if (status != null ? !status.equals(that.status) : that.status != null) return false;
//        if (delFlag != null ? !delFlag.equals(that.delFlag) : that.delFlag != null) return false;
//        if (realname != null ? !realname.equals(that.realname) : that.realname != null) return false;
//        if (tenantId != null ? !tenantId.equals(that.tenantId) : that.tenantId != null) return false;
//        if (thirdUserUuid != null ? !thirdUserUuid.equals(that.thirdUserUuid) : that.thirdUserUuid != null)
//            return false;
//        if (thirdUserId != null ? !thirdUserId.equals(that.thirdUserId) : that.thirdUserId != null) return false;
//        if (createBy != null ? !createBy.equals(that.createBy) : that.createBy != null) return false;
//        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
//        if (updateBy != null ? !updateBy.equals(that.updateBy) : that.updateBy != null) return false;
//        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
//        if (thirdType != null ? !thirdType.equals(that.thirdType) : that.thirdType != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (sysUserId != null ? sysUserId.hashCode() : 0);
//        result = 31 * result + (avatar != null ? avatar.hashCode() : 0);
//        result = 31 * result + (status != null ? status.hashCode() : 0);
//        result = 31 * result + (delFlag != null ? delFlag.hashCode() : 0);
//        result = 31 * result + (realname != null ? realname.hashCode() : 0);
//        result = 31 * result + (tenantId != null ? tenantId.hashCode() : 0);
//        result = 31 * result + (thirdUserUuid != null ? thirdUserUuid.hashCode() : 0);
//        result = 31 * result + (thirdUserId != null ? thirdUserId.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        result = 31 * result + (thirdType != null ? thirdType.hashCode() : 0);
//        return result;
//    }
}
