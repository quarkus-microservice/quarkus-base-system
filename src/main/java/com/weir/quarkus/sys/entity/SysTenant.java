package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_tenant")
public class SysTenant {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "begin_date")
    private Timestamp beginDate;
    @Basic
    @Column(name = "end_date")
    private Timestamp endDate;
    @Basic
    @Column(name = "status")
    private Integer status;
    @Basic
    @Column(name = "trade")
    private String trade;
    @Basic
    @Column(name = "company_size")
    private String companySize;
    @Basic
    @Column(name = "company_address")
    private String companyAddress;
    @Basic
    @Column(name = "company_logo")
    private String companyLogo;
    @Basic
    @Column(name = "house_number")
    private String houseNumber;
    @Basic
    @Column(name = "work_place")
    private String workPlace;
    @Basic
    @Column(name = "secondary_domain")
    private String secondaryDomain;
    @Basic
    @Column(name = "login_bkgd_img")
    private String loginBkgdImg;
    @Basic
    @Column(name = "position")
    private String position;
    @Basic
    @Column(name = "department")
    private String department;
    @Basic
    @Column(name = "del_flag")
    private Integer delFlag;
    @Basic
    @Column(name = "apply_status")
    private Integer applyStatus;

//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.createTime = createTime;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Timestamp getBeginDate() {
//        return beginDate;
//    }
//
//    public void setBeginDate(Timestamp beginDate) {
//        this.beginDate = beginDate;
//    }
//
//    public Timestamp getEndDate() {
//        return endDate;
//    }
//
//    public void setEndDate(Timestamp endDate) {
//        this.endDate = endDate;
//    }
//
//    public Integer getStatus() {
//        return status;
//    }
//
//    public void setStatus(Integer status) {
//        this.status = status;
//    }
//
//    public String getTrade() {
//        return trade;
//    }
//
//    public void setTrade(String trade) {
//        this.trade = trade;
//    }
//
//    public String getCompanySize() {
//        return companySize;
//    }
//
//    public void setCompanySize(String companySize) {
//        this.companySize = companySize;
//    }
//
//    public String getCompanyAddress() {
//        return companyAddress;
//    }
//
//    public void setCompanyAddress(String companyAddress) {
//        this.companyAddress = companyAddress;
//    }
//
//    public String getCompanyLogo() {
//        return companyLogo;
//    }
//
//    public void setCompanyLogo(String companyLogo) {
//        this.companyLogo = companyLogo;
//    }
//
//    public String getHouseNumber() {
//        return houseNumber;
//    }
//
//    public void setHouseNumber(String houseNumber) {
//        this.houseNumber = houseNumber;
//    }
//
//    public String getWorkPlace() {
//        return workPlace;
//    }
//
//    public void setWorkPlace(String workPlace) {
//        this.workPlace = workPlace;
//    }
//
//    public String getSecondaryDomain() {
//        return secondaryDomain;
//    }
//
//    public void setSecondaryDomain(String secondaryDomain) {
//        this.secondaryDomain = secondaryDomain;
//    }
//
//    public String getLoginBkgdImg() {
//        return loginBkgdImg;
//    }
//
//    public void setLoginBkgdImg(String loginBkgdImg) {
//        this.loginBkgdImg = loginBkgdImg;
//    }
//
//    public String getPosition() {
//        return position;
//    }
//
//    public void setPosition(String position) {
//        this.position = position;
//    }
//
//    public String getDepartment() {
//        return department;
//    }
//
//    public void setDepartment(String department) {
//        this.department = department;
//    }
//
//    public Byte getDelFlag() {
//        return delFlag;
//    }
//
//    public void setDelFlag(Byte delFlag) {
//        this.delFlag = delFlag;
//    }
//
//    public String getUpdateBy() {
//        return updateBy;
//    }
//
//    public void setUpdateBy(String updateBy) {
//        this.updateBy = updateBy;
//    }
//
//    public Timestamp getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Timestamp updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    public Integer getApplyStatus() {
//        return applyStatus;
//    }
//
//    public void setApplyStatus(Integer applyStatus) {
//        this.applyStatus = applyStatus;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysTenant sysTenant = (SysTenant) o;
//
//        if (id != null ? !id.equals(sysTenant.id) : sysTenant.id != null) return false;
//        if (name != null ? !name.equals(sysTenant.name) : sysTenant.name != null) return false;
//        if (createTime != null ? !createTime.equals(sysTenant.createTime) : sysTenant.createTime != null) return false;
//        if (createBy != null ? !createBy.equals(sysTenant.createBy) : sysTenant.createBy != null) return false;
//        if (beginDate != null ? !beginDate.equals(sysTenant.beginDate) : sysTenant.beginDate != null) return false;
//        if (endDate != null ? !endDate.equals(sysTenant.endDate) : sysTenant.endDate != null) return false;
//        if (status != null ? !status.equals(sysTenant.status) : sysTenant.status != null) return false;
//        if (trade != null ? !trade.equals(sysTenant.trade) : sysTenant.trade != null) return false;
//        if (companySize != null ? !companySize.equals(sysTenant.companySize) : sysTenant.companySize != null)
//            return false;
//        if (companyAddress != null ? !companyAddress.equals(sysTenant.companyAddress) : sysTenant.companyAddress != null)
//            return false;
//        if (companyLogo != null ? !companyLogo.equals(sysTenant.companyLogo) : sysTenant.companyLogo != null)
//            return false;
//        if (houseNumber != null ? !houseNumber.equals(sysTenant.houseNumber) : sysTenant.houseNumber != null)
//            return false;
//        if (workPlace != null ? !workPlace.equals(sysTenant.workPlace) : sysTenant.workPlace != null) return false;
//        if (secondaryDomain != null ? !secondaryDomain.equals(sysTenant.secondaryDomain) : sysTenant.secondaryDomain != null)
//            return false;
//        if (loginBkgdImg != null ? !loginBkgdImg.equals(sysTenant.loginBkgdImg) : sysTenant.loginBkgdImg != null)
//            return false;
//        if (position != null ? !position.equals(sysTenant.position) : sysTenant.position != null) return false;
//        if (department != null ? !department.equals(sysTenant.department) : sysTenant.department != null) return false;
//        if (delFlag != null ? !delFlag.equals(sysTenant.delFlag) : sysTenant.delFlag != null) return false;
//        if (updateBy != null ? !updateBy.equals(sysTenant.updateBy) : sysTenant.updateBy != null) return false;
//        if (updateTime != null ? !updateTime.equals(sysTenant.updateTime) : sysTenant.updateTime != null) return false;
//        if (applyStatus != null ? !applyStatus.equals(sysTenant.applyStatus) : sysTenant.applyStatus != null)
//            return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (name != null ? name.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (beginDate != null ? beginDate.hashCode() : 0);
//        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
//        result = 31 * result + (status != null ? status.hashCode() : 0);
//        result = 31 * result + (trade != null ? trade.hashCode() : 0);
//        result = 31 * result + (companySize != null ? companySize.hashCode() : 0);
//        result = 31 * result + (companyAddress != null ? companyAddress.hashCode() : 0);
//        result = 31 * result + (companyLogo != null ? companyLogo.hashCode() : 0);
//        result = 31 * result + (houseNumber != null ? houseNumber.hashCode() : 0);
//        result = 31 * result + (workPlace != null ? workPlace.hashCode() : 0);
//        result = 31 * result + (secondaryDomain != null ? secondaryDomain.hashCode() : 0);
//        result = 31 * result + (loginBkgdImg != null ? loginBkgdImg.hashCode() : 0);
//        result = 31 * result + (position != null ? position.hashCode() : 0);
//        result = 31 * result + (department != null ? department.hashCode() : 0);
//        result = 31 * result + (delFlag != null ? delFlag.hashCode() : 0);
//        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        result = 31 * result + (applyStatus != null ? applyStatus.hashCode() : 0);
//        return result;
//    }
}
