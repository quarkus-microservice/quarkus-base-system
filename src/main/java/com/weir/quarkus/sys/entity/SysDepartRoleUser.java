package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "sys_depart_role_user")
public class SysDepartRoleUser {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "user_id")
    private Integer userId;
    @Basic
    @Column(name = "drole_id")
    private Integer droleId;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getUserId() {
//        return userId;
//    }
//
//    public void setUserId(String userId) {
//        this.userId = userId;
//    }
//
//    public String getDroleId() {
//        return droleId;
//    }
//
//    public void setDroleId(String droleId) {
//        this.droleId = droleId;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysDepartRoleUser that = (SysDepartRoleUser) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
//        if (droleId != null ? !droleId.equals(that.droleId) : that.droleId != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (userId != null ? userId.hashCode() : 0);
//        result = 31 * result + (droleId != null ? droleId.hashCode() : 0);
//        return result;
//    }
}
