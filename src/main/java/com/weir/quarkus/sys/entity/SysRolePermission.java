package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

import org.hibernate.annotations.GenericGenerator;

import com.weir.quarkus.base.system.entity.MyIdGenerator;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Getter
@Setter
@Entity
@Table(name = "sys_role_permission")
public class SysRolePermission extends PanacheEntityBase {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "role_id")
    private String roleId;
    @Basic
    @Column(name = "permission_id")
    private String permissionId;
    @Basic
    @Column(name = "data_rule_ids")
    private String dataRuleIds;
    @Basic
    @Column(name = "operate_date")
    private LocalDateTime operateDate;
    @Basic
    @Column(name = "operate_ip")
    private String operateIp;
	/**
	 * @param roleId
	 * @param permissionId
	 */
	public SysRolePermission(String roleId, String permissionId) {
		this.roleId = roleId;
		this.permissionId = permissionId;
	}
	/**
	 * 
	 */
	public SysRolePermission() {
	}
	/**
	 * @param roleId
	 * @param permissionId
	 * @param operateDate
	 * @param operateIp
	 */
	public SysRolePermission(String roleId, String permissionId, LocalDateTime operateDate, String operateIp) {
		this.roleId = roleId;
		this.permissionId = permissionId;
		this.operateDate = operateDate;
		this.operateIp = operateIp;
	}

}
