package com.weir.quarkus.sys.entity;

import org.hibernate.annotations.GenericGenerator;

import com.weir.quarkus.base.system.entity.MyIdGenerator;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "sys_log")
public class SysLog extends BaseSysEntity {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "log_type")
    private Integer logType;
    @Basic
    @Column(name = "log_content")
    private String logContent;
    @Basic
    @Column(name = "operate_type")
    private Integer operateType;
    @Basic
    @Column(name = "userid")
    private String userid;
    @Basic
    @Column(name = "username")
    private String username;
    @Basic
    @Column(name = "ip")
    private String ip;
    @Basic
    @Column(name = "method")
    private String method;
    @Basic
    @Column(name = "request_url")
    private String requestUrl;
    @Basic
    @Column(name = "request_param")
    private String requestParam;
    @Basic
    @Column(name = "request_type")
    private String requestType;
    @Basic
    @Column(name = "cost_time")
    private Long costTime;
    @Basic
    @Column(name = "tenant_id")
    private Integer tenantId;

}
