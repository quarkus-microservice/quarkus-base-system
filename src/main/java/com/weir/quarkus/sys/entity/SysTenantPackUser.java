package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_tenant_pack_user")
public class SysTenantPackUser {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "pack_id")
    private String packId;
    @Basic
    @Column(name = "user_id")
    private Integer userId;
    @Basic
    @Column(name = "tenant_id")
    private Integer tenantId;

    @Basic
    @Column(name = "status")
    private Integer status;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getPackId() {
//        return packId;
//    }
//
//    public void setPackId(String packId) {
//        this.packId = packId;
//    }
//
//    public String getUserId() {
//        return userId;
//    }
//
//    public void setUserId(String userId) {
//        this.userId = userId;
//    }
//
//    public Integer getTenantId() {
//        return tenantId;
//    }
//
//    public void setTenantId(Integer tenantId) {
//        this.tenantId = tenantId;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.createTime = createTime;
//    }
//
//    public String getUpdateBy() {
//        return updateBy;
//    }
//
//    public void setUpdateBy(String updateBy) {
//        this.updateBy = updateBy;
//    }
//
//    public Timestamp getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Timestamp updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    public Integer getStatus() {
//        return status;
//    }
//
//    public void setStatus(Integer status) {
//        this.status = status;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysTenantPackUser that = (SysTenantPackUser) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (packId != null ? !packId.equals(that.packId) : that.packId != null) return false;
//        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
//        if (tenantId != null ? !tenantId.equals(that.tenantId) : that.tenantId != null) return false;
//        if (createBy != null ? !createBy.equals(that.createBy) : that.createBy != null) return false;
//        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
//        if (updateBy != null ? !updateBy.equals(that.updateBy) : that.updateBy != null) return false;
//        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
//        if (status != null ? !status.equals(that.status) : that.status != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (packId != null ? packId.hashCode() : 0);
//        result = 31 * result + (userId != null ? userId.hashCode() : 0);
//        result = 31 * result + (tenantId != null ? tenantId.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        result = 31 * result + (status != null ? status.hashCode() : 0);
//        return result;
//    }
}
