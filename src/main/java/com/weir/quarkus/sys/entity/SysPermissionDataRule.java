package com.weir.quarkus.sys.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sys_permission_data_rule")
public class SysPermissionDataRule {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "permission_id")
    private String permissionId;
    @Basic
    @Column(name = "rule_name")
    private String ruleName;
    @Basic
    @Column(name = "rule_column")
    private String ruleColumn;
    @Basic
    @Column(name = "rule_conditions")
    private String ruleConditions;
    @Basic
    @Column(name = "rule_value")
    private String ruleValue;
    @Basic
    @Column(name = "status")
    private Integer status;


//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getPermissionId() {
//        return permissionId;
//    }
//
//    public void setPermissionId(String permissionId) {
//        this.permissionId = permissionId;
//    }
//
//    public String getRuleName() {
//        return ruleName;
//    }
//
//    public void setRuleName(String ruleName) {
//        this.ruleName = ruleName;
//    }
//
//    public String getRuleColumn() {
//        return ruleColumn;
//    }
//
//    public void setRuleColumn(String ruleColumn) {
//        this.ruleColumn = ruleColumn;
//    }
//
//    public String getRuleConditions() {
//        return ruleConditions;
//    }
//
//    public void setRuleConditions(String ruleConditions) {
//        this.ruleConditions = ruleConditions;
//    }
//
//    public String getRuleValue() {
//        return ruleValue;
//    }
//
//    public void setRuleValue(String ruleValue) {
//        this.ruleValue = ruleValue;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Timestamp createTime) {
//        this.createTime = createTime;
//    }
//
//    public String getCreateBy() {
//        return createBy;
//    }
//
//    public void setCreateBy(String createBy) {
//        this.createBy = createBy;
//    }
//
//    public Timestamp getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Timestamp updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    public String getUpdateBy() {
//        return updateBy;
//    }
//
//    public void setUpdateBy(String updateBy) {
//        this.updateBy = updateBy;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysPermissionDataRule that = (SysPermissionDataRule) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (permissionId != null ? !permissionId.equals(that.permissionId) : that.permissionId != null) return false;
//        if (ruleName != null ? !ruleName.equals(that.ruleName) : that.ruleName != null) return false;
//        if (ruleColumn != null ? !ruleColumn.equals(that.ruleColumn) : that.ruleColumn != null) return false;
//        if (ruleConditions != null ? !ruleConditions.equals(that.ruleConditions) : that.ruleConditions != null)
//            return false;
//        if (ruleValue != null ? !ruleValue.equals(that.ruleValue) : that.ruleValue != null) return false;
//        if (status != null ? !status.equals(that.status) : that.status != null) return false;
//        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
//        if (createBy != null ? !createBy.equals(that.createBy) : that.createBy != null) return false;
//        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
//        if (updateBy != null ? !updateBy.equals(that.updateBy) : that.updateBy != null) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (permissionId != null ? permissionId.hashCode() : 0);
//        result = 31 * result + (ruleName != null ? ruleName.hashCode() : 0);
//        result = 31 * result + (ruleColumn != null ? ruleColumn.hashCode() : 0);
//        result = 31 * result + (ruleConditions != null ? ruleConditions.hashCode() : 0);
//        result = 31 * result + (ruleValue != null ? ruleValue.hashCode() : 0);
//        result = 31 * result + (status != null ? status.hashCode() : 0);
//        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
//        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
//        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
//        result = 31 * result + (updateBy != null ? updateBy.hashCode() : 0);
//        return result;
//    }
}
