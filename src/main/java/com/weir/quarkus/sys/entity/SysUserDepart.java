package com.weir.quarkus.sys.entity;

import org.hibernate.annotations.GenericGenerator;

import com.weir.quarkus.base.system.entity.MyIdGenerator;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "sys_user_depart")
public class SysUserDepart extends PanacheEntityBase {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "ID")
    private String id;
    @Basic
    @Column(name = "user_id")
    private String userId;
    @Basic
    @Column(name = "dep_id")
    private String depId;
	/**
	 * @param userId
	 * @param depId
	 */
	public SysUserDepart(String userId, String depId) {
		this.userId = userId;
		this.depId = depId;
	}
	/**
	 * 
	 */
	public SysUserDepart() {
	}

}
