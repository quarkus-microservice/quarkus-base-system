package com.weir.quarkus.sys.entity;

import org.hibernate.annotations.GenericGenerator;

import com.weir.quarkus.base.system.entity.MyIdGenerator;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "sys_position")
public class SysPosition extends BaseSysEntity {
	@GenericGenerator(name = "sequence_id", type = MyIdGenerator.class )
    @GeneratedValue(generator = "sequence_id")
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "code")
    private String code;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "post_rank")
    private String postRank;
    @Basic
    @Column(name = "company_id")
    private String companyId;

    @Basic
    @Column(name = "sys_org_code")
    private String sysOrgCode;
    @Basic
    @Column(name = "tenant_id")
    private Integer tenantId;

}
