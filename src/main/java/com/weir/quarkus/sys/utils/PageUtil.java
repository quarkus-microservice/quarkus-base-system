/** 
 * @Title: PageUtil.java 
 * @Package com.weir.quarkus.sys.utils 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月12日 09:17:46 
 * @version V1.0 
 */
package com.weir.quarkus.sys.utils;

import java.util.List;

import com.weir.quarkus.sys.vo.PageDTO;

/** 
 * @Title: PageUtil.java 
 * @Package com.weir.quarkus.sys.utils 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月12日 09:17:46 
 * @version V1.0 
 */
public class PageUtil {

	/**
	 * @param page
	 * @param size
	 * @param count
	 * @param resultList
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static PageDTO toPage(int page, int size, int count, List list) {
		PageDTO pageDTO = new PageDTO<>();
		pageDTO.setCurrent(page);
		pageDTO.setRecords(list);
		pageDTO.setSize(size);
		pageDTO.setTotal(count);
		return pageDTO;
	}

	
}
