/** 
 * @Title: MergeEntityCopyUtil.java 
 * @Package com.weir.quarkus.sys.utils 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月10日 21:51:08 
 * @version V1.0 
 */
package com.weir.quarkus.sys.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.LocalDateTime;

/** 
 * @Title: MergeEntityCopyUtil.java 
 * @Package com.weir.quarkus.sys.utils 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月10日 21:51:08 
 * @version V1.0 
 */
public class MergeEntityCopyUtil {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <T> T updateEntity(T entity, T oldEntity) {
		Class oldClass = oldEntity.getClass();
		Class clazz = entity.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) { // 遍历属性
			try {
				PropertyDescriptor pd = new PropertyDescriptor(field.getName(), clazz);
				Method readMethod = pd.getReadMethod(); // 获取属性的get方法
				Method writeMethod = pd.getWriteMethod(); // 获取属性的set方法
				Object invoke = readMethod.invoke(entity);
				if (invoke != null) {
					Method declaredMethod = null;
					if (field.getType() == String.class) {
						declaredMethod = oldClass.getDeclaredMethod(writeMethod.getName(), String.class);
					}else if (field.getType() == Integer.class) {
						declaredMethod = oldClass.getDeclaredMethod(writeMethod.getName(), Integer.class);						
					}else if (field.getType() == Double.class) {
						declaredMethod = oldClass.getDeclaredMethod(writeMethod.getName(), Double.class);						
					}else if (field.getType() == Long.class) {
						declaredMethod = oldClass.getDeclaredMethod(writeMethod.getName(), Long.class);						
					}else if (field.getType() == Boolean.class) {
						declaredMethod = oldClass.getDeclaredMethod(writeMethod.getName(), Boolean.class);						
					}else if (field.getType() == Short.class) {
						declaredMethod = oldClass.getDeclaredMethod(writeMethod.getName(), Short.class);						
					}else if (field.getType() == Float.class) {
						declaredMethod = oldClass.getDeclaredMethod(writeMethod.getName(), Float.class);						
					}else if (field.getType() == LocalDateTime.class) {
						declaredMethod = oldClass.getDeclaredMethod(writeMethod.getName(), LocalDateTime.class);						
					}else if (field.getType() == LocalDate.class) {
						declaredMethod = oldClass.getDeclaredMethod(writeMethod.getName(), LocalDate.class);						
					}
					if (declaredMethod != null) {						
//						declaredMethod.setAccessible(true);
						declaredMethod.invoke(oldEntity, invoke);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return oldEntity;
	}
}
