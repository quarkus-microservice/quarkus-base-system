/** 
 * @Title: SysPermissionMapper.java 
 * @Package com.weir.quarkus.sys.mapper 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月10日 15:23:10 
 * @version V1.0 
 */
package com.weir.quarkus.sys.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import com.weir.quarkus.sys.entity.SysPermission;
import com.weir.quarkus.sys.vo.SysPermissionDTO;

import org.mapstruct.MappingConstants.ComponentModel;

/** 
 * @Title: SysPermissionMapper.java 
 * @Package com.weir.quarkus.sys.mapper 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月10日 15:23:10 
 * @version V1.0 
 */
@Mapper(componentModel = ComponentModel.JAKARTA, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SysPermissionMapper {

	SysPermission toEntity(SysPermissionDTO dto);
}
