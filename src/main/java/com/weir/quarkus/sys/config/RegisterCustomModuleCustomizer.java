//package com.weir.quarkus.sys.config;
//import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.SerializationFeature;
//import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
//import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
//import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
//import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
//
//import io.quarkus.jackson.ObjectMapperCustomizer;
//import jakarta.enterprise.context.ApplicationScoped;
//import jakarta.inject.Singleton;
//
//import java.text.SimpleDateFormat;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//
//
//@Singleton
//public class RegisterCustomModuleCustomizer implements ObjectMapperCustomizer {
//
//    public void customize(ObjectMapper objectMapper) {
////        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
////        objectMapper.disable(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS);
////        objectMapper.disable(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS);
////
////        JavaTimeModule module = new JavaTimeModule();
////        LocalDateTimeDeserializer localDateTimeDeserializer = new LocalDateTimeDeserializer(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
////        module.addDeserializer(LocalDateTime.class, localDateTimeDeserializer);
////        objectMapper.registerModule(module);
//    	
//    	objectMapper.findAndRegisterModules();
//        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//        objectMapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);
//        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
//        objectMapper.registerModule(new JavaTimeModule());
//        objectMapper.registerModule(new ParameterNamesModule());
//        objectMapper.registerModule(new Jdk8Module());
//        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//    	System.out.println("-----------------------RegisterCustomModuleCustomizer-------------------------");
//    }
//}