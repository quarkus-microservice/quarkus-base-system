//package com.weir.quarkus.sys.config;
//import java.lang.annotation.Annotation;
//import java.lang.reflect.Type;
//import java.time.LocalDateTime;
//
//import jakarta.ws.rs.ext.ParamConverter;
//import jakarta.ws.rs.ext.ParamConverterProvider;
//import jakarta.ws.rs.ext.Provider;
//
////import javax.ws.rs.ext.ParamConverter;
////import javax.ws.rs.ext.ParamConverterProvider;
////import javax.ws.rs.ext.Provider;
//
//@Provider
//public class LocalDateTimeParamConverterProvider implements ParamConverterProvider {
//    @SuppressWarnings("unchecked")
//    @Override
//    public <T> ParamConverter<T> getConverter(Class<T> rawType,
//                                              Type genericType,
//                                              Annotation[] annotations) {
//    	System.out.println("--------LocalDateTimeParamConverterProvider------");
//        if (rawType.isAssignableFrom(LocalDateTime.class)) {
//            return (ParamConverter<T>) new LocalDateTimeParamConverter();
//        }
//        return null;
//    }
//}