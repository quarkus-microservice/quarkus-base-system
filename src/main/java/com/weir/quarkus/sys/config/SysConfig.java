/** 
 * @Title: SysConfig.java 
 * @Package com.weir.quarkus.sys.config 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月4日 10:52:05 
 * @version V1.0 
 */
package com.weir.quarkus.sys.config;

import io.quarkus.runtime.annotations.StaticInitSafe;
import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/** 
 * @Title: SysConfig.java 
 * @Package com.weir.quarkus.sys.config 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月4日 10:52:05 
 * @version V1.0 
 */
@StaticInitSafe
@ConfigMapping(prefix = "sys")
public interface SysConfig {
	/**
     * 签名密钥串(字典等敏感接口)
     */
	@WithDefault("dd05f1c54d63749eda95f9fa6d49v442a")
    String signatureSecret();
    /**
     * 需要加强校验的接口清单
     */
	@WithDefault("")
    String signUrls();
    /**
     * 上传模式  
     * 本地：local\Minio：minio\阿里云：alioss
     */
	@WithDefault("")
    String uploadType();
    
    /**
     * 平台安全模式配置
     */
    Firewall firewall();
    
    interface Firewall{
    	/**
         * 数据源安全 (开启后，Online报表和图表的数据源为必填)
         */
    	@WithDefault("false")
        Boolean dataSourceSafe();
        /**
         * 低代码模式（dev:开发模式，prod:发布模式——关闭所有在线开发配置能力）
         */
    	@WithDefault("dev")
        String lowCodeMode();
    }
    
    /**
     * 上传文件配置
     */
    Path path();

    interface Path{
    	@WithDefault("")
    	String upload();
    	@WithDefault("")
        String webapp();
    }
    /**
     * 前端页面访问地址
     * pc: http://localhost:3100
     * app: http://localhost:8051
     */
    DomainUrl domainUrl();

    interface DomainUrl{
    	@WithDefault("")
    	String pc();
    	@WithDefault("")
        String app();
    }
    /**
     * 文件预览
     */
    @WithDefault("")
    String fileViewDomain();
     /**
     * ES配置
     */
    Elasticsearch elasticsearch();

    interface Elasticsearch{
    	@WithDefault("")
    	String clusterNodes();
    	@WithDefault("false")
        boolean checkEnabled();
    }
    /**
     * 微信支付
     * @return
     */
    WeiXinPay weiXinPay();
    interface WeiXinPay{
    	/**
         * 微信公众号id
         */
    	@WithDefault("")
        String appId();
        /**
         * 商户号id
         */
    	@WithDefault("")
        String mchId();
        /**
         * 商户号秘钥
         */
    	@WithDefault("")
        String apiKey();
        /**
         * 回调地址
         */
    	@WithDefault("")
        String notifyUrl();
        /**
         * 是否开启会员认证
         */
    	@WithDefault("")
        Boolean openVipLimit();
        /**
         * 证书路径
         */
    	@WithDefault("")
        String certPath();
    }
    
}
