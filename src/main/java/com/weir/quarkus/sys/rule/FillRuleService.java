/** 
 * @Title: FillRuleService.java 
 * @Package com.weir.quarkus.sys.rule 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月19日 14:48:19 
 * @version V1.0 
 */
package com.weir.quarkus.sys.rule;

import java.util.HashMap;
import java.util.Map;

//import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weir.quarkus.sys.entity.SysFillRule;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;

/** 
 * @Title: FillRuleService.java 
 * @Package com.weir.quarkus.sys.rule 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月19日 14:48:19 
 * @version V1.0 
 */
@ApplicationScoped
public class FillRuleService {

	@Inject
    Instance<IFillRuleHandler> instance;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object executeRule(String ruleCode, Map<String,Object> formData) {
		SysFillRule fillRule = SysFillRule.find("ruleCode", ruleCode).singleResult();
		if (fillRule == null) {
			return null;
		}
		String ruleClass = fillRule.getRuleClass();
//		JSONObject params = JSONObject.parseObject(fillRule.getRuleParams());
		Map params = null;
		try {
			params = mapper.readValue(fillRule.getRuleParams(), Map.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Class clazz = null;
		try {
			clazz = Class.forName(ruleClass);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (formData == null) {
            formData = new HashMap<String, Object>();
        }
		return ((IFillRuleHandler) instance.select(clazz).get()).execute(params, formData);
	}
}
