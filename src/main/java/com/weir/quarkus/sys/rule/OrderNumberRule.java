package com.weir.quarkus.sys.rule;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;

//import com.alibaba.fastjson.JSONObject;

import jakarta.enterprise.context.ApplicationScoped;

/**
 * 填值规则Demo：生成订单号
 * 【测试示例】
 */
@ApplicationScoped
public class OrderNumberRule implements IFillRuleHandler {

    @Override
    public Object execute(Map<String,Object> params, Map<String,Object> formData) {
        String prefix = "CN";
        //订单前缀默认为CN 如果规则参数不为空，则取自定义前缀
        if (prefix != null) {
            Object obj = params.get("prefix");
            if (obj != null) prefix = obj.toString();
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
//        int random = RandomUtils.nextInt(90) + 10;
        Random r = new Random();
        int random = r.nextInt(90) + 10;
        String value = prefix + format.format(new Date()) + random;
        // 根据formData的值的不同，生成不同的订单号
        String name = (String) formData.get("name");
//        String name = formData.getString("name");
        if (StringUtils.isNotBlank(name)) {
            value += name;
        }
        return value;
    }

}
