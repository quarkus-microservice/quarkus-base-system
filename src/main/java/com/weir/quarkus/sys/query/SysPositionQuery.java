package com.weir.quarkus.sys.query;

import com.weir.quarkus.base.system.utils.Query;
import com.weir.quarkus.base.system.utils.Query.Type;

import jakarta.ws.rs.QueryParam;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysPositionQuery extends BaseSysQuery {
    @QueryParam(value = "id")
    private String id;
    @QueryParam(value = "code")
    private String code;
    @Query(type = Type.INNER_LIKE)
    @QueryParam(value = "name")
    private String name;
    @QueryParam(value = "postRank")
    private String postRank;
    @QueryParam(value = "companyId")
    private String companyId;

    @QueryParam(value = "sysOrgCode")
    private String sysOrgCode;
    @QueryParam(value = "tenantId")
    private Integer tenantId;

}
