package com.weir.quarkus.sys.query;

import jakarta.ws.rs.QueryParam;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

import com.weir.quarkus.base.system.utils.Query;

@Getter
@Setter
public class SysUserQuery extends BaseSysQuery {
    private String id;
    @Query()
    @QueryParam(value = "username")
    private String username;
    @Query()
    @QueryParam(value = "realname")
    private String realname;
    @QueryParam(value = "salt")
    private String salt;
    @QueryParam(value = "avatar")
    private String avatar;
    @QueryParam(value = "birthday")
    private LocalDate birthday;
    @Query()
    @QueryParam(value = "sex")
    private Integer sex;
    @QueryParam(value = "email")
    private String email;
    @Query()
    @QueryParam(value = "phone")
    private String phone;
    @QueryParam(value = "orgCode")
    private String orgCode;
    @Query()
    @QueryParam(value = "status")
    private Integer status;
    @QueryParam(value = "thirdId")
    private String thirdId;
    @QueryParam(value = "thirdType")
    private String thirdType;
    @QueryParam(value = "activitiSync")
    private Integer activitiSync;
    @QueryParam(value = "workNo")
    private String workNo;
    @QueryParam(value = "telephone")
    private String telephone;

    @QueryParam(value = "userIdentity")
    private Integer userIdentity;
    @QueryParam(value = "departIds")
    private String departIds;
    @QueryParam(value = "clientId")
    private String clientId;
    @QueryParam(value = "loginTenantId")
    private Integer loginTenantId;
    @QueryParam(value = "bpmStatus")
    private String bpmStatus;
    
    @Query()
    @QueryParam(value = "roleId")
    private String roleId;

}
