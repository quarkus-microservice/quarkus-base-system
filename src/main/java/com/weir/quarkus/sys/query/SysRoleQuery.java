package com.weir.quarkus.sys.query;

import jakarta.ws.rs.QueryParam;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysRoleQuery extends BaseSysQuery {
	
	@QueryParam(value = "id")
    private String id;
	@QueryParam(value = "roleName")
    private String roleName;
	@QueryParam(value = "roleCode")
    private String roleCode;
	@QueryParam(value = "description")
    private String description;

}
