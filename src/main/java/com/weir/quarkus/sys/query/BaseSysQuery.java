package com.weir.quarkus.sys.query;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.QueryParam;

@Data
public class BaseSysQuery {

	@QueryParam(value = "pageNo")
	@DefaultValue(value = "1")
	private Integer page = 1;
	@QueryParam(value = "pageSize")
	@DefaultValue(value = "10")
	private Integer pageSize = 10;

	@QueryParam(value = "createTime")
	private LocalDateTime createTime;

	@QueryParam(value = "updateTime")
	private LocalDateTime updateTime;

	@QueryParam(value = "createBy")
	private String createBy;
	@QueryParam(value = "updateBy")
	private String updateBy;

	@QueryParam(value = "remark")
	private String remark;
	
	@QueryParam(value = "delFlag")
    private Boolean delFlag;
	
	@QueryParam(value = "sort")
	private List<String> sort;
	
	@QueryParam(value = "column")
	private String column;
	@QueryParam(value = "order")
	private String order;
	@QueryParam(value = "selections")
	private String selections;
}
