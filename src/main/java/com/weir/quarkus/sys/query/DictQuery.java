package com.weir.quarkus.sys.query;

import jakarta.ws.rs.QueryParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
//@EqualsAndHashCode(callSuper=false)
public class DictQuery extends BaseSysQuery {

	@QueryParam(value = "id")
	private Integer id;
    @QueryParam(value = "dictName")
    private String dictName;
    @QueryParam(value = "dictCode")
    private String dictCode;

    @QueryParam(value = "type")
    private Integer type;
//    private Integer tenantId;
//    private String lowAppId;
}
