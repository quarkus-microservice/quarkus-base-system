package com.weir.quarkus.sys.query;

import com.weir.quarkus.base.system.utils.Query;

import jakarta.ws.rs.QueryParam;
import lombok.Data;

@Data
public class DictItemQuery extends BaseSysQuery {
	@QueryParam(value = "id")
    private String id;
	@Query()
	@QueryParam(value = "dictId")
    private String dictId;
	@Query()
	@QueryParam(value = "itemText")
    private String itemText;
	@QueryParam(value = "itemValue")
    private String itemValue;
	@QueryParam(value = "itemColor")
    private String itemColor;
    private String description;
    @QueryParam(value = "sortOrder")
    private Integer sortOrder;
    @Query()
    @QueryParam(value = "status")
    private Integer status;


}
