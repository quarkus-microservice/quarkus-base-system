package com.weir.quarkus.sys.vo;

import java.io.InputStream;

import org.jboss.resteasy.reactive.PartType;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;

import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.core.MediaType;

public class UploadEntity {

//	@RestForm("file")
	@FormParam("file")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    public byte[] file;

//    @FormParam("fileName")
//    @PartType(MediaType.TEXT_PLAIN)
//    public String fileName;
}