package com.weir.quarkus.sys.vo;

import java.util.List;

import lombok.Data;

@Data
public class PageDTO<T> {

	private int pages;
    /**
     * 当前分页总页数
     */
    public int getPages() {
        if (getSize() == 0) {
            return 0;
        }
        int pages = getTotal() / getSize();
        if (getTotal() % getSize() != 0) {
            pages++;
        }
        return pages;
    }

    /**
     * 分页记录列表
     *
     * @return 分页对象记录列表
     */
    private List<T> records;

    /**
     * 当前满足条件总行数
     *
     * @return 总条数
     */
    private int total;

    /**
     * 获取每页显示条数
     *
     * @return 每页显示条数
     */
    private int size;

    /**
     * 当前页
     *
     * @return 当前页
     */
    private int current;
}
