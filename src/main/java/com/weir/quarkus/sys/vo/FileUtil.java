/** 
 * @Title: FileUtil.java 
 * @Package com.weir.quarkus.sys.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月13日 17:37:06 
 * @version V1.0 
 */
package com.weir.quarkus.sys.vo;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.weir.quarkus.sys.resource.SysDictController;

import lombok.extern.slf4j.Slf4j;

/** 
 * @Title: FileUtil.java 
 * @Package com.weir.quarkus.sys.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月13日 17:37:06 
 * @version V1.0 
 */
@Slf4j
public class FileUtil {

	public static File byte2File(byte[] buf, String filePath, String fileName) {
		BufferedOutputStream bos = null;
	    FileOutputStream fos = null;
	    File file = null;
	    try {
	        File dir = new File(filePath);
	        if (!dir.exists() && dir.isDirectory()) {
	            dir.mkdirs();
	        }
	        file = new File(filePath + File.separator + fileName);
	        fos = new FileOutputStream(file);
	        bos = new BufferedOutputStream(fos);
	        bos.write(buf);
	    } catch (Exception e) {
		    log.error("byte2File failed: ", e);
	    } finally {
	        if (bos != null) {
	            try {
	                bos.close();
	            } catch (IOException e) {
	                log.error("byte2File close bos failed: ", e);
	            }
	        }
	        if (fos != null) {
	            try {
	                fos.close();
	            } catch (IOException e) {
	                log.error("byte2File close fos failed: ", e);
	            }
	        }
	    }
	    return file;
	}
}
