/** 
 * @Title: UserInfo.java 
 * @Package com.weir.quarkus.sys.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月5日 15:34:47 
 * @version V1.0 
 */
package com.weir.quarkus.sys.vo;

import java.util.List;
import java.util.Map;

import com.weir.quarkus.sys.entity.SysDepart;
import com.weir.quarkus.sys.entity.SysUser;

import lombok.Data;

/** 
 * @Title: UserInfo.java 
 * @Package com.weir.quarkus.sys.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月5日 15:34:47 
 * @version V1.0 
 */
@Data
public class UserInfo {

	private String token;
	private SysUser userInfo;
	private List<SysDepart> departs ;
	private Integer multiDepart;
	private  Map<String,List<DictModel>> sysAllDictItems;
}
