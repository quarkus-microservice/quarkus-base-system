package com.weir.quarkus.sys.vo;

import java.io.Serializable;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import jakarta.ws.rs.QueryParam;
import lombok.Data;

/**
 * @Title: DuplicateCheckVo
 * @Description: 重复校验VO
 * @Author 张代浩
 * @Date 2019-03-25
 * @Version V1.0
 */
@Data
@Schema(description="重复校验数据模型")
public class DuplicateCheckVo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 表名
	 */
	@QueryParam(value = "tableName")
	@Schema(description="表名",name="tableName",example="sys_log")
	private String tableName;
	
	/**
	 * 字段名
	 */
	@QueryParam(value = "fieldName")
	@Schema(description="字段名",name="fieldName",example="id")
	private String fieldName;
	
	/**
	 * 字段值
	 */
	@QueryParam(value = "fieldVal")
	@Schema(description="字段值",name="fieldVal",example="1000")
	private String fieldVal;
	
	/**
	 * 数据ID
	*/
	@QueryParam(value = "dataId")
	@Schema(description="数据ID",name="dataId",example="2000")
	private String dataId;

}