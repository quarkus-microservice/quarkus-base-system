/** 
 * @Title: MenuDTO.java 
 * @Package com.weir.quarkus.sys.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月8日 16:57:41 
 * @version V1.0 
 */
package com.weir.quarkus.sys.vo;

import java.util.List;

import lombok.Data;

/** 
 * @Title: MenuDTO.java 
 * @Package com.weir.quarkus.sys.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月8日 16:57:41 
 * @version V1.0 
 */
@Data
public class MenuDTO {

	private String id;
	private String route;
	private String path;
	private String name;
	private String component;
	private String redirect;
	private Boolean hidden;
	private Boolean alwaysShow;
	private MetaDTO meta;
	private List<MenuDTO> children;
	
}
