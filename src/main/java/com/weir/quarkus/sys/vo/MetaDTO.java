/** 
 * @Title: MetaDTO.java 
 * @Package com.weir.quarkus.sys.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月8日 17:00:40 
 * @version V1.0 
 */
package com.weir.quarkus.sys.vo;

import java.util.List;

import lombok.Data;

/** 
 * @Title: MetaDTO.java 
 * @Package com.weir.quarkus.sys.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月8日 17:00:40 
 * @version V1.0 
 */
@Data
public class MetaDTO {
	private	Boolean hideMenu;
	private Boolean keepAlive;
	private Boolean internalOrExternal;
	private String title;
	private String componentName;
	private String icon;
	private String url;
	private Boolean hideTab;
	private List<MenuDTO> permissionList;
}
