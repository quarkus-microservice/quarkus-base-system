package com.weir.quarkus.sys.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysPermissionDTO {
    private String id;
    private String parentId;
    private String name;
    private String url;
    private String component;
    private Boolean isRoute;
    private String componentName;
    private String redirect;
    private Integer menuType;
    private String perms;
    private String permsType;
    private Integer sortNo;
    private Boolean alwaysShow;
    private String icon;
    private Boolean isLeaf;
    private Boolean keepAlive;
    private Boolean hidden;
    private Boolean hideTab;
    private String description;
    private Boolean delFlag;
    private Integer ruleFlag;
    private String status;
    private Boolean internalOrExternal;

}
