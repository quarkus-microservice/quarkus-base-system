package com.weir.quarkus.base.system.vo;
import java.util.List;
import java.util.Set;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.weir.quarkus.base.aspect.DictData;

/**
 * 
 * @Title: SysUserDTO.java 
 * @Package com.weir.quarkus.base.system.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年1月30日 13:28:17 
 * @version V1.0
 */
@Data
public class SysUserDTO {
	private Integer id;
	
	private String email;
	
	private String userName;
	
	@DictData(code = "sex")
	private String sex;
//	private String userPwd;
	@DictData(code = "id",table = "sys_depart",text = "name")
	private Integer departId;
	
	private Boolean enable;
	
	private Set<String> moduleCodes;
	private String token;
	private List<Integer> roleList;

}