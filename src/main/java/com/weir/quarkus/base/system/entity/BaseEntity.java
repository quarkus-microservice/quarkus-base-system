package com.weir.quarkus.base.system.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.weir.quarkus.base.system.handler.BaseEntityListener;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SoftDelete;
import org.hibernate.annotations.Where;

/**
 * 
 * @ClassName: BaseEntity
 * @Description: 实体公共基类（表的创建时间 创建人 修改时间 修改人 分页的当前页 每页数据）
 * @author weir
 * @date 2021年8月25日
 *
 */
@Getter
@Setter
//@Builder
@MappedSuperclass
@EntityListeners(BaseEntityListener.class)
//@SoftDelete(columnName = "delFlag") //目前有问题

public class BaseEntity extends PanacheEntityBase {

	@Transient
	private Integer page = 0;
	@Transient
	private Integer rows = 10;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "create_time")
	private LocalDateTime createTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "update_time")
	private LocalDateTime updateTime;

	@Column(name = "create_by")
	private String createBy;
	@Column(name = "update_by")
	private String updateBy;
	
	@Basic
    @Column(name = "del_flag")
    private Boolean delFlag = false;

//	private String remark;

}