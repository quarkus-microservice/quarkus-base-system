package com.weir.quarkus.base.system.vo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.QueryParam;

@Data
public class BaseQuery {

	@QueryParam(value = "page")
	@DefaultValue(value = "1")
	private Integer page = 1;
	@QueryParam(value = "pageSize")
	@DefaultValue(value = "10")
	private Integer pageSize = 10;

	@QueryParam(value = "createTime")
	private Date createTime;

	@QueryParam(value = "creator")
	private Integer creator;

	@QueryParam(value = "modifyTime")
	private Date modifyTime;

	@QueryParam(value = "modifier")
	private Integer modifier;

	@QueryParam(value = "remark")
	private String remark;
	
	@QueryParam(value = "column")
	private String column;
	@QueryParam(value = "order")
	private String order;
	
	@QueryParam(value = "sort")
	private List<String> sort;
}
