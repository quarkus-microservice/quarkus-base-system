package com.weir.quarkus.base.system.entity;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.annotations.Array;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

@Getter
@Setter
//@Builder
@Entity
public class MumberUser extends BaseWeirEntity {

	/**自定义id生产*/
	@Id
	@GenericGenerator(name = "sequence_id", strategy = "com.weir.quarkus.base.system.entity.MyIdGenerator" )
    @GeneratedValue(generator = "sequence_id")
	private Long id;
	private String name;
	
}
