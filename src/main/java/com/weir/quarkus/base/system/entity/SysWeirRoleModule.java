package com.weir.quarkus.base.system.entity;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @ClassName: SysRoleModule
 * @Description: 后台角色菜单中间实体
 * @author weir
 * @date 2021年8月25日
 *
 */
@Getter
@Setter
//@Builder
@Entity
@Table(name = "sys_weir_role_module")
public class SysWeirRoleModule extends BaseWeirEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "role_id")
	private Integer roleId;
	@Column(name = "module_id")
	private Integer moduleId;
	
//	public SysWeirRoleModule(Integer roleId, Integer moduleId) {
//		this.roleId = roleId;
//		this.moduleId = moduleId;
//	}
//	public SysWeirRoleModule() {
//	}
//	public SysWeirRoleModule(Integer id, Integer roleId, Integer moduleId) {
//		this.id = id;
//		this.roleId = roleId;
//		this.moduleId = moduleId;
//	}

}