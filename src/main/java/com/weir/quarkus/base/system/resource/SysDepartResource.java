//package com.weir.quarkus.base.system.resource;
//
//import com.weir.quarkus.base.system.entity.SysWeirDepart;
//import com.weir.quarkus.base.system.mapper.SysDepartMapper;
//import com.weir.quarkus.base.system.vo.SysDepartVo;
//
//import org.eclipse.microprofile.openapi.annotations.tags.Tag;
//
//import java.util.List;
//
//import jakarta.enterprise.context.ApplicationScoped;
//import jakarta.inject.Inject;
//import jakarta.transaction.Transactional;
//import jakarta.ws.rs.*;
//import jakarta.ws.rs.core.Response;
//
//@Tag(name = "部门管理")
//@Path("/depart")
//@ApplicationScoped
//public class SysDepartResource {
//	
//	@Inject
//	SysDepartMapper sysDepartMapper;
//	
//	@GET
//	@Path("/{id}")
//	public SysDepartVo get(@PathParam("id") Integer id) {
//		SysWeirDepart depart = SysWeirDepart.findById(id);
//		return sysDepartMapper.toDTO(depart);
//	}
//	@GET
//	@Path("list-all")
//	public List<SysWeirDepart> listAll() {
//		return SysWeirDepart.listAll();
//	}
//
//	@POST
//	@Transactional
//	public SysWeirDepart add(SysWeirDepart depart) {
//		depart.persist();
//		return depart;
//	}
//	@PUT
//	@Transactional
//	public Response edit(SysDepartVo departVo) {
//		SysWeirDepart depart = SysWeirDepart.findById(departVo.getId());
//		sysDepartMapper.merge(depart, departVo);
//		
//		return Response.ok(sysDepartMapper.toDTO(depart)).build();
//	}
//
//}
