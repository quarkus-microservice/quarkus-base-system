package com.weir.quarkus.base.system.redis;

import io.quarkus.redis.datasource.ReactiveRedisDataSource;
import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.keys.KeyCommands;
import io.quarkus.redis.datasource.keys.ReactiveKeyCommands;
import io.quarkus.redis.datasource.value.SetArgs;
import io.quarkus.redis.datasource.value.ValueCommands;
import io.smallrye.mutiny.Uni;
import jakarta.inject.Singleton;

/** 
 * @Title: ValueCommandsService.java 
 * @Package com.weir.quarkus.base.system.redis 
 * @Description: redis工具
 * @author weir
 * @date 2024年2月1日 10:25:53 
 * @version V1.0 
 */
@Singleton
public class ValueCommandsService {

	private ReactiveKeyCommands<String> keys;
    private KeyCommands<String> keyCommands;
    private ValueCommands<String, String> valueCommands;
    
    JsonJacksonCodec codec = JsonJacksonCodec.INSTANCE;
    
    public ValueCommandsService(RedisDataSource redisDS,  ReactiveRedisDataSource reactiveRedisDS) {
        keys = reactiveRedisDS.key();
        valueCommands = redisDS.value(String.class);
        keyCommands = redisDS.key(String.class);
    }
    
    public Uni<Void> del(String key) {
        return keys.del(key)
            .replaceWithVoid();
    }

    public <T> T get(String key) {
    	String s = valueCommands.get(key);
    	if (s == null) {
			return null;
		}else {			
			return (T) codec.decoder(s);
		}
    }
    /**
	 * 普通缓存获取
	 * 
	 * @param key 键
	 * @return 值
	 */
//	public Object get(String key) {
//		return key == null ? null : redisTemplate.opsForValue().get(key);
//	}

    public <T> void set(String key, T value, long time) {
    	if (time > 0) {
    		valueCommands.set(key, codec.encoder(value), new SetArgs().ex(time));			
		}
    }
    public <T> void set(String key, T value) {
    	valueCommands.set(key, codec.encoder(value));
    }
    /**
	 * 普通缓存放入
	 * 
	 * @param key   键
	 * @param value 值
	 * @return true成功 false失败
	 */
//	public boolean set(String key, Object value) {
//		try {
//			redisTemplate.opsForValue().set(key, value);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
	
    /**
	 * 指定缓存失效时间
	 * 
	 * @param key  键
	 * @param time 时间(秒)
	 * @return
	 */
	public boolean expire(String key, long time) {
		try {
			if (time > 0) {
//				redisTemplate.expire(key, time, TimeUnit.SECONDS);
				keyCommands.pexpire(key, time);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	/**
	 * 根据key 获取过期时间
	 * 
	 * @param key 键 不能为null
	 * @return 时间(秒) 返回0代表为永久有效
	 */
	public long getExpire(String key) {
//		redisTemplate.getExpire(key, TimeUnit.SECONDS);
		return keyCommands.pttl(key);
	}
	
	/**
	 * 判断key是否存在
	 * 
	 * @param key 键
	 * @return true 存在 false不存在
	 */
	public boolean hasKey(String key) {
		try {
//			return redisTemplate.hasKey(key);
			return keyCommands.exists(key);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 删除缓存
	 * 
	 * @param key 可以传一个值 或多个
	 */
	public void del(String... key) {
		if (key != null && key.length > 0) {
//			if (key.length == 1) {
//				redisTemplate.delete(key[0]);
//			} else {
				//springboot2.4后用法
//				redisTemplate.delete(Arrays.asList(key));
//			}
			keyCommands.del(key);
		}
	}
	
	
}
