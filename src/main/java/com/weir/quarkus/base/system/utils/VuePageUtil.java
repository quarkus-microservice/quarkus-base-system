package com.weir.quarkus.base.system.utils;

import jakarta.persistence.TypedQuery;

import java.util.List;

import com.weir.quarkus.base.system.vo.ElementPageVo;
import com.weir.quarkus.sys.vo.VuePageListVo;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;

/**
 * vue分页数据计算组装
 * @author weir
 *
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class VuePageUtil {
	
	public static VuePageListVo pageDTO(List dtos,Long count,int page,int pageSize) {
		VuePageListVo pageList = new VuePageListVo<>();
		pageList.setList(dtos);
		pageList.setPage(page);
		pageList.setPageCount(count % pageSize == 0 ? count / pageSize : count / pageSize + 1);
		pageList.setPageSize(pageSize);
		return pageList;
	}

	public static VuePageListVo toPage(PanacheQuery<PanacheEntityBase> panacheQuery, VuePageListVo list, Integer page,
			Integer pageSize) {
		long count = panacheQuery.count();
		list.setPage(page);
		list.setPageSize(pageSize);
		list.setList(panacheQuery.list());
		list.setPageCount(count % pageSize == 0 ? count / pageSize : count / pageSize + 1);
		
		ElementPageVo pageVo = new ElementPageVo();
		pageVo.setPage(page);
		pageVo.setPageSize(pageSize);
		pageVo.setTotal(count);
//		list.setPageEl(pageVo);
		return list;
	}
	public static VuePageListVo toPageForTypedQuery(TypedQuery typedQuery, Integer page,
			Integer pageSize) {
		VuePageListVo list = new VuePageListVo<>();
		Long count = Integer.valueOf(typedQuery.getResultList().size()).longValue();
		list.setPage(page);
		list.setPageSize(pageSize);
		List resultList = typedQuery.setFirstResult(page * pageSize).setMaxResults(pageSize).getResultList();
		list.setList(resultList);
		list.setPageCount(count % pageSize == 0 ? count / pageSize : count / pageSize + 1);
		
		ElementPageVo pageVo = new ElementPageVo();
		pageVo.setPage(page);
		pageVo.setPageSize(pageSize);
		pageVo.setTotal(count);
//		list.setPageEl(pageVo);
		return list;
	}
	public static VuePageListVo toListForTypedQuery(TypedQuery typedQuery, VuePageListVo list) {
		list.setList(typedQuery.getResultList());
		return list;
	}
}
