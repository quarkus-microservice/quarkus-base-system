package com.weir.quarkus.base.system.vo;

import lombok.Data;

@Data
public class UserVo {
	
	private Integer id;

	private String userIds;
	private String roleIds;
}
