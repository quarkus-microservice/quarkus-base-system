/** 
 * @Title: PageData.java 
 * @Package com.weir.quarkus.base.system.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年1月30日 16:21:10 
 * @version V1.0 
 */
package com.weir.quarkus.base.system.vo;

import java.util.List;

import lombok.Data;

/** 
 * @Title: PageData.java 
 * @Package com.weir.quarkus.base.system.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年1月30日 16:21:10 
 * @version V1.0 
 */
@Data
public class PageData<T> {

	private long count;
	private List<T> list;
	/**
	 * @param count
	 * @param list
	 */
	public PageData(long count, List<T> list) {
		super();
		this.count = count;
		this.list = list;
	}
	/**
	 * 
	 */
	public PageData() {
		super();
	}
}
