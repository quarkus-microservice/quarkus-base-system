package com.weir.quarkus.base.system.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants.ComponentModel;
import org.mapstruct.NullValuePropertyMappingStrategy;

import com.weir.quarkus.base.system.entity.SysWeirUser;
import com.weir.quarkus.base.system.vo.SysUserDTO;

@Mapper(componentModel = ComponentModel.JAKARTA, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SysUserMapper {

	SysUserDTO toDTO(SysWeirUser user);
	List<SysUserDTO> toDTO(List<SysWeirUser> users);
}
