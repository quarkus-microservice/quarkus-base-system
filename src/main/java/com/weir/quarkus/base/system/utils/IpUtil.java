package com.weir.quarkus.base.system.utils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 
 * @ClassName: IpUtil
 * @Description: IP 工具类
 * @author weir
 * @date 2021年8月25日
 *
 */
public class IpUtil {

	private IpUtil() {
	}

	private static String getIp(HttpServletRequest request) {
		List<String> keyList = Arrays.asList("X-Client-IP", "X-Real-IP", "X-Real-Ip", "WL-Proxy-Client-IP",
				"PROXY_CLIENT_IP", "X_Forwarded_For");

		for (String key : keyList) {
			String ip = request.getHeader(key);
			// 是合法的 IP，直接返回
			if (StringUtils.isNotBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
				return ip;
			}
		}

		return request.getLocalAddr();
	}

	public static String getSingleIp(HttpServletRequest request) {
		String ip = getIp(request);

		if (ip.contains(",")) {
			return ip.split(",")[0];
		}

		return ip;
	}

	public static Map<String, String> getAllIpInfo(HttpServletRequest request) {
		Map<String, String> map = new HashMap<>();
		map.put("X-Client-IP", request.getHeader("X-Client-IP"));
		map.put("X-Real-IP", request.getHeader("X-Real-IP"));
		map.put("X-Real-Ip", request.getHeader("X-Real-Ip"));
		map.put("WL-Proxy-Client-IP", request.getHeader("WL-Proxy-Client-IP"));
		map.put("PROXY_CLIENT_IP", request.getHeader("PROXY_CLIENT_IP"));
		map.put("X_Forwarded_For", request.getHeader("X_Forwarded_For"));
		map.put("RemoteAddress", request.getRemoteHost());
		return map;
	}

	public static InetAddress getAddress() {
		InetAddress result = null;
		try {
			int lowest = Integer.MAX_VALUE;
			for (Enumeration<NetworkInterface> nics = NetworkInterface.getNetworkInterfaces(); nics
					.hasMoreElements();) {
				NetworkInterface ifc = nics.nextElement();
				if (ifc.isUp()) {
//					this.log.trace("Testing interface: " + ifc.getDisplayName());
					if (ifc.getIndex() < lowest || result == null) {
						lowest = ifc.getIndex();
					} else if (result != null) {
						continue;
					}
				}
			}
		} catch (IOException ex) {
		} finally {
			if (result != null) {
				return result;
			}
			try {
				return InetAddress.getLocalHost();
			} catch (UnknownHostException e) {
			}
		}
		return null;
	}
}