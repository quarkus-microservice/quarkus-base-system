package com.weir.quarkus.base.system.vo;

import lombok.Data;

@Data
public class ElementPageVo {

	private Integer page;
	private Integer pageSize;
	private Long total;
}
