package com.weir.quarkus.base.system.entity;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.Array;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.ParamDef;
import org.hibernate.type.SqlTypes;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.weir.quarkus.base.aspect.DictData;

/**
 * 
 * @ClassName: SysUser
 * @Description: 后台用户实体
 * @author weir
 * @date 2021年8月25日
 *
 */
//@FilterDef(name = "orderOwnerFilter", parameters = {@ParamDef(name= "departIds",type = "integer")})
//@Filter(name= "orderOwnerFiler", condition = "departId in (:departIds)")
@Getter
@Setter
//@Builder
//@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "sys_weir_user")
public class SysWeirUser extends BaseWeirEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	// 唯一性
//	@DictData(code = "email",table = "temail")
	@Column(unique = true)
	@NotBlank
	private String email;
	
//	@DictData(code = "user_name",table = "sys_user",text = "email")
	@Column(unique = true, name = "user_name")
	@NotBlank
	private String userName;
	
	// 密码加密保存和解密展示
//	@DictData(code = "sex")
	private String sex;
//	@Convert(converter = EncryptConverter.class)
	@Column(name = "user_pwd")
	private String userPwd;
	@Column(name = "depart_id")
	private Integer departId;
//	@ManyToOne
//	@JoinColumn(name="depart_id")
//	private Integer departId;
	
	private boolean enable = true;
	
//	@JdbcTypeCode(SqlTypes.JSON)
//	@Array(length = 5)
//	public String[] shuzu;
//	@ElementCollection
//	public List<String> phoneNumbers;
	
	@Transient
	private Set<String> moduleCodes;
	@Transient
	private String token;
	@Transient
	private List<Integer> roleList;

}