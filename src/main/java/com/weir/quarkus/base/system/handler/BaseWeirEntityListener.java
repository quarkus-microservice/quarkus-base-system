package com.weir.quarkus.base.system.handler;

import com.weir.quarkus.base.system.entity.BaseWeirEntity;
import com.weir.quarkus.base.system.entity.SysWeirUser;

import io.vertx.core.json.JsonObject;

import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;

import java.time.LocalDateTime;

import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;

@RequestScoped
public class BaseWeirEntityListener {

	public BaseWeirEntityListener() {}

	public static SysWeirUser getUser(String userJson) {
		return new JsonObject(new JsonObject(userJson).getValue("user").toString()).mapTo(SysWeirUser.class);
	}

	@PrePersist
    public void prePersist(Object target) {
		JsonWebToken context = CDI.current().select(JsonWebToken.class).get();
		Object claim = context.getClaim(Claims.preferred_username.name());
//		System.out.println("----------------BaseEntityListener-------prePersist---------------" + claim);
		if (claim == null) {
			return;
		}
		SysWeirUser user = getUser(claim.toString());
		if (target instanceof BaseWeirEntity) {
			BaseWeirEntity baseEntity = (BaseWeirEntity) target;
			baseEntity.setCreator(user.getId());
			baseEntity.setCreateTime(LocalDateTime.now());
		}
	}
	@PreUpdate
    public void preUpdate(Object target) {
		JsonWebToken context = CDI.current().select(JsonWebToken.class).get();
		Object claim = context.getClaim(Claims.preferred_username.name());
		if (claim == null) {
			return;
		}
		SysWeirUser user = getUser(claim.toString());
		if (target instanceof BaseWeirEntity) {
			BaseWeirEntity baseEntity = (BaseWeirEntity) target;
			baseEntity.setModifier(user.getId());
			baseEntity.setModifyTime(LocalDateTime.now());
		}
//		System.out.println("----------------BaseEntityListener-------preUpdate---------------");
	}
}
