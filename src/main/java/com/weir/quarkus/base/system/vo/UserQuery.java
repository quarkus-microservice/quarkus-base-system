package com.weir.quarkus.base.system.vo;

import com.weir.quarkus.base.system.utils.Query;

import jakarta.ws.rs.QueryParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class UserQuery extends BaseQuery {

	@QueryParam(value = "id")
	private Integer id;
	
	@QueryParam(value = "email")
	@Query()
	private String email;
	@QueryParam(value = "userName")
	@Query(blurry = "email,userName")
	private String userName;
	
	
	@QueryParam(value = "userPwd")
	private String userPwd;
	@QueryParam(value = "enable")
	private Boolean enable;
	
	@QueryParam(value = "departId")
	@Query(propName = "id", joinName = "sys_depart")
	private Integer departId;
}
