package com.weir.quarkus.base.system.resource;

import com.alibaba.excel.EasyExcel;
import com.weir.quarkus.base.system.entity.SysArea;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import java.io.File;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Tag(name = "easy-excel测试")
@Path("/easy-excel")
@ApplicationScoped
public class EasyExcelResource {

	@GET
	@Path("/out")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response outExcel() throws Exception {
		long s = System.currentTimeMillis();
		List<SysArea> list = SysArea.findAll().list();
		String pathFile = "/Users/weir/Downloads/logs/";
		String fileName = "区划" + s + ".xlsx";
		EasyExcel.write(pathFile + fileName, SysArea.class).sheet("模板").doWrite(list);
		File file =  new File(pathFile + fileName);
		long s2 = System.currentTimeMillis();
		System.out.println("------------------ok-------------" + (s2 - s)/1000);
		// 对输出的文件名进行编码，防止下载的中文文件名乱码
		String encodFileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8);
		return Response.ok(file).header("content-disposition", "attachment; filename=\"" + encodFileName + "\"")
				.header("Content-Length", file.length()).build();
	}

}
