package com.weir.quarkus.base.system.vo;

/**
 * 
 * @ClassName: ResultVo
 * @Description: 返回数据格式
 * @author weir
 * @date 2021年8月28日
 *
 * @param <T>
 */
public class ResultVo<T> {

	public Integer code;
	public String message;
	public T data;
	public ResultVo() {
	}
	public ResultVo(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	public ResultVo(Integer code, T data) {
		this.code = code;
		this.data = data;
	}
	public ResultVo(Integer code, String message, T data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}
}
