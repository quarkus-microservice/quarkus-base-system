package com.weir.quarkus.base.system.entity;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @ClassName: SysUserRole
 * @Description: 后台用户角色中间实体
 * @author weir
 * @date 2021年8月25日
 *
 */
@Getter
@Setter
//@Builder
@Entity
@Table(name = "sys_weir_user_role")
public class SysWeirUserRole extends BaseWeirEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "user_id")
	private Integer userId;
	@Column(name = "role_id")
	private Integer roleId;
//	public Integer getRoleId() {
//		return roleId;
//	}
//	public SysWeirUserRole(Integer userId, Integer roleId) {
//		super();
//		this.userId = userId;
//		this.roleId = roleId;
//	}
//	public SysWeirUserRole() {
//		super();
//	}
	

}