package com.weir.quarkus.base.system.vo;

import java.util.List;

import lombok.Data;

@Data
public class VuePageListVo<T> {

	private List<T> list;
	private Integer page = 1;
	private Integer pageSize = 10;
	private Long pageCount = 0L;
	
	private ElementPageVo pageEl;
}
