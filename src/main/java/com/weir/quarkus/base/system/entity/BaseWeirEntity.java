package com.weir.quarkus.base.system.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.weir.quarkus.base.system.handler.BaseWeirEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * 
 * @ClassName: BaseEntity
 * @Description: 实体公共基类（表的创建时间 创建人 修改时间 修改人 分页的当前页 每页数据）
 * @author weir
 * @date 2021年8月25日
 *
 */
@Getter
@Setter
//@Builder
//@EqualsAndHashCode(callSuper=false)
@MappedSuperclass
@EntityListeners(BaseWeirEntityListener.class)
public class BaseWeirEntity extends PanacheEntityBase {

	@Transient
	private Integer page = 0;
	@Transient
	private Integer rows = 10;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "create_time")
	private LocalDateTime createTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "modify_time")
	private LocalDateTime modifyTime;

	@Column(name = "creator")
	private Integer creator;
	@Column(name = "modifier")
	private Integer modifier;

	private String remark;

}