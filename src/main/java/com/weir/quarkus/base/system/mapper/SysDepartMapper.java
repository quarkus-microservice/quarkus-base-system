package com.weir.quarkus.base.system.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.MappingConstants.ComponentModel;

import com.weir.quarkus.base.system.entity.SysWeirDepart;
import com.weir.quarkus.base.system.vo.SysDepartVo;

@Mapper(componentModel = ComponentModel.JAKARTA, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
//@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SysDepartMapper {

	SysDepartVo toDTO(SysWeirDepart depart);
	SysWeirDepart toDAO(SysDepartVo departVo);
	
	void merge(@MappingTarget SysWeirDepart target,SysDepartVo departVo);
}
