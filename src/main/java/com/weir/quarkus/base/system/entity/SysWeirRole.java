package com.weir.quarkus.base.system.entity;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @ClassName: SysRole
 * @Description: 后台角色实体
 * @author weir
 * @date 2021年8月25日
 *
 */
@Getter
@Setter
//@Builder
@Entity
@Table(name = "sys_weir_role")
public class SysWeirRole extends BaseWeirEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(unique = true)
	@NotBlank
	private String name;
	
	@Column(unique = true)
	@NotBlank
	private String code;

	@Transient
	private List<Integer> menuKeys;
}