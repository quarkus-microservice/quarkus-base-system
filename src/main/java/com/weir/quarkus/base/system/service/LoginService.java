//package com.weir.quarkus.base.system.service;
//
//import jakarta.enterprise.context.ApplicationScoped;
//import jakarta.transaction.Transactional;
//
//import com.weir.quarkus.base.system.entity.SysWeirModule;
//import com.weir.quarkus.base.system.entity.SysWeirRole;
//import com.weir.quarkus.base.system.entity.SysWeirRoleModule;
//import com.weir.quarkus.base.system.entity.SysWeirUser;
//import com.weir.quarkus.base.system.entity.SysWeirUserRole;
//import com.weir.quarkus.base.system.utils.AesEncyptUtil;
//
//@ApplicationScoped
//public class LoginService {
//
//	public void roles(Integer userId) {
//		
//	}
//	
//	@Transactional
//	public void init() {
//		SysWeirModule module = new SysWeirModule();
//		module.code = "system";
//		module.component = "system";
//		module.name = "系统管理";
//		module.path = "sys";
//		module.stepType = 1;
//		SysWeirModule.persist(module);
//		
//		SysWeirModule module_user = new SysWeirModule();
//		module_user.code = "user";
//		module_user.component = "sys_user";
//		module_user.name = "后台用户管理";
//		module_user.path = "user";
//		module_user.stepType = 1;
//		SysWeirModule.persist(module_user);
//		
//		SysWeirModule module_module = new SysWeirModule();
//		module_module.code = "module";
//		module_module.component = "sys_module";
//		module_module.name = "后台菜单管理";
//		module_module.path = "module";
//		module_module.stepType = 1;
//		SysWeirModule.persist(module_module);
//		
//		SysWeirRole role = new SysWeirRole();
//		role.code = "admin";
//		role.name = "管理员";
//		SysWeirRole.persist(role);
//		
//		SysWeirRoleModule roleModule = new SysWeirRoleModule();
//		roleModule.roleId = role.id;
//		roleModule.moduleId = module.id;
//		SysWeirRoleModule.persist(roleModule);
//		SysWeirRoleModule roleModule2 = new SysWeirRoleModule();
//		roleModule2.roleId = role.id;
//		roleModule2.moduleId = module_user.id;
//		SysWeirRoleModule.persist(roleModule2);
//		SysWeirRoleModule roleModule3 = new SysWeirRoleModule();
//		roleModule3.roleId = role.id;
//		roleModule3.moduleId = module_module.id;
//		SysWeirRoleModule.persist(roleModule3);
//		
//		SysWeirUser user = new SysWeirUser();
//		user.setEmail("22222@qq.com");
//		user.setUserName("weiwei");
//		user.setUserPwd(AesEncyptUtil.encrypt("123456"));
//		SysWeirUser.persist(user);
//		
//		SysWeirUserRole userRole = new SysWeirUserRole();
//		userRole.userId = user.getId();
//		userRole.roleId = role.id;
//		SysWeirUserRole.persist(userRole);
//	}
//}
