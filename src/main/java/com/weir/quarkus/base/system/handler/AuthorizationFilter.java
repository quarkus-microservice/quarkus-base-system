package com.weir.quarkus.base.system.handler;

import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.RoutingContext;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.jwt.JsonWebToken;

import jakarta.annotation.Priority;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;

/**
 *
 * @ClassName: AuthorizationFilter
 * @Description: url权限拦截
 * @author weir
 * @date 2021年8月28日
 *
 */
@ApplicationScoped
@Priority(Priorities.USER + 200)
@Provider
public class AuthorizationFilter implements ContainerRequestFilter {

	@ConfigProperty(name = "authorization.permit-patterns")
	String permitPatterns;
	
    //  RESTEASY003880: Unable to find contextual data of type: io.vertx.core.http.HttpServerRequest
	@Context
	HttpServerRequest request;
	@Inject
	RoutingContext context;
	
	@Inject
    JsonWebToken jwt;
	
	@Context
    ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) {
    	Method method = resourceInfo.getResourceMethod();
    	RolesAllowed methodPermAnnotation = method.getAnnotation(RolesAllowed.class);

//        if(methodPermAnnotation != null && checkAccess(methodPermAnnotation)) {
//            System.out.println("Verified permissions");
//        } else {
//        	containerRequestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());
//        }
	
	
//    	System.out.println("----------containerRequestContext----request------------" + request.remoteAddress().host());
//    	System.out.println("----------containerRequestContext----context------------" + context.request().remoteAddress().host());
    	
//    	String[] splits = StringUtils.split(permitPatterns, ",");
//    	List<String> list = Arrays.asList(splits);
    	String path = containerRequestContext.getUriInfo().getPath();
    	System.out.println("--------------filter-------------------" + path);
//    	if (list.stream().anyMatch(path::contains)) {
//			return;
//		}
//        Map<String, List<String>> headers = containerRequestContext.getHeaders();
//        List<String> token = headers.get("Authorization");
//        if (token == null || token.isEmpty()) {
////        	throw new RuntimeException("No Authorization");
//		}

//        try {
//			SysUser user = TokenUtils.getUser(token.get(0));
//			if (user == null) {
//				throw new RuntimeException("非法用户");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
    }
//    @Override
//    public void filter(ContainerRequestContext containerRequestContext) {
//    	String[] splits = StringUtils.split(permitPatterns, ",");
//    	List<String> list = Arrays.asList(splits);
//    	String path = containerRequestContext.getUriInfo().getPath();
//    	if (list.stream().anyMatch(path::contains)) {
//    		return;
//    	}
//    	Map<String, Cookie> cookies = containerRequestContext.getCookies();
//    	Cookie cookieToken = cookies.get("weir-token");
//    	if (cookieToken == null) {
//    		try {
//    			containerRequestContext.setRequestUri(new URI("admin/index"));
//    		} catch (URISyntaxException e) {
//    			e.printStackTrace();
//    		}
//    	}
//
//    }

    private boolean checkAccess(RolesAllowed perm) {
        boolean verified = false;
        if(perm == null) {
			//If no permission annotation verification failed
            verified = false;
        }
//        else if(jwt.getClaim("userId") == null) {
//			// Don’t support Anonymous users
//            verified = false;
//        }
        else {
            String userId = jwt.getClaim("userId");
            String role = getRolesForUser(userId);
            String[] userPermissions = getPermissionForRole(role);            
            if(Arrays.asList(userPermissions).stream()
               .anyMatch(userPerm -> Arrays.asList(perm.value()).contains(userPerm))) {
                 verified = true;
            }
        }
        return verified;
    }
    private String[] getPermissionForRole(String role) {
        Map<String, String[]> rolePermissionMap = new HashMap<>();
        rolePermissionMap.put("Admin", new String[] {"task:write", "task:read"});
        rolePermissionMap.put("Member", new String[] {"task:read"});
        return rolePermissionMap.get(role);
    }

    // userId -> role mapping
    private String getRolesForUser(String userId) {
        Map<String, String> userMap = new HashMap<>();
        userMap.put("1234", "Admin");
        userMap.put("6789", "Member");
        return userMap.get(userId);
    }

}