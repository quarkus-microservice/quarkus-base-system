package com.weir.quarkus.base.system.entity;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @ClassName: SysModule
 * @Description: 后台菜单模块实体
 * @author weir
 * @date 2021年8月25日
 *
 */
@Getter
@Setter
//@Builder
@Entity
@Table(name = "sys_weir_module")
public class SysWeirModule extends BaseWeirEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(unique = true)
	@NotBlank
	private String name;
	
	@Column(unique = true)
	@NotBlank
	private String code;
	private String url;
	@Column(name = "parent_id")
	private Integer parentId;
	
	/**
     * 级别(1菜单和2方法功能)
     */
	@Column(name = "step_type")
	private Integer stepType = 1;
	
	/**
     * 路由
     */
	private String path;

    /**
     * 组件
     */
	private String component;
	private String icon;
	
	private Boolean enable;

}