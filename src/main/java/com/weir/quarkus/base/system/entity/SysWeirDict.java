package com.weir.quarkus.base.system.entity;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 字典
 * @author weir
 *
 */
@Getter
@Setter
//@Builder
@Entity
@Table(name = "sys_weir_dict")
public class SysWeirDict extends BaseWeirEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(unique = true)
	@NotBlank
	private String name;
	
	@Column(unique = true)
	@NotBlank
	private String code;

}