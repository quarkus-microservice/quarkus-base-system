package com.weir.quarkus.base.system.jwt;

import org.eclipse.microprofile.jwt.Claims;
import org.jboss.logmanager.Logger;
import org.jose4j.jwt.JwtClaims;

import com.weir.quarkus.base.system.utils.TokenUtils;

import jakarta.enterprise.context.RequestScoped;

import java.util.Collection;
import java.util.UUID;

/**
 * https://quarkus.io/guides/security-authorize-web-endpoints-reference
 * @Title: TokenService.java 
 * @Package com.weir.quarkus.base.system.jwt 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年3月23日 10:34:44 
 * @version V1.0
 */
@RequestScoped
public class TokenService {

    public final static Logger LOGGER = Logger.getLogger(TokenService.class.getSimpleName());

    public String generateUserToken(String email, String username, Collection<String> group) {
        return generateToken(email, username, group);
    }

    public String generateToken(String subject, String name, Collection<String> group) {
        try {
            JwtClaims jwtClaims = new JwtClaims();
            jwtClaims.setIssuer("http://www.loveweir.com"); // change to your company
            jwtClaims.setJwtId(UUID.randomUUID().toString());
            jwtClaims.setSubject(subject);
            jwtClaims.setClaim(Claims.upn.name(), subject);
            jwtClaims.setClaim(Claims.preferred_username.name(), name); //add more
            jwtClaims.setClaim(Claims.groups.name(), group);
            jwtClaims.setAudience("using-jwt");
            jwtClaims.setExpirationTimeMinutesInTheFuture(86400); // 分钟(60*24=一天 * 60=60天)


            String token = TokenUtils.generateTokenString(jwtClaims);
            LOGGER.info("TOKEN generated: " + token);
            return token;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}

//@ApplicationScoped
//@Alternative
//@Priority(1)
//public class TestJWTCallerPrincipalFactory extends JWTCallerPrincipalFactory {
//
//  @Inject JsonUtil jsonUtil;
//  @Inject RedisUtil redisUtil;
//
//  @Override
//  public JWTCallerPrincipal parse(String token, JWTAuthContextInfo authContextInfo)
//      throws ParseException {
//    try {
//      String json =
//          new String(Base64.getUrlDecoder().decode(token.split("\\.")[1]), StandardCharsets.UTF_8);
//      ObjectNode jsonNode = jsonUtil.parseObj(json);
//      String groups = redisUtil.get(jsonNode.get("sub").asText());
//      if (groups != null) {
//        jsonNode.putArray("groups").addAll(jsonUtil.parseArray(groups));
//      }
//      return new DefaultJWTCallerPrincipal(JwtClaims.parse(jsonNode.toString()));
//    } catch (InvalidJwtException ex) {
//      throw new ParseException(ex.getMessage());
//    }
//  }
//}

