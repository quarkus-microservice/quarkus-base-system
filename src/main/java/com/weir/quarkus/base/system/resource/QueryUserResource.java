//package com.weir.quarkus.base.system.resource;
//
//import com.weir.quarkus.base.aspect.Dict;
//import com.weir.quarkus.base.system.entity.SysWeirUser;
//import com.weir.quarkus.base.system.mapper.SysUserMapper;
//import com.weir.quarkus.base.system.utils.QueryHelper;
//import com.weir.quarkus.base.system.utils.VuePageUtil;
//import com.weir.quarkus.base.system.vo.PageData;
//import com.weir.quarkus.base.system.vo.ResultDataVo;
//import com.weir.quarkus.base.system.vo.SysUserDTO;
//import com.weir.quarkus.base.system.vo.UserQuery;
//import com.weir.quarkus.base.system.vo.VuePageListVo;
//import io.vertx.core.http.HttpServerRequest;
//
//import java.util.List;
//
//import org.eclipse.microprofile.openapi.annotations.tags.Tag;
//
//import jakarta.enterprise.context.ApplicationScoped;
//import jakarta.inject.Inject;
//import jakarta.persistence.EntityManager;
//import jakarta.persistence.TypedQuery;
//import jakarta.ws.rs.*;
//import jakarta.ws.rs.core.Context;
//import jakarta.ws.rs.core.Response;
//
///**
// * 
// * @ClassName: SysUserResource
// * @Description: 后台用户API管理
// * @author weir
// * @date 2021年8月25日
// *
// */
//@Tag(name = "用户查询测试")
//@Path("/query")
//@ApplicationScoped
//public class QueryUserResource {
//
//	@Context
//	HttpServerRequest request;
//	@Inject
//	EntityManager em;
//	@Inject
//	SysUserMapper sysUserMapper;
//
//	@GET
//	@Path("user/list")
//	@Dict
//	public Response querylist(@BeanParam UserQuery query) {
//		PageData<SysWeirUser> pageData = QueryHelper.createQueryPage(em, SysWeirUser.class, query,query.getPage()-1,query.getPageSize());
//		List<SysUserDTO> dtos = sysUserMapper.toDTO(pageData.getList());
//		VuePageListVo pageDTO = VuePageUtil.pageDTO(dtos, pageData.getCount(), query.getPage(),query.getPageSize());
//		
////		VuePageListVo<SysUser> queryPage = QueryHelper.createQueryPage(em, SysUser.class, query);
////		List<SysUser> list = queryPage.getList();
////		List<SysUserDTO> dtos = sysUserMapper.toDTOs(list);
////		VuePageListVo dtoList = VuePageUtil.toDTO(queryPage, dtos);
//		
//		return Response.ok().entity(new ResultDataVo<>(200, "ok", pageDTO)).build();
//	}
//
//}
