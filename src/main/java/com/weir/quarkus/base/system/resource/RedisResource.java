package com.weir.quarkus.base.system.resource;

import com.weir.quarkus.base.aspect.Dict;
import com.weir.quarkus.base.aspect.DictVo;
import com.weir.quarkus.base.system.entity.SysWeirUser;
import com.weir.quarkus.base.system.mapper.SysUserMapper;
import com.weir.quarkus.base.system.redis.ValueCommandsService;
import com.weir.quarkus.base.system.utils.QueryHelper;
import com.weir.quarkus.base.system.utils.VuePageUtil;
import com.weir.quarkus.base.system.vo.PageData;
import com.weir.quarkus.base.system.vo.ResultDataVo;
import com.weir.quarkus.base.system.vo.SysUserDTO;
import com.weir.quarkus.base.system.vo.UserQuery;
import com.weir.quarkus.base.system.vo.VuePageListVo;

import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpServerRequest;

import java.util.List;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;

/**
 * 
 * @Title: RedisResource.java 
 * @Package com.weir.quarkus.base.system.resource 
 * @Description: redis测试
 * @author weir
 * @date 2024年2月1日 10:33:09 
 * @version V1.0
 */
@Tag(name = "redis测试")
@Path("/redis")
@ApplicationScoped
public class RedisResource {

	@Inject
	ValueCommandsService valueCommandsService;

	@GET
	@Path("set")
	public Response set() {
		DictVo d = new DictVo("weir", "weiwei");
		
		valueCommandsService.expire("weir", 100);
		valueCommandsService.set("weir", d);
		long expire = valueCommandsService.getExpire("weir");
		System.out.println("----------------expire------"+expire);
		boolean haskey = valueCommandsService.hasKey("weir");
		System.out.println("----------------haskey------"+haskey);
		return Response.ok().build();
	}
	@GET
	@Path("get")
	public DictVo get() {
		DictVo str = valueCommandsService.get("weir");
		return str;
	}
	@GET
	@Path("del")
	public Response del() {
		Uni<Void> del = valueCommandsService.del("weir");
		return Response.ok().build();
	}

}
