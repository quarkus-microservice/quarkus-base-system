package com.weir.quarkus.base.system.handler;

import com.weir.quarkus.base.system.entity.BaseEntity;
import com.weir.quarkus.sys.entity.BaseSysEntity;
import com.weir.quarkus.sys.entity.SysUser;

import io.vertx.core.json.JsonObject;

import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;

import java.time.LocalDateTime;

import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;

@RequestScoped
public class BaseEntityListener {

	public BaseEntityListener() {}

	public static SysUser getUser(String userJson) {
		return new JsonObject(new JsonObject(userJson).getValue("user").toString()).mapTo(SysUser.class);
	}

	@PrePersist
    public void prePersist(Object target) {
		JsonWebToken context = CDI.current().select(JsonWebToken.class).get();
		Object claim = context.getClaim(Claims.preferred_username.name());
//		System.out.println("----------------BaseEntityListener-------prePersist---------------" + claim);
		if (claim == null) {
			return;
		}
		SysUser user = getUser(claim.toString());
		if (target instanceof BaseEntity) {
			BaseEntity baseEntity = (BaseEntity) target;
			baseEntity.setCreateBy(user.getUsername());
			baseEntity.setCreateTime(LocalDateTime.now());
		}
		if (target instanceof BaseSysEntity) {
			BaseSysEntity baseEntity = (BaseSysEntity) target;
			baseEntity.setCreateBy(user.getUsername());
			baseEntity.setCreateTime(LocalDateTime.now());
		}
	}
	@PreUpdate
    public void preUpdate(Object target) {
		JsonWebToken context = CDI.current().select(JsonWebToken.class).get();
		Object claim = context.getClaim(Claims.preferred_username.name());
		if (claim == null) {
			return;
		}
		SysUser user = getUser(claim.toString());
		if (target instanceof BaseEntity) {
			BaseEntity baseEntity = (BaseEntity) target;
			baseEntity.setUpdateBy(user.getUsername());
			baseEntity.setUpdateTime(LocalDateTime.now());
		}
		if (target instanceof BaseSysEntity) {
			BaseSysEntity baseEntity = (BaseSysEntity) target;
			baseEntity.setUpdateBy(user.getUsername());
			baseEntity.setUpdateTime(LocalDateTime.now());
		}
//		System.out.println("----------------BaseEntityListener-------preUpdate---------------");
	}
}
