/** 
 * @Title: DictTupleTransformer.java 
 * @Package com.weir.quarkus.base.aspect 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年1月29日 17:35:10 
 * @version V1.0 
 */
package com.weir.quarkus.base.aspect;

import org.hibernate.query.TupleTransformer;

/** 
 * @Title: DictTupleTransformer.java 
 * @Package com.weir.quarkus.base.aspect 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年1月29日 17:35:10 
 * @version V1.0 
 */
public class DictTupleTransformer implements TupleTransformer<DictVo> {

	@Override
	public DictVo transformTuple(Object[] tuple, String[] aliases) {
		DictVo d = new DictVo(tuple[0].toString(), (String) tuple[1]);
		return d;
	}

}
