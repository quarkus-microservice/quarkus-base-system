package com.weir.quarkus.base.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.weir.quarkus.sys.vo.CommonConstant;

import jakarta.enterprise.util.Nonbinding;
import jakarta.interceptor.InterceptorBinding;

/**
 * 接口调用日志注解
 * @author weir
 *
 */
//@InterceptorBinding //这个就是标识这个注解作为拦截注解
//@Target({ElementType.TYPE,ElementType.METHOD})
//@Retention(RetentionPolicy.RUNTIME)

@InterceptorBinding 
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR}) 
@Inherited
public @interface AutoLog {
	/**
	 * 日志内容
	 * 
	 * @return
	 */
	@Nonbinding
	String value() default "";

	/**
	 * 日志类型
	 * 
	 * @return 0:操作日志;1:登录日志;2:定时任务;
	 */
	int logType() default CommonConstant.LOG_TYPE_2;
	
	/**
	 * 操作日志类型
	 * 
	 * @return （1查询，2添加，3修改，4删除）
	 */
	int operateType() default 0;

	/**
	 * 模块类型 默认为common
	 * @return
	 */
	ModuleType module() default ModuleType.COMMON;
}
