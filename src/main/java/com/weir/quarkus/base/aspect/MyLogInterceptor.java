package com.weir.quarkus.base.aspect;

import java.lang.reflect.Method;

import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weir.quarkus.base.system.handler.BaseEntityListener;
import com.weir.quarkus.sys.entity.SysLog;
import com.weir.quarkus.sys.entity.SysUser;
import com.weir.quarkus.sys.service.SysLogService;
import com.weir.quarkus.sys.vo.CommonConstant;

import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.RoutingContext;
import jakarta.annotation.Priority;
import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;

@Priority(2020)
@Interceptor // 标识这个是拦截器
@AutoLog // 需要匹配的注解
class MyLogInterceptor {

	@Inject
	RoutingContext routingContext;
	@Inject
	JsonWebToken jwt;
	@Inject
	SysLogService sysLogService;
	
	ObjectMapper mapper = new ObjectMapper();

	// 编写环绕通知逻辑
	@AroundInvoke
	Object markInvocation(InvocationContext context) throws Exception {
		long beginTime = System.currentTimeMillis();
		// 执行方法
		Object result = context.proceed();
		// 执行时长(毫秒)
		long time = System.currentTimeMillis() - beginTime;

		// 保存日志
		saveSysLog(context, time, result);
		return result;
	}

	/**
	 * @param context
	 * @param time
	 * @param result
	 */
	private void saveSysLog(InvocationContext context, long time, Object result) {
		Method method = context.getMethod();
		AutoLog autoLog = method.getAnnotation(AutoLog.class);
		SysLog log = new SysLog();
		if (autoLog != null) {
			String content = autoLog.value();
			// 注解上的描述,操作日志内容
			log.setLogType(autoLog.logType());
			log.setLogContent(content);
		}
		// 请求的方法名
		String methodName = method.toString();
		log.setMethod(methodName);
		// 设置操作类型
		if (CommonConstant.LOG_TYPE_2 == log.getLogType()) {
			log.setOperateType(getOperateType(methodName, autoLog.operateType()));
		}
		// 请求的参数
		try {
			log.setRequestParam(mapper.writeValueAsString(context.getParameters()));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
//		log.setRequestParam(JSONObject.toJSONString(context.getParameters()));
		// 设置IP地址
		log.setIp(getRemoteIP(routingContext.request()));

		// 获取登录用户信息
		Object claim = jwt.getClaim(Claims.preferred_username.name());
		if (claim != null) {
			SysUser user = BaseEntityListener.getUser(claim.toString());
			if (user != null) {
				log.setUserid(user.getUsername());
				log.setUsername(user.getRealname());
			}
		}
		// 耗时
		log.setCostTime(time);
		sysLogService.saveLog(log);
	}

	/**
	 * 获取操作类型
	 */
	private int getOperateType(String methodName, int operateType) {
		if (operateType > 0) {
			return operateType;
		}
		return OperateTypeEnum.getTypeByMethodName(methodName);
	}
	
	public static String getRemoteIP(HttpServerRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.remoteAddress().host();
        }
        if (ip != null && ip.indexOf(",") > 0) {
            String[] parts = ip.split(",");
            for (String part : parts) {
                if (!part.isEmpty() && !"unknown".equalsIgnoreCase(part)) {
                    ip = part.trim();
                    break;
                }
            }
        }
        if ("0:0:0:0:0:0:0:1".equals(ip)) {
            ip = "127.0.0.1";
        }
        return ip;
    }
}
