/** 
 * @Title: DictResultListTransformer.java 
 * @Package com.weir.quarkus.base.aspect 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年1月29日 17:41:26 
 * @version V1.0 
 */
package com.weir.quarkus.base.aspect;

import java.util.List;

import org.hibernate.query.ResultListTransformer;

/** 
 * @Title: DictResultListTransformer.java 
 * @Package com.weir.quarkus.base.aspect 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年1月29日 17:41:26 
 * @version V1.0 
 */
public class DictResultListTransformer implements ResultListTransformer<DictVo>{

	@Override
	public List<DictVo> transformList(List<DictVo> resultList) {
		return resultList;
	}

}
