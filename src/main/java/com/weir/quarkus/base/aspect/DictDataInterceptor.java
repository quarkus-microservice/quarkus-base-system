package com.weir.quarkus.base.aspect;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.persistence.EntityManager;
import jakarta.ws.rs.core.Response;

import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.query.sql.internal.NativeQueryImpl;
import org.hibernate.transform.Transformers;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.weir.quarkus.base.system.vo.ResultDataVo;
import com.weir.quarkus.base.system.vo.VuePageListVo;

import io.quarkus.runtime.util.StringUtil;
import lombok.extern.slf4j.Slf4j;

@Interceptor // 标识这个是拦截器
@Dict
@Slf4j
class DictDataInterceptor {

	ObjectMapper mapper = new ObjectMapper();

	// 编写环绕通知逻辑
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@AroundInvoke
	Object markInvocation(InvocationContext context) {
		// context包含调用点信息比如method paramter 之类的
		try {
			List<ObjectNode> items = new ArrayList<>();
			// 对象字段和字段值
			Map<String, String> fieldValueMap = new HashMap<>();
			// 有字典的字段
			List<Field> dictFieldList = new ArrayList<>();
			// 字典map
			Map<String, List<String>> dataListMap = new HashMap<>();

			Object result = context.proceed();
			if (result instanceof Response) {
				if (((Response) result).getEntity() instanceof ResultDataVo) {
					for (Object record : ((VuePageListVo) ((ResultDataVo) ((Response) result).getEntity()).result)
							.getList()) {
						String json = "{}";
						try {
							// 解决@JsonFormat注解解析不了的问题详见SysAnnouncement类的@JsonFormat
							json = mapper.writeValueAsString(record);
						} catch (JsonProcessingException e) {
							log.error("json解析失败" + e.getMessage(), e);
						}
						ObjectNode jsonObject = (ObjectNode) mapper.readTree(json);

						for (Field field : getAllFields(record)) {
							JsonNode jsonNode = jsonObject.get(field.getName());
							String value = null;
							if (!jsonNode.isNull()) {
								value = jsonNode.asText();
								fieldValueMap.put(field.getName(), value);
							} else {
								continue;
							}
							// 找到字典注解
							if (field.getAnnotation(DictData.class) != null) {
								String code = field.getAnnotation(DictData.class).code();
								String text = field.getAnnotation(DictData.class).text();
								String table = field.getAnnotation(DictData.class).table();
								// 只有code有值
								if (!StringUtil.isNullOrEmpty(code)
										&& (StringUtil.isNullOrEmpty(table) && StringUtil.isNullOrEmpty(text))) {
									if (!dictFieldList.contains(field)) {
										dictFieldList.add(field);
									}
									if (!dataListMap.keySet().contains(code) && value != null) {
										dataListMap.put(code, new ArrayList<>(Arrays.asList(value)));
									}
									// code table text 都有值
								} else if (!StringUtil.isNullOrEmpty(code)
										&& (!StringUtil.isNullOrEmpty(table) && !StringUtil.isNullOrEmpty(text))) {
									if (!dictFieldList.contains(field)) {
										dictFieldList.add(field);
									}
									if (value != null) {										
										String dictCode = code;
										if (!StringUtil.isNullOrEmpty(table)) {
											dictCode = String.format("%s,%s,%s", table, text, code);
										}
										if (!dataListMap.keySet().contains(dictCode)) {
											dataListMap.put(dictCode, new ArrayList<>(Arrays.asList(value)));
										} else {
											List<String> list = dataListMap.get(dictCode);
											list.add(value);
											dataListMap.put(dictCode, list);
										}
									}
								}
							}
							// date类型默认转换string格式化日期
							if (field.getType().getName().equals("java.util.Date")
									&& field.getAnnotation(JsonFormat.class) == null
									&& jsonObject.get(field.getName()) != null) {
								jsonObject.put(field.getName(),
										dateToString(new Date(jsonObject.get(field.getName()).asLong()
										)));
							}

						}
						items.add(jsonObject);
					}
				}

			}
			if (!items.isEmpty()) {
				Map<String, List<DictVo>> dictMap = getDict(dataListMap);
				jsonRecord(items, dictMap, dictFieldList);

				((VuePageListVo) ((ResultDataVo) ((Response) result).getEntity()).result).setList(items);
			}

			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void jsonRecord(List<ObjectNode> items, Map<String, List<DictVo>> dictMap, List<Field> dictFieldList) {
		for (ObjectNode record : items) {
			for (Field field : dictFieldList) {
				String code = field.getAnnotation(DictData.class).code();
				String text = field.getAnnotation(DictData.class).text();
				String table = field.getAnnotation(DictData.class).table();

				String fieldDictCode = code;
				if (!StringUtil.isNullOrEmpty(table)) {
					fieldDictCode = String.format("%s,%s,%s", table, text, code);
				}
				JsonNode jsonNode = record.get(field.getName());
				if (jsonNode == null) {
					continue;
				}
				String value = jsonNode.asText();
				List<DictVo> dictVos = dictMap.get(fieldDictCode);
				if (dictVos == null || dictVos.isEmpty()) {
					continue;
				}
				for (DictVo dictVo : dictVos) {
					if (dictVo.getValue().equals(value)) {
						record.put(field.getName() + "_text", dictVo.getText());
					}
				}
			}
		}
	}

	@Inject
	EntityManager em;

	@SuppressWarnings({ "unchecked", "deprecation" })
	private Map<String, List<DictVo>> getDict(Map<String, List<String>> dataListMap) {
		Map<String, List<DictVo>> textListMap = new HashMap<>();
		if (dataListMap == null || dataListMap.isEmpty()) {
			return textListMap;
		}
		for (Entry<String, List<String>> entry : dataListMap.entrySet()) {
			String dictCode = entry.getKey();
			List<String> dataList = entry.getValue();
			// 去组装原生SQL (sys_user,email,user_name)（table,text,code）
			if (dictCode.contains(",")) {
				String[] split = dictCode.split(",");
				String table_name = split[0], text = split[1], code = split[2];
				List<DictVo> resultList = em
						.createNativeQuery("select " + code + " as value," + text + " as text from " + table_name
								+ " where " + code + " in (?1)")
						.setParameter(1, dataList).unwrap(NativeQueryImpl.class)
//						.setResultTransformer(Transformers.aliasToBean(DictVo.class))
						.setTupleTransformer(new DictTupleTransformer())
						.setResultListTransformer(new DictResultListTransformer())
						.getResultList();
				if (!textListMap.keySet().contains(dictCode)) {
					textListMap.put(dictCode, resultList);
				} else {
					List<DictVo> list = textListMap.get(dictCode);
					list.addAll(resultList);
					textListMap.put(dictCode, list);
				}
			} else { // 去数据字典查询 sys_dict里面的code 然后关联查找sys_dict_item
				List<DictVo> resultList = em.createNativeQuery(
						"select item.itemValue as value,item.itemText as text from sys_dict_item item join sys_dict d on item.pId=d.id where d.code=?1",
						Object[].class).setParameter(1, dictCode).unwrap(NativeQueryImpl.class)
//				.setResultTransformer(Transformers.aliasToBean(DictVo.class))
						.setTupleTransformer(new DictTupleTransformer())
						.setResultListTransformer(new DictResultListTransformer()).getResultList();
				if (CollectionUtils.isNotEmpty(resultList)) {
					textListMap.put(dictCode, resultList);
				}
			}
		}
		return textListMap;
	}

	public static String dateToString(Date d) {
		SimpleDateFormat aDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return aDate.format(d);
	}

	/**
	 * 获取类的所有属性，包括父类
	 * 
	 * @param object
	 * @return
	 */
	public static Field[] getAllFields(Object object) {
		Class<?> clazz = object.getClass();
		List<Field> fieldList = new ArrayList<>();
		while (clazz != null) {
			fieldList.addAll(new ArrayList<>(Arrays.asList(clazz.getDeclaredFields())));
			clazz = clazz.getSuperclass();
		}
		Field[] fields = new Field[fieldList.size()];
		fieldList.toArray(fields);
		return fields;
	}
}
